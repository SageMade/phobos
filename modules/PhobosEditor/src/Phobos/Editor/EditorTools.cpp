#include "Phobos/Editor/EditorTools.h"
#include <GLM/gtc/epsilon.hpp>
#include "imgui.h"
#include "Phobos/Graphics/TextureCubeMap.h"
#include "Phobos/Utils/AssetDatabase.h"
#include "Phobos/Graphics/Texture2D.h"

namespace Phobos::Editor {
	using namespace Phobos::Graphics;
	using namespace Phobos::Utils;
	
	bool EditorTools::DrawTexture2D(Phobos::Guid& imageGuid, const char* name, int* outHandle) {
		Texture2D::sptr tex = AssetDatabase::Get().FindByGuid<Texture2D>(imageGuid);
		Texture2D::sptr newTex = nullptr;
		int handle = 0;
		bool result = false;
		if (imageGuid.isValid()) {
			if (tex != nullptr) {
				handle = tex->GetHandle();
			}
		}
		int size = ImGui::GetTextLineHeight() * 2;
		ImGui::BeginGroup();
		//ImGui::Button("I", ImVec2(size, size));
		ImGui::Image((void*)handle, ImVec2(size, size));

		ImGui::SameLine(0, ImGui::GetStyle().ItemInnerSpacing.x);
		if (tex != nullptr) {
			sprintf_s(_CharBuffer, 256, "%s##%s", tex->GetName().c_str(), tex->GetGUID().str().c_str());
		} else {
			sprintf_s(_CharBuffer, 256, "<none>");
		}
		if (ImGui::BeginCombo(name, _CharBuffer)) {
			if (ImGui::Selectable("<none>", tex == nullptr)) {
				imageGuid.Clear();
			}
			auto& map = AssetDatabase::Get().GetIterator<Texture2D>();

			for (auto& [guid, tex] : map) {
				sprintf_s(_CharBuffer2, 256, "%s##%s", tex->GetName().c_str(), tex->GetGUID().str().c_str());
				if (ImGui::Selectable(_CharBuffer2, strcmp(_CharBuffer, _CharBuffer2) == 0)) {
					imageGuid = tex->GetGUID();
					newTex = tex;
					result |= true;
				}
			}
			ImGui::EndCombo();
		}
		ImGui::EndGroup();

		if (ImGui::BeginDragDropSource(ImGuiDragDropFlags_SourceAllowNullID)) {
			ImGui::SetDragDropPayload("Texture2D", &imageGuid, 16);        // Set payload to carry the index of our item (could be anything)
			ImGui::EndDragDropSource();
		}
		if (ImGui::BeginDragDropTarget()) {
			if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("Texture2D")) {
				memcpy(&imageGuid, payload->Data, 16); // Copy GUID over
				result = true;
			}
			ImGui::EndDragDropTarget();
		}
		if (outHandle != nullptr) {
			if (newTex != nullptr) *outHandle = newTex->GetHandle();
			else if (tex != nullptr) *outHandle = tex->GetHandle();
			else *outHandle = 0;
		}
		return result;
	}

	bool EditorTools::DrawTextureCube(Phobos::Guid& imageGuid, const char* name, int* outHandle)
	{
		TextureCubeMap::sptr tex = AssetDatabase::Get().FindByGuid<TextureCubeMap>(imageGuid);
		TextureCubeMap::sptr newTex = nullptr;
		int handle = 0;
		bool result = false;
		if (imageGuid.isValid()) {
			if (tex != nullptr) {
				handle = tex->GetHandle();
			}
		}
		int size = ImGui::GetTextLineHeight() * 2;
		ImGui::BeginGroup();
		ImGui::Button("E", ImVec2(size, size));

		ImGui::SameLine(0, ImGui::GetStyle().ItemInnerSpacing.x);
		if (tex != nullptr) {
			sprintf_s(_CharBuffer, 256, "%s##%s", tex->GetName().c_str(), tex->GetGUID().str().c_str());
		} else {
			sprintf_s(_CharBuffer, 256, "<none>");
		}
		if (ImGui::BeginCombo(name, _CharBuffer)) {
			if (ImGui::Selectable("<none>", tex == nullptr)) {
				imageGuid.Clear();
			}
			auto& map = AssetDatabase::Get().GetIterator<TextureCubeMap>();

			for (auto& [guid, tex] : map) {
				sprintf_s(_CharBuffer2, 256, "%s##%s", tex->GetName().c_str(), tex->GetGUID().str().c_str());
				if (ImGui::Selectable(_CharBuffer2, strcmp(_CharBuffer, _CharBuffer2) == 0)) {
					imageGuid = tex->GetGUID();
					newTex = tex;
					result |= true;
				}
			}
			ImGui::EndCombo();
		}
		ImGui::EndGroup();

		if (ImGui::BeginDragDropSource(ImGuiDragDropFlags_SourceAllowNullID)) {
			ImGui::SetDragDropPayload("TextureCubeMap", &imageGuid, 16);        // Set payload to carry the index of our item (could be anything)
			ImGui::EndDragDropSource();
		}
		if (ImGui::BeginDragDropTarget()) {
			if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("TextureCubeMap")) {
				memcpy(&imageGuid, payload->Data, 16); // Copy GUID over
				result = true;
			}
			ImGui::EndDragDropTarget();
		}
		if (outHandle != nullptr) {
			if (newTex != nullptr) *outHandle = newTex->GetHandle();
			else if (tex != nullptr) *outHandle = tex->GetHandle();
			else *outHandle = 0;
		}
		return result;
	}

	bool EditorTools::Draw(const char* name, float* value, int n) {
		if (_CurrentHints.RenderType == EditorRenderType::Angle && n == 1) {
			return ImGui::SliderAngle(name, value);
		} else if (_CurrentHints.RenderType == EditorRenderType::Color && n == 3) {
			return ImGui::ColorEdit3(name, value);
		} else if (_CurrentHints.RenderType == EditorRenderType::Color && n == 4) {
			return ImGui::ColorEdit4(name, value);
		} else {
			if (glm::epsilonEqual(_CurrentHints.Min, _CurrentHints.Max, glm::epsilon<float>())) {
				return ImGui::DragScalarN(name, ImGuiDataType_Float, value, n, _CurrentHints.Power);
			} else if (_CurrentHints.RenderType == EditorRenderType::Drag) {
				return ImGui::DragScalarN(name, ImGuiDataType_Float, value, n, _CurrentHints.Power, &_CurrentHints.Min, &_CurrentHints.Max);
			} else {
				return ImGui::SliderScalarN(name, ImGuiDataType_Float, value, n, &_CurrentHints.Min, &_CurrentHints.Max, "%.3f");
			}
		}
	}

	bool EditorTools::Draw(const char* name, int* value, int n) {
		return Draw(name, ImGuiDataType_S32, value, n);
	}

	bool EditorTools::Draw(const char* name, uint32_t* value, int n) {
		return Draw(name, ImGuiDataType_U32, value, n);
	}

	bool EditorTools::Draw(const char* name, ImGuiDataType type, void* value, int n)
	{
		if (glm::epsilonEqual(_CurrentHints.Min, _CurrentHints.Max, glm::epsilon<float>())) {
			return ImGui::DragScalarN(name, type, value, n, _CurrentHints.Power);
		} else if (_CurrentHints.RenderType == EditorRenderType::Drag) {
			return _RenderDragScalar(name, type, value, n);
		} else {
			return _RenderSlidingScalar(name, type, value, n);
		}
	}

	void EditorTools::PushHints(const EditorHints& hints) {
		_Hints.push(_CurrentHints);
		_CurrentHints = hints;
	}

	void EditorTools::PopHints()
	{
		LOG_ASSERT(_Hints.size() > 0, "Hint Push/Pop mismatch");
		_CurrentHints = _Hints.top();
		_Hints.pop();
	}

	bool EditorTools::_RenderDragScalar(const char* name, ImGuiDataType type, void* value, int n) {
		switch (type) {
		case ImGuiDataType_S8:
		{
			int8_t min = static_cast<int8_t>(_CurrentHints.Min);
			int8_t max = static_cast<int8_t>(_CurrentHints.Max);
			return ImGui::DragScalarN(name, type, value, n, _CurrentHints.Power, &min, &max);
		}
		case ImGuiDataType_U8:
		{
			uint8_t min = static_cast<uint8_t>(_CurrentHints.Min);
			uint8_t max = static_cast<uint8_t>(_CurrentHints.Max);
			return ImGui::DragScalarN(name, type, value, n, _CurrentHints.Power, &min, &max);
		}
		case ImGuiDataType_S16:
		{
			int16_t min = static_cast<int16_t>(_CurrentHints.Min);
			int16_t max = static_cast<int16_t>(_CurrentHints.Max);
			return ImGui::DragScalarN(name, type, value, n, _CurrentHints.Power, &min, &max);
		}
		case ImGuiDataType_U16:
		{
			uint16_t min = static_cast<uint16_t>(_CurrentHints.Min);
			uint16_t max = static_cast<uint16_t>(_CurrentHints.Max);
			return ImGui::DragScalarN(name, type, value, n, _CurrentHints.Power, &min, &max);
		}
		case ImGuiDataType_S32:
		{
			int32_t min = static_cast<int32_t>(_CurrentHints.Min);
			int32_t max = static_cast<int32_t>(_CurrentHints.Max);
			return ImGui::DragScalarN(name, type, value, n, _CurrentHints.Power, &min, &max);
		}
		case ImGuiDataType_U32:
		{
			int32_t min = static_cast<uint32_t>(_CurrentHints.Min);
			int32_t max = static_cast<uint32_t>(_CurrentHints.Max);
			return ImGui::DragScalarN(name, type, value, n, _CurrentHints.Power, &min, &max);
		}
		case ImGuiDataType_S64:
		{
			int64_t min = static_cast<int64_t>(_CurrentHints.Min);
			int64_t max = static_cast<int64_t>(_CurrentHints.Max);
			return ImGui::DragScalarN(name, type, value, n, _CurrentHints.Power, &min, &max);
		}
		case ImGuiDataType_U64:
		{
			int64_t min = static_cast<uint64_t>(_CurrentHints.Min);
			int64_t max = static_cast<uint64_t>(_CurrentHints.Max);
			return ImGui::DragScalarN(name, type, value, n, _CurrentHints.Power, &min, &max);
		}
		case ImGuiDataType_Float:
		{
			return ImGui::DragScalarN(name, type, value, n, _CurrentHints.Power, &_CurrentHints.Min, &_CurrentHints.Max);
		}
		case ImGuiDataType_Double:
		{
			double min = static_cast<double>(_CurrentHints.Min);
			double max = static_cast<double>(_CurrentHints.Max);
			return ImGui::DragScalarN(name, type, value, n, _CurrentHints.Power, &min, &max);
		}
		default:
			LOG_ASSERT(false, "Unknown type");
			return false;
		}
	}

	bool EditorTools::_RenderSlidingScalar(const char* name, ImGuiDataType type, void* value, int n) {
		switch (type) {
		case ImGuiDataType_S8:
		{
			int8_t min = static_cast<int8_t>(_CurrentHints.Min);
			int8_t max = static_cast<int8_t>(_CurrentHints.Max);
			return ImGui::SliderScalarN(name, type, value, n, &min, &max, nullptr, _CurrentHints.Power);
		}
		case ImGuiDataType_U8:
		{
			uint8_t min = static_cast<uint8_t>(_CurrentHints.Min);
			uint8_t max = static_cast<uint8_t>(_CurrentHints.Max);
			return ImGui::SliderScalarN(name, type, value, n, &min, &max, nullptr, _CurrentHints.Power);
		}
		case ImGuiDataType_S16:
		{
			int16_t min = static_cast<int16_t>(_CurrentHints.Min);
			int16_t max = static_cast<int16_t>(_CurrentHints.Max);
			return ImGui::SliderScalarN(name, type, value, n, &min, &max, nullptr, _CurrentHints.Power);
		}
		case ImGuiDataType_U16:
		{
			uint16_t min = static_cast<uint16_t>(_CurrentHints.Min);
			uint16_t max = static_cast<uint16_t>(_CurrentHints.Max);
			return ImGui::SliderScalarN(name, type, value, n, &min, &max, nullptr, _CurrentHints.Power);
		}
		case ImGuiDataType_S32:
		{
			int32_t min = static_cast<int32_t>(_CurrentHints.Min);
			int32_t max = static_cast<int32_t>(_CurrentHints.Max);
			return ImGui::SliderScalarN(name, type, value, n, &min, &max, nullptr, _CurrentHints.Power);
		}
		case ImGuiDataType_U32:
		{
			uint32_t min = static_cast<uint32_t>(_CurrentHints.Min);
			uint32_t max = static_cast<uint32_t>(_CurrentHints.Max);
			return ImGui::SliderScalarN(name, type, value, n, &min, &max, nullptr, _CurrentHints.Power);
		}
		case ImGuiDataType_S64:
		{
			int64_t min = static_cast<int64_t>(_CurrentHints.Min);
			int64_t max = static_cast<int64_t>(_CurrentHints.Max);
			return ImGui::SliderScalarN(name, type, value, n, &min, &max, nullptr, _CurrentHints.Power);
		}
		case ImGuiDataType_U64:
		{
			uint64_t min = static_cast<uint64_t>(_CurrentHints.Min);
			uint64_t max = static_cast<uint64_t>(_CurrentHints.Max);
			return ImGui::SliderScalarN(name, type, value, n, &min, &max, nullptr, _CurrentHints.Power);
		}
		case ImGuiDataType_Float:
		{
			return ImGui::SliderScalarN(name, type, value, n, &_CurrentHints.Min, &_CurrentHints.Max, nullptr, _CurrentHints.Power);
		}
		case ImGuiDataType_Double:
		{
			double min = static_cast<double>(_CurrentHints.Min);
			double max = static_cast<double>(_CurrentHints.Max);
			return ImGui::SliderScalarN(name, type, value, n, &min, &max, nullptr, _CurrentHints.Power);
		}
		default:
			LOG_ASSERT(false, "Unknown type");
			return false;
		}
	}
}