#include "Phobos/Editor/Windows/InspectorWindow.h"
#include "Phobos/Gameplay/GameObject.h"
#include "Phobos/Gameplay/Transform.h"
#include "Phobos/Gameplay/Scene.h"
#include "Phobos/Editor/EditorManager.h"
#include <imgui.h>

#include <ENTT/entt.hpp>
#include <ENTT/core/hashed_string.hpp>

#include "Phobos/Application.h"
#include "Phobos/Logging.h"
#include "Phobos/Gameplay/GameObjectTag.h"
#include "Phobos/Gameplay/IBehaviour.h"

namespace Phobos::Editor::Windows
{
	using namespace Gameplay;
	using namespace entt::literals;

	void InspectorWindow::_RenderMeta(entt::meta_type& meta, entt::meta_handle instance)
	{
		if (meta) {
			entt::meta_type type = instance->type();
			auto fields = type.data();
			for (auto& field : fields) {
				entt::meta_any instanceData = instance->get(field.id());
				std::string name = "Unknown";
				float min = 0.0;
				float max = 0.0;
				for(auto& prop : field.prop()) {
					auto keyType = prop.key().type();
					entt::basic_hashed_string<char>* key = prop.key().try_cast<entt::basic_hashed_string<char>>();
					if (key) {
						auto value = prop.value();
						auto valueType = prop.value().type();
						if (*key == "_name"_hs) {
							const char** temp = value.try_cast<const char*>();
							if (temp) {
								name = *temp;
							}
						}
						if (*key == "_min"_hs) {
							float* temp =  prop.value().try_cast<float>();
							if (temp != nullptr) min = *temp;
						}
						if (*key == "_max"_hs) {
							float* temp = prop.value().try_cast<float>();
							if (temp != nullptr) max = *temp;
						}
					}
				}
				entt::meta_type fieldType = field.type();
				if (fieldType.info().seq() == entt::type_seq<bool>::value()) {
					bool* value = instanceData.try_cast<bool>();
					LOG_ASSERT(value, "Failed to cast metaprop to bool, check your template definitions!");
					if (ImGui::Checkbox(name.c_str(), value)) {
						instance->set(field.id(), *value);
					}
				} else if (fieldType.info().seq() == entt::type_seq<glm::vec3>::value()) {
					glm::vec3* value = instanceData.try_cast<glm::vec3>();
					LOG_ASSERT(value, "Failed to cast metaprop to bool, check your template definitions!");
					if (ImGui::DragFloat3(name.c_str(), (float*)value)) {
						instance->set(field.id(), *value);
					}
				} else if (fieldType.info().seq() == entt::type_seq<float>::value()) {
					float* value = instanceData.try_cast<float>();
					LOG_ASSERT(value, "Failed to cast metaprop to float, check your template definitions!");
					if (min != 0.0f && max != 0.0f) {
						if (ImGui::DragFloat(name.c_str(), value, min, max)) {
							instance->set(field.id(), *value);
						}
					} else {
						if (ImGui::DragFloat(name.c_str(), value)) {
							instance->set(field.id(), *value);
						}
					}
				}
			}
		}
	}

	void InspectorWindow::Render()
	{
		GameObject context = EditorManager::GetSelectedObject();
		if (context.IsValid()) {
			static char buffer[256];
			sprintf_s(buffer, 256, "%s", context.GetName().c_str());
			if (ImGui::InputText("Name", buffer, 256)) {
				context.SetName(buffer);
			}
			ImGui::Separator();
			ImGui::Text("Transform");
			ImGui::Separator();
			Gameplay::Transform& t = context.GetTransform();
			glm::vec3 pos = t.LocalTransform()[3];
			if (ImGui::DragFloat3("Local Position", &pos[0])) {
				t.SetLocalPosition(pos);
			}
			glm::vec3 euler = t.GetLocalRotation();
			if (ImGui::DragFloat3("Local Rotation", &euler[0])) {
				t.SetLocalRotation(euler);
			}
			glm::vec3 scale = t.GetLocalScale();
			if (ImGui::DragFloat3("Local Scale", &scale[0])) {
				t.SetLocalScale(scale);
			}
			ImGui::Separator();

			entt::registry& reg = Application::Instance().ActiveScene->Registry();
			reg.visit(context, [this, &reg, &context](const entt::type_info metaType) {
				entt::meta_type type = entt::resolve(metaType);
				if (type) {
					entt::meta_prop nameProp = type.prop("_name"_hs);
					std::string typeName = type.info().name().data();
					if (nameProp) {
						const char** value = nameProp.value().try_cast<const char*>();
						if (value)
							typeName = *value;
					}
					if (ImGui::CollapsingHeader(typeName.c_str())) {
						entt::meta_handle instance = GameScene::GetComponent(reg, context, metaType.seq());
						_RenderMeta(type, instance);
					}
				}
			});
			ImGui::Separator();
			ImGui::Text("Scripts");
			ImGui::Separator();
			if (context.Has<BehaviourBinding>()) {
				BehaviourBinding& binding = context.Get<BehaviourBinding>();
				for(std::shared_ptr<IBehaviour>& behaviour : binding.Behaviours) {
					ImGui::PushID(behaviour->Guid.str().c_str());
					ImGui::Separator();
					ImGui::Checkbox(behaviour->GetTypeName(), &behaviour->Enabled);
					ImGui::Separator();
					entt::id_type typeID = behaviour->GetTypeNameHash(); //2687213279
					entt::meta_type meta = entt::resolve(typeID);
					if (meta) {
						entt::meta_handle instance = behaviour->GetHandle();
						_RenderMeta(meta, instance);
					}
					ImGui::PopID();
				}
			}
		}
	}
}
