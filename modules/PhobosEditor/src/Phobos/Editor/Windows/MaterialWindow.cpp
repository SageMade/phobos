#include "Phobos/Editor/Windows/MaterialWindow.h"
#include "Phobos/Editor/EditorTools.h"

#include "Phobos/Editor/Windows/ShaderWindow.h"
#include "Phobos/Utils/AssetDatabase.h"

namespace Phobos::Editor::Windows {

	bool MaterialWindow::_RenderMaterialUniform(Graphics::ShaderMaterial::UniformData& uniform, EditorHints* hints) {
		if (hints != nullptr) {
			EditorTools::PushHints(*hints);
		}
		bool result = false;
		if (uniform.ArraySize == 1) {
			result = ShaderWindow::RenderUniformControl(uniform.Name.c_str(), uniform.Type, uniform.Value);
		} else {
			result = ShaderWindow::RenderUniformControl(uniform.Name.c_str(), uniform.Type, uniform.ArrayBlock, uniform.ArraySize);
		}
		if (hints != nullptr) {
			EditorTools::PopHints();
		}
		return result;
	}

	void MaterialWindow::Render() {
		auto it = Utils::AssetDatabase::Get().GetIterator<Graphics::ShaderMaterial>();
		for (auto& [guid, material] : it) {
			ImGui::PushID(material->GetName().c_str());
			RenderMaterial(material);
			ImGui::PopID();
		}
	}

	void MaterialWindow::RenderMaterial(const Graphics::ShaderMaterial::sptr& mat) {
		sprintf_s(_CharBuffer, 256, "%s##%s", mat->GetName().c_str(), mat->GetGUID().str().c_str());
		if (ImGui::CollapsingHeader(_CharBuffer)) {
			ImGui::SliderInt("Render Order", &mat->RenderLayer, 0, 1000);
			ImGui::Checkbox("Transparent", &mat->IsTransparent);
			ImGui::Checkbox("Read Depth", &mat->DepthRead);
			ImGui::Checkbox("Write Depth", &mat->DepthWrite);
			ImGui::Indent();
			for (auto& [hash, data] : mat->_data) {
				if (data.HasEditor) {
					if (_RenderMaterialUniform(data, mat->GetShader()->GetEditorHints(data.Location))) {
						mat->Set(data.Name, data.Type, data.Value, data.ArraySize);
					}
				}
			}
			ImGui::Unindent();
		}
	}

}
