#include "Phobos/Editor/Windows/TextureWindow.h"

#include "imgui.h"
#include "Phobos/Utils/Platform/FileDialogs.h"

namespace Phobos::Editor::Windows {
	using namespace Utils;
	using namespace Graphics;
	
	void TextureWindow::Render() {
		const auto& it = AssetDatabase::Get().GetIterator<Texture2D>();
		int cols = 1 + glm::min((int)ImGui::GetContentRegionAvailWidth() / 64, 3);
		int size = (ImGui::GetContentRegionAvailWidth() / cols);
		ImGui::Columns(cols);
		for (const auto& [guid, ptr] : it) {
			ImGui::PushID(&guid);
			ImGui::Image((void*)ptr->GetHandle(), ImVec2(size, size));
			if (ImGui::BeginDragDropSource(ImGuiDragDropFlags_SourceAllowNullID)) {
				ImGui::SetDragDropPayload("Texture2D", &guid, 16);        // Set payload to carry the index of our item (could be anything)
				ImGui::EndDragDropSource();
			}
			ImGui::Text(ptr->GetName().c_str());
			ImGui::NextColumn();
			ImGui::PopID();
		}
		if (ImGui::Button("+")) {
			std::optional<std::string> file = Platform::FileDialogs::OpenFile("PNG\0*.png\0jpg\0*.jpg\0jpeg\0*.jpeg\0bmp\0*.bmp\0\0");

			if (file.has_value()) {
				AssetDatabase::Get().LoadFromFile<Texture2D>(file.value());
			}
		}
		ImGui::Columns(cols);
	}
}