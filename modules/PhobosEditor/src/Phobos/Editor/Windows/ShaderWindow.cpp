#include "Phobos/Editor/Windows/ShaderWindow.h"
#include "Phobos/Editor/EditorTools.h"
#include "Phobos/Utils/AssetDatabase.h"
#include "Phobos/Graphics/Texture2D.h"

namespace Phobos::Editor::Windows {
	using namespace Utils;
	using namespace Graphics;
	void ShaderWindow::RenderShader(const Shader::sptr& shader) {
		sprintf_s(_CharBuffer, 256, "%s##%s", shader->GetName().c_str(), shader->GetGUID().str().c_str());
		if (ImGui::CollapsingHeader(_CharBuffer)) {
			ImGui::Indent();
			if (ImGui::CollapsingHeader("Uniform Defaults")) {
				ImGui::Indent();
				std::vector<Shader::UniformInfo>& uniforms = shader->GetUniforms();
				for (Shader::UniformInfo& uniform : uniforms) {
					RenderUniformDefaultSelector(uniform);
				}
				ImGui::Unindent();
			}
			ImGui::Unindent();
		}
	}

	bool ShaderWindow::RenderUniformDefaultSelector(Shader::UniformInfo& uniform) {
		if (uniform.EditorHints != nullptr) {
			EditorTools::PushHints(*uniform.EditorHints);
		}
		if (uniform.ArraySize == 1) {
			return RenderUniformControl(uniform.Name.c_str(), uniform.Type, uniform.DefaultValueBuffer);
		} else {
			return RenderUniformControl(uniform.Name.c_str(), uniform.Type, uniform.DefaultArrayData, uniform.ArraySize);
		}
		if (uniform.EditorHints != nullptr) {
			EditorTools::PopHints();
		}
	}

	bool ShaderWindow::RenderUniformControl(const char* name, ShaderDataType type, void* data, int count)
	{
		const char* names[] = {
			"###0", "###1", "###2", "###3"
		};
		bool result = false;
		if (type != ShaderDataType::None && data != nullptr) {
			ShaderDataTypecode baseType = GetShaderDataTypeCode(type);
			ImGui::PushID(name);
			static char buffer[256];
			for (int ix = 0; ix < count; ix++) {
				ImGui::PushID(ix);
				void* item = ((char*)data) + (ShaderDataTypeSize(type) * ix);
				if (count > 1) {
					sprintf_s(buffer, 256, "%s [%i]", name, ix);
				} else {
					sprintf_s(buffer, 256, "%s", name);
				}
				switch (baseType)
				{
				case ShaderDataTypecode::Bool:
				{
					int count = ShaderDataTypeComponentCount(type);
					ImGui::SetNextItemWidth(ImGui::GetContentRegionAvailWidth() / 2.0f);
					ImGui::Text(buffer);
					ImGui::SameLine();
					float boxW = (ImGui::GetContentRegionAvailWidth() / 2.0f) / count;
					for (int ix = 0; ix < count; ix++) {
						ImGui::SetNextItemWidth(boxW);
						result |= ImGui::Checkbox(names[ix], ((bool*)item) + ix);
						if (ix < count - 1) ImGui::SameLine();
					}
				} break;
				case ShaderDataTypecode::Double:
				{
					int count = ShaderDataTypeComponentCount(type);
					result = ImGui::DragScalarN(buffer,
												ImGuiDataType_Double,
												((double*)item),
												count,
												1.0f);
				} break;
				case ShaderDataTypecode::Float:
				{
					int count = ShaderDataTypeComponentCount(type);
					result = EditorTools::Draw(buffer, (float*)item, count);

				} break;
				case ShaderDataTypecode::Int:
				{
					int count = ShaderDataTypeComponentCount(type);
					result = EditorTools::Draw(buffer, ImGuiDataType_S32, (int*)item, count);
				} break;
				case ShaderDataTypecode::Uint:
				{
					int count = ShaderDataTypeComponentCount(type);
					result = EditorTools::Draw(buffer, ImGuiDataType_U32, (uint32_t*)item, count);
				} break;
				case ShaderDataTypecode::Matrix:
				{
					switch (type)
					{
					case ShaderDataType::Mat3:
					{
						glm::mat3& mat = *(glm::mat3*)item;
						float eulerX = glm::atan(mat[1][2], mat[2][2]);
						float eulerY = glm::atan(mat[0][2], glm::sqrt(glm::pow(mat[1][2], 2.0f) + glm::pow(mat[2][2], 2.0f)));
						float eulerZ = glm::atan(mat[0][1], mat[0][0]);
						glm::vec3 eulerDeg = glm::degrees(glm::vec3(eulerX, eulerY, eulerZ));
						if (ImGui::SliderFloat3(buffer, &eulerDeg[0], -180.0f, 180.0f)) {
							mat = glm::mat3_cast(glm::quat(glm::radians(eulerDeg)));
							result = true;
						}
					}
					}
				} break;
				case ShaderDataTypecode::Texture:
				{
					void* guidPtr = ((int*)item) + 1; // The first 4 bytes is the texture handle
					int& handle = *(int*)item;
					switch (type)
					{
					case ShaderDataType::Tex2D:
						result |= EditorTools::DrawTexture2D(*(Phobos::Guid*)guidPtr, buffer, &handle);
						break;
					case ShaderDataType::TexCube:
						result |= EditorTools::DrawTextureCube(*(Phobos::Guid*)guidPtr, buffer, &handle);
						break;
					}
				} break;

				}
				ImGui::PopID();
			}
			ImGui::PopID();
		}
		return result;
	}

	void ShaderWindow::Render() {
		auto it = AssetDatabase::Get().GetIterator<Shader>();
		for (auto& [guid, shader] : it) {
			ImGui::PushID(shader->GetName().c_str());
			RenderShader(shader);
			ImGui::PopID();
		}
	}
}
