#include "Phobos/Editor/Windows/CodeWindow.h"
#include <fstream>
#include "Phobos/Logging.h"
#include "imgui_internal.h"
#include "GLFW/glfw3.h"
#include <algorithm>
#include <filesystem>
#include "inipp.h"
#include "Phobos/Utils/Platform/FileDialogs.h"

namespace Phobos::Editor::Windows {
std::vector<TextEditor::LanguageDefinition> CodeWindow::KnownLanguages = {
	TextEditor::LanguageDefinition::GLSL(),
	TextEditor::LanguageDefinition::HLSL(),
	TextEditor::LanguageDefinition::CPlusPlus(),
	TextEditor::LanguageDefinition::C(),
	TextEditor::LanguageDefinition::Lua(),
	TextEditor::LanguageDefinition::AngelScript()
};

CodeWindow::CodeWindow() {
	myLang = TextEditor::LanguageDefinition::GLSL();
	myEditor.SetLanguageDefinition(myLang);

	inipp::Ini<char> file;
	std::ifstream is("codesettings.ini");
	if(is.is_open()) {
		LOG_INFO("Loading code editor settings from INI file");
		file.parse(is);
	}
	is.close();
	
	TextEditor::ErrorMarkers markers;
	myEditor.SetErrorMarkers(markers);
	
	bool bValue = false;
	int iValue = 1;
	inipp::extract(file.sections["Editor"]["show_whitespace"], bValue, false);
	myEditor.SetShowWhitespaces(bValue);
	inipp::extract(file.sections["Editor"]["colorizer_enabled"], bValue, true);
	myEditor.SetColorizerEnable(bValue);
	inipp::extract(file.sections["Editor"]["zoom"], iValue, 1);
	myEditor.SetZoom(iValue);
	inipp::extract(file.sections["Editor"]["tab_size"], iValue, 4);
	myEditor.SetTabSize(iValue);

	TextEditor::Palette palette = myEditor.GetDarkPalette();
	inipp::extract(file.sections["Palette"]["default"],                    palette[(unsigned)TextEditor::PaletteIndex::Default],                 palette[(unsigned)TextEditor::PaletteIndex::Default]);
	inipp::extract(file.sections["Palette"]["keyword"],                    palette[(unsigned)TextEditor::PaletteIndex::Keyword],                 palette[(unsigned)TextEditor::PaletteIndex::Keyword]);
	inipp::extract(file.sections["Palette"]["number"],                     palette[(unsigned)TextEditor::PaletteIndex::Number],                  palette[(unsigned)TextEditor::PaletteIndex::Number]);
	inipp::extract(file.sections["Palette"]["string"],                     palette[(unsigned)TextEditor::PaletteIndex::String],                  palette[(unsigned)TextEditor::PaletteIndex::String]);
	inipp::extract(file.sections["Palette"]["char_literal"],               palette[(unsigned)TextEditor::PaletteIndex::CharLiteral],             palette[(unsigned)TextEditor::PaletteIndex::CharLiteral]);
	inipp::extract(file.sections["Palette"]["punctuation"],                palette[(unsigned)TextEditor::PaletteIndex::Punctuation],             palette[(unsigned)TextEditor::PaletteIndex::Punctuation]);
	inipp::extract(file.sections["Palette"]["preprocessor"],               palette[(unsigned)TextEditor::PaletteIndex::Preprocessor],            palette[(unsigned)TextEditor::PaletteIndex::Preprocessor]);
	inipp::extract(file.sections["Palette"]["identifier"],                 palette[(unsigned)TextEditor::PaletteIndex::Identifier],              palette[(unsigned)TextEditor::PaletteIndex::Identifier]);
	inipp::extract(file.sections["Palette"]["known_identifier"],           palette[(unsigned)TextEditor::PaletteIndex::KnownIdentifier],         palette[(unsigned)TextEditor::PaletteIndex::KnownIdentifier]);
	inipp::extract(file.sections["Palette"]["preproc_identifier"],         palette[(unsigned)TextEditor::PaletteIndex::PreprocIdentifier],       palette[(unsigned)TextEditor::PaletteIndex::PreprocIdentifier]);
	inipp::extract(file.sections["Palette"]["comment"],                    palette[(unsigned)TextEditor::PaletteIndex::Comment],                 palette[(unsigned)TextEditor::PaletteIndex::Comment]);
	inipp::extract(file.sections["Palette"]["multiline_comment"],          palette[(unsigned)TextEditor::PaletteIndex::MultiLineComment],        palette[(unsigned)TextEditor::PaletteIndex::MultiLineComment]);
	inipp::extract(file.sections["Palette"]["background"],                 palette[(unsigned)TextEditor::PaletteIndex::Background],              palette[(unsigned)TextEditor::PaletteIndex::Background]);
	inipp::extract(file.sections["Palette"]["cursor"],                     palette[(unsigned)TextEditor::PaletteIndex::Cursor],                  palette[(unsigned)TextEditor::PaletteIndex::Cursor]);
	inipp::extract(file.sections["Palette"]["selection"],                  palette[(unsigned)TextEditor::PaletteIndex::Selection],               palette[(unsigned)TextEditor::PaletteIndex::Selection]);
	inipp::extract(file.sections["Palette"]["error_marker"],               palette[(unsigned)TextEditor::PaletteIndex::ErrorMarker],             palette[(unsigned)TextEditor::PaletteIndex::ErrorMarker]);
	inipp::extract(file.sections["Palette"]["breakpoint"],                 palette[(unsigned)TextEditor::PaletteIndex::Breakpoint],              palette[(unsigned)TextEditor::PaletteIndex::Breakpoint]);
	inipp::extract(file.sections["Palette"]["line_number"],                palette[(unsigned)TextEditor::PaletteIndex::LineNumber],              palette[(unsigned)TextEditor::PaletteIndex::LineNumber]);
	inipp::extract(file.sections["Palette"]["current_line_fill"],          palette[(unsigned)TextEditor::PaletteIndex::CurrentLineFill],         palette[(unsigned)TextEditor::PaletteIndex::CurrentLineFill]);
	inipp::extract(file.sections["Palette"]["current_line_fill_inactive"], palette[(unsigned)TextEditor::PaletteIndex::CurrentLineFillInactive], palette[(unsigned)TextEditor::PaletteIndex::CurrentLineFillInactive]);
	inipp::extract(file.sections["Palette"]["current_line_edge"],          palette[(unsigned)TextEditor::PaletteIndex::CurrentLineEdge],         palette[(unsigned)TextEditor::PaletteIndex::CurrentLineEdge]);
	myEditor.SetPalette(palette);
	
	isFileModified = false;
}

void CodeWindow::OnClose() {
	inipp::Ini<char> file;
	inipp::insert(file.sections["Editor"]["show_whitespace"], myEditor.IsShowingWhitespaces());
	inipp::insert(file.sections["Editor"]["colorizer_enabled"], myEditor.IsColorizerEnabled());
	inipp::insert(file.sections["Editor"]["zoom"], myEditor.GetZoom());
	inipp::insert(file.sections["Editor"]["tab_size"], myEditor.GetTabSize());

	const TextEditor::Palette& palette = myEditor.GetPalette();
	inipp::insert(file.sections["Palette"]["default"],                    palette[(unsigned)TextEditor::PaletteIndex::Default]);
	inipp::insert(file.sections["Palette"]["keyword"],                    palette[(unsigned)TextEditor::PaletteIndex::Keyword]);
	inipp::insert(file.sections["Palette"]["number"],                     palette[(unsigned)TextEditor::PaletteIndex::Number]);
	inipp::insert(file.sections["Palette"]["string"],                     palette[(unsigned)TextEditor::PaletteIndex::String]);
	inipp::insert(file.sections["Palette"]["char_literal"],               palette[(unsigned)TextEditor::PaletteIndex::CharLiteral]);
	inipp::insert(file.sections["Palette"]["punctuation"],                palette[(unsigned)TextEditor::PaletteIndex::Punctuation]);
	inipp::insert(file.sections["Palette"]["preprocessor"],               palette[(unsigned)TextEditor::PaletteIndex::Preprocessor]);
	inipp::insert(file.sections["Palette"]["identifier"],                 palette[(unsigned)TextEditor::PaletteIndex::Identifier]);
	inipp::insert(file.sections["Palette"]["known_identifier"],           palette[(unsigned)TextEditor::PaletteIndex::KnownIdentifier]);
	inipp::insert(file.sections["Palette"]["preproc_identifier"],         palette[(unsigned)TextEditor::PaletteIndex::PreprocIdentifier]);
	inipp::insert(file.sections["Palette"]["comment"],                    palette[(unsigned)TextEditor::PaletteIndex::Comment]);
	inipp::insert(file.sections["Palette"]["multiline_comment"],          palette[(unsigned)TextEditor::PaletteIndex::MultiLineComment]);;
	inipp::insert(file.sections["Palette"]["background"],                 palette[(unsigned)TextEditor::PaletteIndex::Background]);
	inipp::insert(file.sections["Palette"]["cursor"],                     palette[(unsigned)TextEditor::PaletteIndex::Cursor]);
	inipp::insert(file.sections["Palette"]["selection"],                  palette[(unsigned)TextEditor::PaletteIndex::Selection]);
	inipp::insert(file.sections["Palette"]["error_marker"],               palette[(unsigned)TextEditor::PaletteIndex::ErrorMarker]);
	inipp::insert(file.sections["Palette"]["breakpoint"],                 palette[(unsigned)TextEditor::PaletteIndex::Breakpoint]);
	inipp::insert(file.sections["Palette"]["line_number"],                palette[(unsigned)TextEditor::PaletteIndex::LineNumber]);
	inipp::insert(file.sections["Palette"]["current_line_fill"],          palette[(unsigned)TextEditor::PaletteIndex::CurrentLineFill]);
	inipp::insert(file.sections["Palette"]["current_line_fill_inactive"], palette[(unsigned)TextEditor::PaletteIndex::CurrentLineFillInactive]);
	inipp::insert(file.sections["Palette"]["current_line_edge"],          palette[(unsigned)TextEditor::PaletteIndex::CurrentLineEdge]);
	
	std::ofstream os("codesettings.ini");
	if (os.is_open()) {
		LOG_INFO("Saving code editor settings to INI file");
		file.generate(os);
	}
	os.close();
}

void CodeWindow::Render() {
	
	ImGui::PushStyleColor(ImGuiCol_WindowBg, { 0,0,0,1 });
	ImGui::PushStyleColor(ImGuiCol_ChildBg, { 0,0,0,1 });
	static char buffer[256];
	sprintf_s(buffer, 256, "%s##%d", myFilePath.empty() ? "<unsaved file>" : std::filesystem::path(myFilePath).stem().string().c_str(), *reinterpret_cast<uint32_t*>(this));
	if (ImGui::Begin(buffer, &IsOpen, { 300, 600 }, -1.0f, ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_MenuBar)) {
		auto cPos = myEditor.GetCursorPosition();
		ImVec2 size = ImGui::GetContentRegionAvail();
		myOpenFileId = ImGui::GetID("Select File");
		mySaveFileId = ImGui::GetID("Save File");

		__RenderMenuBar();
				
		size.y -= 24;
		ImGui::PushStyleColor(ImGuiCol_WindowBg, { 0,0,0,1 });
		ImGui::BeginChild("Editor", size);
		isFileModified |= myEditor.Render(myFilePath.c_str());
		__HandleKeyboard();
		ImGui::EndChild();
		
		ImGui::Text("%5d/%-5d %6d lines  %ix | %s | %s | %s %s", cPos.mLine + 1, cPos.mColumn + 1, myEditor.GetTotalLines(),
			myEditor.GetZoom(),
			myEditor.IsOverwrite() ? "Ovr" : "Ins",
			myEditor.CanUndo() ? "u" : " ",
			myFilePath.empty() ? "<unsaved file>" : myFilePath.c_str(),
			isFileModified ? "*" : "");
		ImGui::PopStyleColor();	
	}
	ImGui::End();
	ImGui::PopStyleColor(2);
}

const std::string& CodeWindow::WindowName() const {
	static char buffer[256];
	sprintf_s(buffer, 256, "%s###%d", myFilePath.empty() ? "<unsaved file>" : myFilePath.c_str(), *reinterpret_cast<const uint32_t*>(this));
	myWindowTitle = buffer;
	return myWindowTitle;
}

void CodeWindow::SetLang(const TextEditor::LanguageDefinition& lang) {
	myLang = lang;
	myEditor.SetLanguageDefinition(lang);
}

const TextEditor::LanguageDefinition& CodeWindow::GetLang() const {
	return myLang;
}

void CodeWindow::OpenFile(const std::string& fileName) {
	std::ifstream file = std::ifstream(std::string(fileName));
	if (file.is_open()) {
		std::string str((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());
		SetSource(str.c_str());
		myFilePath = fileName;
		isFileModified = false;
		__SelectLanguage();
	}
}

void CodeWindow::NewFile()
{
	myEditor.SetText("");
	myFilePath = "";
}

void CodeWindow::SaveFile(const char* fileName) {
	std::ofstream file = std::ofstream(std::string(fileName));
	if (file.is_open()) {
		file << myEditor.GetText();
		myFilePath = fileName;
		isFileModified = false;
		__SelectLanguage();
	}
}

void CodeWindow::SetSource(const char* source) {
	myEditor.SetText(source);
}

std::string CodeWindow::GetSource() const {
	return myEditor.GetText();
}

void CodeWindow::__SelectLanguage() {
	auto path = std::filesystem::path(myFilePath);
	if (path.has_extension()) {
		std::string ext = path.extension().string().substr(1);
		for (auto& lang : KnownLanguages) {
			if (lang.mFileMatches.find(ext) != lang.mFileMatches.end()) {
				myEditor.SetLanguageDefinition(lang);
				break;
			}
		}
	}
}

void CodeWindow::__HandleKeyboard() {
	if (ImGui::IsWindowFocused(ImGuiFocusedFlags_RootAndChildWindows))
	{
		ImGuiIO& io = ImGui::GetIO();
		auto shift = io.KeyShift;
		auto ctrl = io.ConfigMacOSXBehaviors ? io.KeySuper : io.KeyCtrl;
		auto alt = io.ConfigMacOSXBehaviors ? io.KeyCtrl : io.KeyAlt;

		if (ctrl && !shift && ImGui::IsKeyPressed(GLFW_KEY_S)) {
			if (isFileModified) {
				if (!myFilePath.empty()) {
					SaveFile(myFilePath.c_str());
				}
				else {
					__SaveFile();
					//mySaveFile.Show(mySaveFileId);
				}
			}
		}
		if (ctrl && shift && ImGui::IsKeyPressed(GLFW_KEY_S)) {
			__SaveFile();
			//mySaveFile.Show(mySaveFileId);
		}
		if (ctrl && shift && ImGui::IsKeyPressed(GLFW_KEY_N)) {
			NewFile();
		}
		if (ctrl && shift && ImGui::IsKeyPressed(GLFW_KEY_O)) {
			//myOpenFile.Show(myOpenFileId);
			__OpenFile();
		}
	}
}

void CodeWindow::__SaveFile() {
	std::optional<std::string> path = Utils::Platform::FileDialogs::SaveFile("Any\0*.*\0\0");
	if (path.has_value()) {
		myFilePath = path.value();
		SaveFile(myFilePath.c_str());
	}
}

void CodeWindow::__OpenFile() {
	std::optional<std::string> path = Utils::Platform::FileDialogs::OpenFile("Any\0*.*\0\0");
	if (path.has_value()) {
		myFilePath = path.value();
		OpenFile(myFilePath.c_str());
	}
}

void CodeWindow::__RenderMenuBar() {
	if (ImGui::BeginMenuBar()) {
		__RenderFileMenu();
		__RenderOptionsMenu();
	}
	ImGui::EndMenuBar();
}

void CodeWindow::__RenderFileMenu() {
	if (ImGui::BeginMenu("File")) {
		if (ImGui::MenuItem("Open", "Ctrl+Shift+O")) {
			//myOpenFile.Show(myOpenFileId);
			__OpenFile();
		}
		// Save is disabled if our file path is empty
		{
			if (myFilePath.empty()) {
				ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
				ImGui::PushStyleVar(ImGuiStyleVar_Alpha, 0.6f);
			}
			if (ImGui::MenuItem("Save", "Ctrl+S")) {
				SaveFile(myFilePath.c_str());
			}
			if (myFilePath.empty()) {
				ImGui::PopItemFlag();
				ImGui::PopStyleVar();
			}
		}
		if (ImGui::MenuItem("Save As", "Ctrl+Shift+S")) {
			//mySaveFile.Show(mySaveFileId);
			__SaveFile();
		}
		if (ImGui::MenuItem("New", "Ctrl+Shift+N")) {
			NewFile();
		}
		ImGui::EndMenu();
	}
}

#define BoolMenuItem(name, getValue, setValue) \
	{ bool checked = getValue(); if (ImGui::MenuItem(name, nullptr, checked)) { setValue(!checked); } }

ImVec4 ContrastColor(ImVec4 iColor) {
	// Calculate the perceptive luminance (aka luma) - human eye favors green color... 
	double luma = ((0.299 * iColor.x) + (0.587 * iColor.y) + (0.114 * iColor.z));

	// Return black for bright colors, white for dark colors
	return luma > 0.5 ? ImVec4(0, 0, 0, iColor.w) : ImVec4(1.0f, 1.0f, 1.0f, iColor.w);
}
ImU32 ContrastColor(ImU32 iValue) {
	return ImGui::ColorConvertFloat4ToU32(ContrastColor(ImGui::ColorConvertU32ToFloat4(iValue)));
}

void CodeWindow::__RenderOptionsMenu() {
	if (ImGui::BeginMenu("Options")) {
		BoolMenuItem("Show Whitespace", myEditor.IsShowingWhitespaces, myEditor.SetShowWhitespaces);
		BoolMenuItem("Colorizer", myEditor.IsColorizerEnabled, myEditor.SetColorizerEnable);
		if (ImGui::BeginMenu("Language")) {
			for(size_t ix = 0; ix < KnownLanguages.size(); ix++) {
				if (ImGui::MenuItem(KnownLanguages[ix].mName.c_str(), nullptr, myLang.mName == KnownLanguages[ix].mName)) {
					SetLang(KnownLanguages[ix]);
				}
			}
			ImGui::EndMenu();
		}
		if (ImGui::BeginMenu("Palette")) {
			const char* paletteNames[] = {
				"Default",
				"Keyword",
				"Number",
				"String",
				"CharLiteral",
				"Punctuation",
				"Preprocessor",
				"Identifier",
				"KnownIdentifier",
				"PreprocIdentifier",
				"Comment",
				"MultiLineComment",
				"Background",
				"Cursor",
				"Selection",
				"ErrorMarker",
				"Breakpoint",
				"LineNumber",
				"CurrentLineFill",
				"CurrentLineFillInactive",
				"CurrentLineEdge",
			};
			TextEditor::Palette palette = myEditor.GetPalette();
			bool hasChanged = false;
			for (size_t ix = 0; ix < (unsigned)TextEditor::PaletteIndex::Max; ix++) {
				ImGui::PushID(ix + (unsigned)TextEditor::PaletteIndex::Max);
				ImGui::PushStyleColor(ImGuiCol_Text, palette[ix]); 
				if(ImGui::BeginMenu(paletteNames[ix])) {
					ImGui::PushStyleColor(ImGuiCol_Text,   0xFFFFFFFFu);
					ImVec4 col = ImGui::ColorConvertU32ToFloat4(palette[ix]);
					hasChanged |= ImGui::ColorPicker3("Color", &col.x);
					palette[ix] = ImGui::ColorConvertFloat4ToU32(col);
					ImGui::PopStyleColor(1);
					
					ImGui::EndMenu();
				}
				ImGui::PopStyleColor(1);
				ImGui::PopID();
			}
			if (hasChanged)
				myEditor.SetPalette(palette);
			
			ImGui::EndMenu();
		}
		ImGui::EndMenu();
	}
}

TextEditor::LanguageDefinition CreateCppLang() {
	auto lang = TextEditor::LanguageDefinition::CPlusPlus();

	// set your own known preprocessor symbols...
	static const char* ppnames[] = {
		"NULL", "ZeroMemory", "assert" };
	// ... and their corresponding values
	static const char* ppvalues[] = {
		"#define NULL ((void*)0)",
		"Microsoft's own memory zapper function\n(which is a macro actually)\nvoid ZeroMemory(\n\t[in] PVOID  Destination,\n\t[in] SIZE_T Length\n); ",
		" #define assert(expression) (void)(                                                  \n"
		"    (!!(expression)) ||                                                              \n"
		"    (_wassert(_CRT_WIDE(#expression), _CRT_WIDE(__FILE__), (unsigned)(__LINE__)), 0) \n"
		" )"
	};

	for (int i = 0; i < sizeof(ppnames) / sizeof(ppnames[0]); ++i) {
		TextEditor::Identifier id;
		id.mDeclaration = ppvalues[i];
		lang.mPreprocIdentifiers.insert(std::make_pair(std::string(ppnames[i]), id));
	}

	// set your own identifiers
	static const char* identifiers[] = {
		"UINT", "LPVOID"
	};
	static const char* idecls[] = {
		"UINT", "LPVOID"
	};

	for (int i = 0; i < sizeof(identifiers) / sizeof(identifiers[0]); ++i) {
		TextEditor::Identifier id;
		id.mDeclaration = std::string(idecls[i]);
		lang.mIdentifiers.insert(std::make_pair(std::string(identifiers[i]), id));
	}

	return lang;
}
}