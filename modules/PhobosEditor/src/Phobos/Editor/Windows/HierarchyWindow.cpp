#include "Phobos/Editor/Windows/HierarchyWindow.h"


#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "Phobos/Application.h"
#include "Phobos/Gameplay/Scene.h"
#include <imgui.h>



#include "Phobos/Logging.h"
#include "Phobos/Editor/EditorManager.h"
#include "Phobos/Gameplay/GameObjectTag.h"

namespace Phobos::Editor::Windows {
	using namespace Gameplay;

	void HierarchyWindow::__RenderNode(Gameplay::GameObject object)
	{
		HierarchyNode& node = object.Get<HierarchyNode>();
		GameObjectTag& tag = object.Get<GameObjectTag>();
		const std::string guid = tag.GUID.str();
		ImGui::PushID(guid.c_str());
		sprintf_s(_CharBuffer, 256, "%s (0x%08X)###%s", tag.Name.c_str(), ((entt::handle)object).entity(), guid.c_str());
		ImGuiTreeNodeFlags flags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick;
		if (EditorManager::GetSelectedObject() == object) flags |= ImGuiTreeNodeFlags_Selected;
		if (node.FirstChild == entt::null) flags |= ImGuiTreeNodeFlags_Leaf;
		
		bool isExpanded = ImGui::TreeNodeEx(_CharBuffer, flags);
		if (ImGui::IsItemClicked()) {
			EditorManager::SetSelectedObject(object);
		}
		if (ImGui::BeginPopupContextItem()) {
			if (ImGui::MenuItem("Add Child")) {
				GameObject newObj = Application::Instance().ActiveScene->CreateEntity("GameObject", object);
			}
			if (ImGui::MenuItem("Delete")) {
				object.Destroy();
			}
			if (ImGui::MenuItem("Move Up", 0, false, node.Previous != entt::null)) {
				HierarchyNode::MoveChildUp(object);
			}
			if (ImGui::MenuItem("Move Down", 0, false, node.Next != entt::null)) {
				HierarchyNode::MoveChildDown(object);
			}
			ImGui::EndPopup();
		}
		if (flags & ImGuiTreeNodeFlags_Selected) {
			if (ImGui::IsKeyPressed(GLFW_KEY_UP) && ImGui::IsKeyDown(GLFW_KEY_LEFT_ALT) && node.Previous != entt::null) {
				HierarchyNode::MoveChildUp(object);
			}
			if (ImGui::IsKeyPressed(GLFW_KEY_DOWN) && ImGui::IsKeyDown(GLFW_KEY_LEFT_ALT) && node.Next != entt::null) {
				HierarchyNode::MoveChildDown(object);
			}
			
		}
		if (isExpanded) {
			
			entt::entity current = node.FirstChild;
			while (current != entt::null) {
				if (object.Registry().valid(current)) {
					GameObject ctx = entt::handle(object.Registry(), current);
					HierarchyNode& childNode = ctx.Get<HierarchyNode>();
					__RenderNode(ctx);
					current = childNode.Next;
				} else {
					LOG_WARN("Child node is invalid!");
					break;
				}
			}
			ImGui::TreePop(); 
		}
		ImGui::PopID();
	}

	void HierarchyWindow::Render() {
		GameScene::sptr scene = Application::Instance().ActiveScene;
		entt::registry& reg = scene->Registry();
		HierarchyNode& sceneNode = reg.ctx_or_set<HierarchyNode>();
		entt::entity current = sceneNode.FirstChild;
		while (current != entt::null) {
			GameObject ctx = entt::handle(reg, current);
			__RenderNode(ctx);
			HierarchyNode& childNode = ctx.Get<HierarchyNode>();
			current = childNode.Next;
		}
	}
}
