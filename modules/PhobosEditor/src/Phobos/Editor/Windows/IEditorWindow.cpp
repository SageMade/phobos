#include "Phobos/Editor/Windows/IEditorWindow.h"

#include "imgui.h"
#include "imgui_internal.h"

namespace Phobos::Editor::Windows {
	void IEditorWindow::Init() {
		__LoadOpenState();
	}

	void IEditorWindow::__LoadOpenState() {
		ImGuiContext& ctx = *ImGui::GetCurrentContext();
		for (int i = 0; i != ctx.SettingsWindows.Size; i++) {
			char* winName = ctx.SettingsWindows[i].Name;
			if (strcmp(winName, WindowName().c_str()) == 0) {
				IsOpen = !ctx.SettingsWindows[i].Collapsed;
				return;
			}
		}
		IsOpen = true;
	}
}
