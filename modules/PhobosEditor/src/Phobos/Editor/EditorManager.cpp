#include "Phobos/Editor/EditorManager.h"
#include "Phobos/Gameplay/Timing.h"
#include <intrin.h>
#include "glad/glad.h"
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "imgui.h"
#include "Phobos/Application.h"
#include "Phobos/Gameplay/Scene.h"

namespace Phobos::Editor {
	void EditorManager::Render()
	{
		static char buffer[256];
		for (const auto& it : _windows) {
			ImGui::SetNextWindowSize(ImVec2(100, 100));
			if (!ImGui::Begin(it->WindowName().c_str(), &it->IsOpen)) {
				ImGui::End();
				continue;
			}
			it->Render();
			ImGui::End();
		}

		auto it = _fileWindows.begin();
		while (it != _fileWindows.end()) {
			ImGui::SetNextWindowSize(ImVec2(100, 100));
			if (!it->second->IsOpen) {
				it = _fileWindows.erase(it);
			} else {
				it->second->Render();
				++it;
			}
		}
	}

	void EditorManager::RenderWindowMenuItems() {
		for (auto& window : _windows) {
			ImGui::MenuItem(window->WindowName().c_str(), NULL, &window->IsOpen, true);
		}
		ImGui::Separator();
		for (auto& [filename, window] : _fileWindows) {
			ImGui::MenuItem(filename.c_str(), NULL, &window->IsOpen, true);
		}
	}

	bool EditorManager::RenderModal(bool* isOpen, ModalOptions options, const char* title, const char* message, ModalResult* result) {
		return RenderModal(isOpen, options, title, message, result, "Yes", "No", "Cancel");
	}

	bool EditorManager::RenderModal(bool* isOpen, ModalOptions options, const char* title, const char* text, ModalResult* result, const char* ok, const char* no, const char* cancel) {
		bool isClosing = false;
		if (isOpen && *isOpen) {
			ImGui::OpenPopup(title);
			if (ImGui::BeginPopupModal(title, isOpen, ImGuiWindowFlags_AlwaysAutoResize)) {
				ImGui::Text(text);
				unsigned columns = 1;
				columns = __popcnt((unsigned)options); // Get number of bits set in flags

				ImGui::Columns(columns);
				if ((uint8_t)options & (uint8_t)ModalOptions::Yes) {
					ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.1f, 0.6f, 0.1f, 1.0f));
					if (ImGui::Button(ok, ImVec2(ImGui::GetColumnWidth(), 0))) {
						if (result)
							*result = ModalResult::Yes;
						ImGui::CloseCurrentPopup();
						*isOpen = false;
						isClosing = true;
					}
					ImGui::NextColumn();
				}
				if ((uint8_t)options & (uint8_t)ModalOptions::No) {
					ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.6f, 0.1f, 0.1f, 1.0f));
					if (ImGui::Button(no, ImVec2(ImGui::GetColumnWidth(), 0))) {
						if (result)
							*result = ModalResult::No;
						ImGui::CloseCurrentPopup();
						*isOpen = false;
						isClosing = true;
					}
					ImGui::NextColumn();
				}
				if ((uint8_t)options & (uint8_t)ModalOptions::Cancel) {
					ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.6f, 0.6f, 0.1f, 1.0f));
					if (ImGui::Button(cancel, ImVec2(ImGui::GetColumnWidth(), 0))) {
						if (result)
							*result = ModalResult::Cancel;
						ImGui::CloseCurrentPopup();
						*isOpen = false;
						isClosing = true;
					}
					ImGui::NextColumn();
				}
				ImGui::PopStyleColor(columns);
				ImGui::Columns(1);
				ImGui::EndPopup();
			}
		}
		return isClosing;
	}

	Gameplay::GameObject EditorManager::GetSelectedObject() {
		return entt::handle(Application::Instance().ActiveScene->Registry(), _selectedObject);
	}

	void EditorManager::SetSelectedObject(Gameplay::GameObject object) {
		_selectedObject = object;
	}
}
