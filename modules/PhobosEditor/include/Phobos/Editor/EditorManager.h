#pragma once
#include <map>
#include <vector>
#include "Phobos/Editor/Windows/IEditorWindow.h"
#include <Phobos/EnumToString.h>

#include "Phobos/Gameplay/GameObject.h"

namespace Phobos::Editor {
	ENUM(ModalResult, uint8_t,
		 No = 0,
		 Yes = 1,
		 Cancel = 2
	);

	ENUM(ModalOptions, uint8_t,
		 No = 1,
		 Yes = 2,
		 Cancel = 4,
		 YesNo = No | Yes,
		 All = No | Yes | Cancel
	);

	class EditorManager {
	public:
		template <typename T, typename = typename std::enable_if<std::is_base_of<Windows::IEditorWindow, T>::value>::type>
		static void RegisterEditor() {
			_windows.push_back(std::make_shared<T>());
		}

		template <typename T, typename = typename std::enable_if<std::is_base_of<Windows::IEditorWindow, T>::value>::type>
		static std::shared_ptr<T> AddFileWindow(const std::string& filename) {
			auto it = _fileWindows.find(filename);
			if (it != _fileWindows.end()) return std::dynamic_pointer_cast<T>(it->second);

			auto result = std::make_shared<T>();
			_fileWindows[filename] = result;
			return result;
		}

		static void Render();

		static void RenderWindowMenuItems();

		static bool RenderModal(bool* isOpen, ModalOptions options, const char* title, const char* message, ModalResult* result);
		static bool RenderModal(bool* isOpen, ModalOptions options, const char* title, const char* text, ModalResult* result, const char* ok, const char* no, const char* cancel);

		static Gameplay::GameObject GetSelectedObject();
		static void SetSelectedObject(Gameplay::GameObject object);
		
	protected:
		inline static std::vector<Windows::IEditorWindow::sptr> _windows;
		inline static std::map<std::string, Windows::IEditorWindow::sptr> _fileWindows;
		inline static entt::entity _selectedObject;
	};
}
