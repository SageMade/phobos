#pragma once
#include <GLM/glm.hpp>

#include "Phobos/EditorHints.h"
#include "imgui.h"
#include "Phobos/Utils/guid.hpp"

namespace Phobos::Editor {
	class EditorTools
	{
	public:
		static bool DrawTexture2D(Phobos::Guid& imageGuid, const char* name, int* outHandle = nullptr);
		static bool DrawTextureCube(Phobos::Guid& imageGuid, const char* name, int* outHandle = nullptr);
		static bool Draw(const char* name, float* value, int n);
		static bool Draw(const char* name, int* value, int n);
		static bool Draw(const char* name, uint32_t* value, int n);
		static bool Draw(const char* name, ImGuiDataType type, void* value, int n);
		static void PushHints(const EditorHints& hints);
		static void PopHints();
		static const EditorHints& Hints() { return _CurrentHints; }
	private:
		static inline char _CharBuffer[256];
		static inline char _CharBuffer2[256];
		static inline std::stack<EditorHints> _Hints;
		static inline EditorHints _CurrentHints;

		static bool _RenderDragScalar(const char* name, ImGuiDataType type, void* value, int n);
		static bool _RenderSlidingScalar(const char* name, ImGuiDataType type, void* value, int n);
	};
}