#pragma once
#include "Phobos/Editor/Windows/IEditorWindow.h"
#include "Phobos/Gameplay/HierarchyNode.h"
#include "Phobos/Gameplay/GameObject.h"

namespace Phobos::Editor::Windows {
	class HierarchyWindow : public IEditorWindow
	{
	private:
		inline static char _CharBuffer[256];
		void __RenderNode(Gameplay::GameObject object);
	public:
		void Render() override;
		const std::string& WindowName() const override { static std::string name = "Hierarchy"; return name; }
	};
}