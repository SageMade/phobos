#pragma once
#include "Phobos/Editor/Windows/IEditorWindow.h"
#include "Phobos/Graphics/ShaderMaterial.h"

namespace Phobos::Editor::Windows {
	class MaterialWindow : public IEditorWindow
	{
		inline static char _CharBuffer[256];
	public:
		static void RenderMaterial(const Graphics::ShaderMaterial::sptr& mat);

	private:
		static bool _RenderMaterialUniform(Graphics::ShaderMaterial::UniformData& uniform, EditorHints* hints = nullptr);
	public:
		void Render() override;
		const std::string& WindowName() const override { static std::string name = "Materials"; return name; }
	};
}
