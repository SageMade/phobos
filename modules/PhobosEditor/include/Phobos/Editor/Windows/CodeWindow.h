#pragma once
#include "Phobos/Editor/Windows/IEditorWindow.h"
#include <TextEditor.h>

namespace Phobos::Editor::Windows {
	TextEditor::LanguageDefinition CreateCppLang();

	class CodeWindow : public IEditorWindow {
	public:
		CodeWindow();
		virtual ~CodeWindow() = default;

		virtual void OnClose() override;

		virtual void Render() override;
		virtual const std::string& WindowName() const override;

		void SetLang(const TextEditor::LanguageDefinition& lang);
		const TextEditor::LanguageDefinition& GetLang() const;

		void SetFont(ImFont* font) { myEditor.SetFont(font); }
		ImFont* GetFont() const { return myEditor.GetFont(); }

		void OpenFile(const std::string& fileName);
		void NewFile();
		void SaveFile(const char* fileName);

		void SetSource(const char* source);
		std::string GetSource() const;

	protected:
		TextEditor myEditor;
		TextEditor::LanguageDefinition myLang;
		std::string myFilePath;
		bool isFileModified;

		mutable std::string myWindowTitle;

		ImGuiID mySaveFileId;
		ImGuiID myOpenFileId;

		void __SelectLanguage();
		void __HandleKeyboard();

		void __SaveFile();
		void __OpenFile();

		void __RenderMenuBar();
		void __RenderFileMenu();
		void __RenderOptionsMenu();

		static std::vector<TextEditor::LanguageDefinition> KnownLanguages;
	};
}