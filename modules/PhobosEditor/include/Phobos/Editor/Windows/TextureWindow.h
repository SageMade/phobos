#pragma once
#include "Phobos/Editor/Windows/IEditorWindow.h"
#include "Phobos/Graphics//ShaderMaterial.h"
#include "Phobos/Graphics/Texture2D.h"

namespace Phobos::Editor::Windows {
	class TextureWindow : public IEditorWindow
	{
	private:
		inline static char _CharBuffer[256];
		static bool _RenderTexture2D(Phobos::Graphics::Texture2D::sptr& uniform);
	public:
		void Render() override;
		const std::string& WindowName() const override { static std::string name = "Textures"; return name; }
	};
}
