#pragma once
#include <memory>
#include <string>

namespace Phobos::Editor::Windows {
	class Editor;

	class IEditorWindow {
	public:
		typedef std::shared_ptr<IEditorWindow> sptr;
		typedef std::unique_ptr<IEditorWindow> uptr;
		bool IsOpen;
		virtual ~IEditorWindow() = default;

		void Init();

		virtual void OnClose() { };

		virtual void Render() = 0;
		virtual const std::string& WindowName() const = 0;

	protected:
		void __LoadOpenState();
	};
}