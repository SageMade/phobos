#pragma once
#include "Phobos/Editor/Windows/IEditorWindow.h"

namespace entt {
	struct meta_handle;
	class meta_type;
	class meta_any;
}

namespace Phobos::Editor::Windows
{
	class InspectorWindow : public IEditorWindow
	{
	private:
		void _RenderMeta(entt::meta_type& meta, entt::meta_handle instance);
	public:
		void Render() override;
		const std::string& WindowName() const override { static std::string name = "Inspector"; return name; };
	};

}
