#pragma once
#include "Phobos/Graphics/Shader.h"
#include "Phobos/Editor/Windows/IEditorWindow.h"
#include <imgui.h>

namespace Phobos::Editor::Windows {
	class ShaderWindow : public IEditorWindow
	{
		inline static char _CharBuffer[256];
	public:
		static void RenderShader(const Phobos::Graphics::Shader::sptr& shader);
		static bool RenderUniformDefaultSelector(Phobos::Graphics::Shader::UniformInfo& uniform);
		static bool RenderUniformControl(const char* name, Phobos::Graphics::ShaderDataType type, void* data, int count = 1);
		void Render() override;
		const std::string& WindowName() const override { static std::string name = "Shaders"; return name; }
	};
}