#pragma once
#include <cstdint>
#include <glad/glad.h>

namespace Phobos::Graphics {
	/// <summary>
	/// We'll use this just to make it more clear what the intended usage of an attribute is in our code!
	/// </summary>
	enum class AttribUsage
	{
		Unknown = 0,
		Position,
		Color,
		Color1,   //
		Color2,   // Extras
		Color3,   //
		Texture,
		Texture1, //
		Texture2, // Extras
		Texture3, //
		Normal,
		Tangent,
		BiNormal,
		User0,    //
		User1,    //
		User2,    // Extras
		User3     //
	};

	/// <summary>
	/// This structure will represent the parameters passed to the glVertexAttribPointer commands
	/// </summary>
	struct BufferAttribute
	{
		/// <summary>
		/// The input slot to the vertex shader that will receive the data
		/// </summary>
		GLuint  Slot;
		/// <summary>
		/// The number of elements to be passed (ex 3 for a vec3)
		/// </summary>
		GLint   Size;
		/// <summary>
		/// The type of data to be passed (ex: GL_FLOAT for a vec3)
		/// </summary>
		GLenum  Type;
		/// <summary>
		/// Whether or not the data should be normalized into the 0-1 range (usually this is false)
		/// </summary>
		bool    Normalized;
		/// <summary>
		/// The total size of an element in this buffer
		/// </summary>
		GLsizei Stride;
		/// <summary>
		/// The offset from the start of an element to this attribute
		/// </summary>
		size_t Offset;
		/// <summary>
		/// The instancing divisor for this attribute
		/// </summary>
		uint32_t InstanceDivisor;

		/// <summary>
		/// The approximate usage for this attribute, does not get passed to OpenGL at all
		/// </summary>
		AttribUsage Usage;

		BufferAttribute(uint32_t slot, uint32_t size, GLenum type, bool normalized, GLsizei stride, size_t offset, AttribUsage usage = AttribUsage::Unknown, uint32_t instanceDivisor = 0) :
			Slot(slot), Size(size), Type(type), Normalized(normalized), Stride(stride), Offset(offset), Usage(usage), InstanceDivisor(instanceDivisor) { }
	};
}
