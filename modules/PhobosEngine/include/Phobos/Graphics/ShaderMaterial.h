#pragma once
#include <string>
#include <unordered_map>
#include "Phobos/Graphics/Shader.h"
#include "Phobos/Graphics/ITexture.h"
#include "Phobos/Graphics/Texture2D.h"
#include "Phobos/Graphics/TextureCubeMap.h"
#include "Phobos/Utils/Macros.h"
#include "Phobos/Utils/AssetDatabase.h"
#include <Phobos/EnumToString.h>
#include <cereal/cereal.hpp>

namespace Phobos::Editor::Windows {
	class MaterialWindow;
}

namespace Phobos::Graphics {
	class ShaderMaterial : public IAsset {
		SMART_MEMORY_MANAGED(ShaderMaterial)
	public:
		/// <summary>
		/// Represents information to be sent to a shader for runtime resources
		/// such as shaders
		/// </summary>
		struct ResourceBlock {
			int Handle;
			Phobos::Guid ResourceGuid;
		};

		ShaderMaterial();
		virtual ~ShaderMaterial();

		int RenderLayer;
		bool IsTransparent;
		bool DepthRead;
		bool DepthWrite;

		void Apply();
		void SetShader(const Shader::sptr shader);
		const Shader::sptr& GetShader() const { return _shader; }

		ShaderMaterial::sptr Clone() const;

		template <typename T>
		void Set(const std::string& name, const T& value) {
			const ShaderDataType type = GetShaderDataType<T>();
			if constexpr (GetShaderDataTypeCode(type) == ShaderDataTypecode::Texture) {
				ResourceBlock block;
				block.Handle = value->GetHandle();
				block.ResourceGuid = value->GetGUID();
				_resources[name] = value;
				Set(name, type, &block);
			} else {
				Set(name, type, &value);
			}
		}

		void Set(const std::string& name, ShaderDataType type, const void* value, size_t count = 1);
	protected:
		friend class cereal::access;
		friend class ::Phobos::Editor::Windows::MaterialWindow;

		Shader::sptr _shader;

		struct UniformData {
			std::string Name;
			int Location = -2;
			uint8_t Value[64];
			void* ArrayBlock = nullptr;
			size_t ArraySize;
			ShaderDataType Type = ShaderDataType::None;
			bool           HasEditor = false;

			~UniformData() {
				if (ArrayBlock != nullptr) {
					free(ArrayBlock);
				}
			}

			template <typename Archive>
			void save(Archive& ar) const {
				bool isResource = GetShaderDataTypeCode(Type) == ShaderDataTypecode::Texture;
				ar(
					CEREAL_NVP(Name),
					CEREAL_NVP(ArraySize),
					cereal::make_nvp("Type", Type)
				);
				if (Type != ShaderDataType::None) {
					if (ArraySize == 1) {
						size_t dataSize = ShaderDataTypeSize(Type);
						if constexpr (cereal::traits::is_text_archive<Archive>::value) {
							ar.saveBinaryValue(Value, dataSize, "Data");
						} else {
							ar(cereal::make_nvp("Data", cereal::binary_data((uint8_t*)Value, dataSize)));
						}
					} else {
						size_t dataSize = ShaderDataTypeSize(Type) * ArraySize;
						if constexpr (cereal::traits::is_text_archive<Archive>::value) {
							ar.saveBinaryValue((uint8_t*)ArrayBlock, dataSize, "Data");
						} else {
							ar(cereal::make_nvp("Data", cereal::binary_data((uint8_t*)ArrayBlock, dataSize)));
						}
					}
				}
			}
			template <typename Archive>
			void load(Archive& ar) {
				ar(
					CEREAL_NVP(Name),
					CEREAL_NVP(ArraySize),
					cereal::make_nvp("Type", Type)
				);
				if (Type != ShaderDataType::None) {
					if (ArraySize == 1) {
						size_t dataSize = ShaderDataTypeSize(Type);
						if constexpr (cereal::traits::is_text_archive<Archive>::value) {
							ar.loadBinaryValue(Value, dataSize, "Data");
						} else {
							ar(cereal::make_nvp("Data", cereal::binary_data((uint8_t*)Value, dataSize)));
						}
					} else {
						size_t dataSize = ShaderDataTypeSize(Type) * ArraySize;
						ArrayBlock = malloc(dataSize);
						if constexpr (cereal::traits::is_text_archive<Archive>::value) {
							ar.loadBinaryValue((uint8_t*)ArrayBlock, dataSize, "Data");
						} else {
							ar(cereal::make_nvp("Data", cereal::binary_data((uint8_t*)ArrayBlock, dataSize)));
						}
					}
				}
				bool isResource = GetShaderDataTypeCode(Type) == ShaderDataTypecode::Texture;
				if (isResource) {
					ResourceBlock& blk = *(ResourceBlock*)Value;
					ITexture::sptr tex = std::dynamic_pointer_cast<ITexture>(GetAsset());
					blk.Handle = tex == nullptr ? 0 : tex->GetHandle();
				}
			}

			inline bool IsResource() const {
				return GetShaderDataTypeCode(Type) == ShaderDataTypecode::Texture;
			}

			inline IAsset::sptr GetAsset() const {
				bool isResource = IsResource();
				IAsset::sptr result = nullptr;
				if (isResource) {
					ResourceBlock& blk = *(ResourceBlock*)Value;
					switch (Type) {
					case ShaderDataType::Tex2D:
						result = Utils::AssetDatabase::Get().FindByGuid<Graphics::Texture2D>(blk.ResourceGuid); break;
					case ShaderDataType::TexCube:
						result = Utils::AssetDatabase::Get().FindByGuid<Graphics::TextureCubeMap>(blk.ResourceGuid); break;
					default:
						LOG_WARN("Currently not handling resource type {} in material deserialization", Type);
					}
				}
				return result;
			}
		};

		std::map<std::string, UniformData> _data;
		std::map<std::string, IAsset::sptr> _resources;

		UniformData& _GetUniform(const std::string& name, entt::hashed_string::value_type* hashOut = nullptr);

		template <typename Archive>
		void save(Archive& ar) const
		{
			ar(
				cereal::base_class<IAsset>(this),
				CEREAL_NVP(RenderLayer),
				CEREAL_NVP(IsTransparent),
				CEREAL_NVP(DepthRead),
				CEREAL_NVP(DepthWrite),
				cereal::make_nvp("Shader", _shader->GetGUID())
			);
			ar(cereal::make_nvp("Fields", _data));
		}

		template <typename Archive>
		void load(Archive& ar)
		{
			_data.clear();
			_resources.clear();
			Phobos::Guid shaderGuid;
			ar(
				cereal::base_class<IAsset>(this),
				CEREAL_NVP(RenderLayer),
				CEREAL_NVP(IsTransparent),
				CEREAL_NVP(DepthRead),
				CEREAL_NVP(DepthWrite),
				cereal::make_nvp("Shader", shaderGuid)
			);
			Shader::sptr shader = Phobos::Utils::AssetDatabase::Get().FindByGuid<Shader>(shaderGuid);

			ar(cereal::make_nvp("Fields", _data));

			// Bind handles for resources so they don't get cleaned up
			for (auto& [hash, data] : _data) {
				if (data.IsResource()) {
					_resources[hash] = data.GetAsset();
				}
			}
			SetShader(shader);
		}
	};

}
CEREAL_REGISTER_TYPE(::Phobos::Graphics::ShaderMaterial);
CEREAL_REGISTER_POLYMORPHIC_RELATION(::Phobos::IAsset, ::Phobos::Graphics::ShaderMaterial);
