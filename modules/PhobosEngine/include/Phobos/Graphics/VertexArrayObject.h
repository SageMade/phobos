#pragma once
#include <glad/glad.h>
#include <cstdint>
#include <vector>
#include <memory>

#include "Phobos/Graphics/VertexBuffer.h"
#include "Phobos/Graphics/IndexBuffer.h"
#include "Phobos/Graphics/BufferAttribute.h"
#include "Phobos/Utils/IAsset.h"
#include "Phobos/EnumToString.h"

namespace Phobos::Graphics {

	ENUM(RenderMode, uint32_t,
		 PointList     = 0,
		 LineList      = 1,
		 LineStrip     = 2,
		 TriangleList  = 3,
		 TriangleStrip = 4,
		 TriangleFan   = 5,

		 Max = TriangleFan
	);

	constexpr uint32_t GetElementCount(RenderMode mode, uint32_t vertexCount) {
		switch (mode) {
			case RenderMode::PointList: return vertexCount;
			case RenderMode::LineList: return vertexCount / 2;
			case RenderMode::LineStrip: return vertexCount - 1;
			case RenderMode::TriangleList: return vertexCount / 3;
			case RenderMode::TriangleStrip: return vertexCount - 2;
			case RenderMode::TriangleFan: return vertexCount - 1;
			default: return 0;
		}
	}
	
	const GLenum GL_RENDERMODE_MAP[*(RenderMode::Max) + 1] = {
		GL_POINTS,
		GL_LINES,
		GL_LINE_STRIP,
		GL_TRIANGLES,
		GL_TRIANGLE_STRIP,
		GL_TRIANGLE_FAN
	};

	/// <summary>
	/// The Vertex Array Object wraps around an OpenGL VAO and basically represents all of the data for a mesh
	/// </summary>
	class VertexArrayObject final : public IAsset
	{
	public:
		typedef std::shared_ptr<VertexArrayObject> sptr;
		template <typename ... TArgs>
		static inline sptr Create(TArgs&&... args) {
			return std::make_shared<VertexArrayObject>(std::forward<TArgs>(args)...);
		}
		// We'll disallow moving and copying, since we want to manually control when the destructor is called
		// We'll use these classes via pointers
		VertexArrayObject(const VertexArrayObject& other) = delete;
		VertexArrayObject(VertexArrayObject&& other) = delete;
		VertexArrayObject& operator=(const VertexArrayObject& other) = delete;
		VertexArrayObject& operator=(VertexArrayObject&& other) = delete;

	public:
		/// <summary>
		/// Creates a new empty Vertex Array Object
		/// </summary>
		VertexArrayObject();
		// Destructor does not need to be virtual due to the use of the final keyword
		~VertexArrayObject();

		/// <summary>
		/// Sets a debug name for this VAO, making debug messages clearer
		/// </summary>
		/// <param name="name">The new name of the object</param>
		void SetDebugName(const std::string& name);

		/// <summary>
		/// Sets the index buffer for this VAO, note that for now, this will not delete the buffer when the VAO is deleted, more on that later
		/// </summary>
		/// <param name="ibo">The index buffer to bind to this VAO</param>
		void SetIndexBuffer(const IndexBuffer::sptr& ibo);
		/// <summary>
		/// Adds a vertex buffer to this VAO, with the specified attributes
		/// </summary>
		/// <param name="buffer">The buffer to add (note, does not take ownership, you will still need to delete later)</param>
		/// <param name="attributes">A list of vertex attributes that will be fed by this buffer</param>
		void AddVertexBuffer(const VertexBuffer::sptr& buffer, const std::vector<BufferAttribute>& attributes);

		/// <summary>
		/// Binds this VAO as the source of data for draw operations
		/// </summary>
		void Bind() const;
		/// <summary>
		/// Unbinds the currently bound VAO
		/// </summary>
		static uint32_t UnBind();

		void Render(RenderMode mode = RenderMode::TriangleList) const;

	protected:
		// Helper structure to store a buffer and the attributes
		struct VertexBufferBinding
		{
			VertexBuffer::sptr Buffer;
			std::vector<BufferAttribute> Attributes;
			GLuint BindingIndex;
		};

		// The index buffer bound to this VAO
		IndexBuffer::sptr _indexBuffer;
		// The vertex buffers bound to this VAO
		std::vector<VertexBufferBinding> _vertexBuffers;

		GLsizei _vertexCount;
		GLsizei _instanceCount;

		inline static uint32_t __BoundHandle;
	};
}