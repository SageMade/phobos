#pragma once
#include <glad/glad.h>
#include <memory>
#include <algorithm>

#include <string>               // for std::string
#include <unordered_map>        // for std::unordered_map
#include <GLM/glm.hpp>          // for our GLM types
#include <GLM/gtc/type_ptr.hpp> // for glm::value_ptr
#include <cereal/cereal.hpp>
#include "Phobos/Logging.h"            // for the logging functions
#include "Phobos/Utils/guid.hpp"
#include "Phobos/Utils/IAsset.h"
#include "Phobos/Graphics/ShaderEnums.h"

namespace Phobos {
	struct EditorHints;
}

namespace Phobos::Graphics {
	/// <summary>
/// This class will wrap around an OpenGL shader program
/// </summary>
	class Shader final : public IAsset
	{
	public:
		typedef std::shared_ptr<Shader> sptr;
		static inline sptr Create() {
			return std::make_shared<Shader>();
		}

	public:
		// We'll disallow moving and copying, since we want to manually control when the destructor is called
		// We'll use these classes via pointers
		Shader(const Shader& other) = delete;
		Shader(Shader&& other) = delete;
		Shader& operator=(const Shader& other) = delete;
		Shader& operator=(Shader&& other) = delete;

	public:
		struct UniformInfo {
			std::string    Name;
			int            Location;
			ShaderDataType Type;
			int            ArraySize;

			uint8_t        DefaultValueBuffer[64];
			void* DefaultArrayData;
			bool           Configurable;
			EditorHints* EditorHints;
		};

		struct UniformBlockInfo {
			std::string Name;
			int         Binding;
			int         SizeInBytes;
			int         NumVariables;
			int         BlockIndex;

			std::vector<UniformInfo> SubUniforms;
		};

	public:
		/// <summary>
		/// Creates a new empty shader object
		/// </summary>
		Shader();

		virtual ~Shader() override;

		/// <summary>
		/// Loads a single shader stage into this shader object (ex: Vertex Shader or Fragment Shader)
		/// </summary>
		/// <param name="source">The source code of the shader to load</param>
		/// <param name="type">The stage to load (GL_VERTEX_SHADER or GL_FRAGMENT_SHADER)</param>
		/// <returns>True if the shader is loaded, false if there was an issue</returns>
		bool LoadShaderPart(const char* source, ShaderStageType type);
		/// <summary>
		/// Loads a single shader stage into this shader object (ex: Vertex Shader or Fragment Shader) from an external file (in res)
		/// </summary>
		/// <param name="path">The relative path to the file containing the source</param>
		/// <param name="type">The stage to load (GL_VERTEX_SHADER or GL_FRAGMENT_SHADER)</param>
		/// <returns>True if the shader is loaded, false if there was an issue</returns>
		bool LoadShaderPartFromFile(const char* path, ShaderStageType type);

		/// <summary>
		/// Loads a shader from a file containing multiple shader parts
		/// </summary>
		/// <param name="filename">The path to the file to load</param>
		void LoadFromFile(const std::string& filename) override;

		/// <summary>
		/// Links the vertex and fragment shader, and allows this shader program to be used
		/// </summary>
		/// <returns>True if the linking was successful, false if otherwise</returns>
		bool Link();

		/// <summary>
		/// Binds this shader for use
		/// </summary>
		void Bind();
		/// <summary>
		/// Unbinds all shader programs
		/// </summary>
		static void UnBind();

	public:
		int GetUniformLocation(const std::string& name);

		template <typename T>
		void SetUniform(const std::string& name, const T& value, bool transposed = false) {
			int location = GetUniformLocation(name);
			if (location != -1) {
				SetUniform(location, GetShaderDataType<T>(), &value, 1, transposed);
			}
		}
		template <typename T>
		void SetUniform(int location, const T& value, bool transposed = false) {
			if (location != -1) {
				SetUniform(location, GetShaderDataType<T>(), &value, 1, transposed);
			}
		}

		template <typename T>
		void SetDefault(const std::string& name, const T& value) {
			int location = GetUniformLocation(name);
			auto it = std::find_if(_uniforms.begin(), _uniforms.end(), [&](UniformInfo& b) { return b.Name == name; });
			if (it != _uniforms.end()) {
				LOG_ASSERT(GetShaderDataType<T>() == it->Type, "Value does not match the value of the uniform");

				if (it->ArraySize > 1) {
					if (it->DefaultArrayData == nullptr) {
						it->DefaultArrayData = malloc(ShaderDataTypeSize(it->Type) * it->ArraySize);
						memset(it->DefaultArrayData, 0, ShaderDataTypeSize(it->Type) * it->ArraySize);
					}
					memcpy(it->DefaultArrayData, &value, ShaderDataTypeSize(it->Type));
				} else {
					memcpy(it->DefaultValueBuffer, &value, ShaderDataTypeSize(it->Type));
				}
			} else {
				LOG_WARN("No uniform named \"{}\" found in shader \"{}\"", name, GetName());
			}
		}

		void SetUniform(int location, ShaderDataType type, const void* data, int count = 1, bool transposed = false);

		bool FindUniform(const std::string& name, UniformInfo& out, bool includeNonConfigurable = false);
		void SetUniformConfigurable(const std::string& name, bool isConfigurable = true);
		void SetUniformEditorHints(const std::string& name, const EditorHints& hints);
		EditorHints* GetEditorHints(int location);

		const std::vector<UniformInfo>& GetUniforms() const { return _uniforms; }
		std::vector<UniformInfo>& GetUniforms() { return _uniforms; }
		const std::vector<UniformBlockInfo>& GetUniformBlocks() const { return _uniformBlocks; }
		std::vector<UniformBlockInfo>& GetUniformBlocks() { return _uniformBlocks; }

		void SetName(const std::string& name) override;

	protected:
		GLuint _shaders[(int)ShaderStageType::Max + 1];

		std::string _filePath;

		std::unordered_map<ShaderStageType, std::string> _shaderSources;
		std::unordered_map<std::string, int> _uniformLocs;
		std::vector<UniformInfo>             _uniforms;
		std::vector<UniformBlockInfo>        _uniformBlocks;

		void _Introspect();
		void _InstrospectUniforms();
		void _IntrospectUniformBlocks();

		static std::map<ShaderStageType, std::string> _Preprocess(const std::string& source, std::map<std::string, EditorHints>* editorHints = nullptr);

		friend class cereal::access;

		template <typename Archive>
		void save(Archive& ar) const
		{
			bool embeddedShader = _filePath.empty();
			ar(
				cereal::base_class<IAsset>(this),
				cereal::make_nvp("embedded", embeddedShader)
			);
			if (!embeddedShader) {
				ar(cereal::make_nvp("filename", _filePath));
			} else {
				ar(cereal::make_nvp("sources", _shaderSources));
			}
		}

		template <typename Archive>
		void load(Archive& ar)
		{
			bool embedded = false;
			ar(
				cereal::base_class<IAsset>(this),
				cereal::make_nvp("embedded", embedded)
			);
			if (!embedded) {
				ar(cereal::make_nvp("filename", _filePath));
				if (_filePath != "") {
					LoadFromFile(_filePath);
				}
			} else {
				ar(cereal::make_nvp("sources", _shaderSources));
				for (auto& kvp : _shaderSources) {
					LoadShaderPart(kvp.second.c_str(), kvp.first);
				}
				Link();
			}
		}
	};
}

CEREAL_REGISTER_TYPE(::Phobos::Graphics::Shader);
CEREAL_REGISTER_POLYMORPHIC_RELATION(::Phobos::IAsset, ::Phobos::Graphics::Shader);
