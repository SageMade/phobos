#pragma once
#include "Phobos.h"
#include <memory>
#include <ENTT/entity/registry.hpp>
#include <GLM/glm.hpp>

// We can declare the name and assume it will get included later, helps avoid circular dependencies
namespace Phobos
{
	namespace Gameplay{
		class GameScene;
	}

	class Application
	{
	public:
		static Application& Instance()
		{
			static Application instance;
			return instance;
		}

		bool HasFocus() const;

		entt::registry& ActiveRegistry();

		GLFWwindow* Window;
		std::shared_ptr<Gameplay::GameScene> ActiveScene;
		bool GameHasFocus;
		glm::ivec2 WindowSize;
	};
	
}
