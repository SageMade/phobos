#pragma once
#include <cereal/archives/json.hpp>
#include "ENTT/entt.hpp"
#include "ENTT/core/type_info.hpp"
#include "Phobos/Utils/Macros.h"
#include <bullet/btBulletDynamicsCommon.h>

#include "Phobos/Physics/PhysicsWorld.h"
#include "Phobos/Physics/PhysicsSystem.h"
#include "Phobos/Gameplay/GameObject.h"

namespace Phobos::Gameplay {
	class HierarchyNode;
	typedef entt::basic_snapshot_loader<entt::entity> SceneDeserializer;
	typedef entt::basic_snapshot<entt::entity> SceneSerializer;
	typedef cereal::JSONOutputArchive SceneOutputArchiveType;
	typedef cereal::JSONInputArchive SceneInputArchiveType;
	
	/// <summary>
	/// Represents a callback that may be used to customize how entity stamping works between registries
	/// </summary>
	typedef void(*StampFunction)(const entt::registry& from, const entt::entity src, entt::registry& to, const entt::entity dst);
	/// <summary>
	/// Represents a callback that may be used to customize how entity stamping works between registries
	/// </summary>
	typedef entt::meta_handle(*GetComponentFunction)(const entt::registry& from, entt::entity entity);
	/// <summary>
	/// Represents a callback that is used to determined if an entity has a component
	/// </summary>
	typedef bool(*HasComponentFunction)(const entt::registry& from, entt::entity entity);

	typedef void(*SerializeFunction)(const SceneSerializer& serializer, SceneOutputArchiveType& archive);
	typedef void(*DeserializeFunction)(const SceneDeserializer& serializer, SceneInputArchiveType& archive);

	struct DeleteObjectTag { };

	class GameScene final
	{
		SMART_MEMORY_MANAGED(GameScene)

	public:
		std::string Name;

		GameScene(const std::string& name = "<default>");
		~GameScene() = default;

		GameObject MainCamera;

		const std::shared_ptr<Physics::PhysicsWorld>& GetPhysics() { return _physics; }

		GameObject CreateEntity(const std::string& name = "");
		GameObject CreateEntity(const std::string& name, const GameObject parent);
		GameObject CreateEntity(const GameObject prefab, const std::string& name = "");

		void Destroy(GameObject object);

		GameObject FindFirst(const std::string& name);

		entt::registry& Registry() { return _registry; }

		/// <summary>
		/// Perform any tasks that should happen at the end of a loop, such as deleting queued objects
		/// </summary>
		void Poll();

		/// <summary>
		/// Creates a new entity in the <i>to</i> registry, copying the components from the <i>src</i> entity in the from registry
		/// </summary>
		/// <param name="from">The source registry to copy the object from</param>
		/// <param name="src">The source entity within the <i>from</i> registry to copy</param>
		/// <param name="to">The destination registry to store the entity in</param>
		/// <returns>A handle for the newly created entity</returns>
		static entt::handle StampEntity(const entt::registry& from, entt::entity src, entt::registry& to);

		static bool HasComponent(const entt::registry& reg, entt::entity entity, entt::id_type meta_id);
		static entt::meta_handle GetComponent(const entt::registry& reg, entt::entity entity, entt::id_type meta_id);

		void NotifyHierarchyDirty();
		void UpdateTransforms();

		template <typename Type>
		static void RegisterComponentType(StampFunction stampOverride = nullptr) {
			auto pair = _registeredTypes.emplace(entt::type_seq<Type>::value());
			if (pair.second) {
				_TypeFunctions funcs = _TypeFunctions();
				funcs.Stamp = stampOverride != nullptr ? stampOverride : &_DefaultComponentStamp<Type>;
				funcs.Get   = &_DefaultComponentGet<Type>;
				funcs.Has   = [](const entt::registry& reg, entt::entity entity) { return reg.has<Type>(entity); };
				funcs.Serialize = [](const SceneSerializer& serializer, SceneOutputArchiveType& archive) { serializer.component<Type>(archive); };
				funcs.Deserialize = [](const SceneDeserializer& serializer, SceneInputArchiveType& archive) { serializer.component<Type>(archive); };
				_typeFuncs[entt::type_seq<Type>::value()] = funcs;
			}
		}
		static entt::registry& Prefabs() { return _prefabRegistry; }
		static const std::set<entt::id_type>& RegisteredTypes() { return _registeredTypes; }

		bool Save(const std::string& path) const;
		bool Load(const std::string& path);

	private:
		entt::registry _registry;
		std::set<GameObject> _deletionQueue;
		std::shared_ptr<Physics::PhysicsWorld> _physics;

		bool _isHierarchyDirty;
		
		friend class Phobos::Physics::PhysicsSystem;

		struct _TypeFunctions {
			StampFunction        Stamp;
			GetComponentFunction Get;
			HasComponentFunction Has;
			SerializeFunction    Serialize;
			DeserializeFunction  Deserialize;
		};

		inline static entt::registry _prefabRegistry;
		inline static std::unordered_map<entt::id_type, _TypeFunctions> _typeFuncs;
		inline static std::set<entt::id_type> _registeredTypes;

		void __OnHierarchyUpdated(entt::registry& reg, entt::entity entity);
		void __OnHierarchyNodeDestroyed(entt::registry& reg, entt::entity entity);
		void __OnTransformMarkedDirty(entt::registry& reg, entt::entity entity);
		void __OnColliderAdded(entt::registry& reg, entt::entity e);
		bool __SortOnHierarchy(const entt::entity lhs, const entt::entity rhs);
		bool __SortOnHierarchyInverted(const entt::entity lhs, const entt::entity rhs);

		template <typename T>
		static void _DefaultComponentStamp(const entt::registry& from, const entt::entity src, entt::registry& to, const entt::entity dst) {
			to.emplace_or_replace<T>(dst, from.get<T>(src));
		}

		template <typename T>
		static entt::meta_handle _DefaultComponentGet(const entt::registry& reg, entt::entity entity) {
			return reg.get<T>(entity);
		}
	};

}
