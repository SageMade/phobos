#pragma once
#include <memory>
#include <ENTT/entt.hpp>
#include <typeindex>
#include <cereal/cereal.hpp>
#include "Phobos/Utils/GlmCerealization.h"
#include <cereal/archives/binary.hpp>
#include <cereal/archives/portable_binary.hpp>
#include <cereal/archives/xml.hpp>
#include <cereal/archives/json.hpp>
#include <cereal/types/polymorphic.hpp>
#include "Phobos/Utils/guid.hpp"
#include "Phobos/Utils/StringUtils.h"

namespace Phobos::Gameplay { 
	struct BehaviourBinding;

	/*
	 * Represents a behaviour that can be tied to a single GameObject
	 */
	class IBehaviour {
	public:
		/*
		 * Whether or not this component will fire it's events
		 */
		bool    Enabled = true;
		/*
		 * A UUIDV4 id for the script instance
		 */
		Phobos::Guid Guid   = Phobos::Guid::New();
		
		virtual ~IBehaviour() = default;

		/*
		 * Invoked when the behaviour is added to the scene, or the scene has been loaded
		 * @param entity The entity that the behaviour is bound to
		 */
		virtual void OnLoad(entt::handle entity) {}
		/*
		 * Invoked when the behaviour is removed from the entity, or the scene is unloaded
		 * @param entity The entity that the behaviour is bound to
		 */
		virtual void OnUnload(entt::handle entity) {}
		/*
		 * Invoked during the variable rate update. This is generally where we want to add our updates.
		 * To get the time since the last update, use florp::app::Timing::DeltaTime
		 * @param entity The entity that the behaviour is bound to
		 */
		virtual void Update(entt::handle entity) {}
		/*
		 * Invoked during the fixed rate update phase.
		 * To get the approximate time since the last fixed update, use florp::app::Timing::FixedTimeStep
		 * @param entity The entity that the behaviour is bound to
		 */
		virtual void FixedUpdate(entt::handle entity) {}
		/*
		 * Invoked during the variable rate update. This is called after all behaviours/layers have called Update
		 * To get the time since the last update, use florp::app::Timing::DeltaTime
		 * @param entity The entity that the behaviour is bound to
		 */
		virtual void LateUpdate(entt::handle entity) {}
		/*
		 * Invoked during the GUI render phase.
		 * @param entity The entity that the behaviour is bound to
		 */
		virtual void RenderGUI(entt::handle entity) {}

		/// <summary>
		/// Invoked before the physics pass for this object
		/// </summary>
		/// <param name="entity">The entity the behaviour is bound to</param>
		virtual void PrePhysics(entt::handle entity) {}
		/// <summary>
		/// Invoked after the physics pass for this object
		/// </summary>
		/// <param name="entity">The entity the behaviour is bound to</param>
		virtual void PostPhysics(entt::handle entity) {}

		/// <summary>
		/// Gets the hashed string name from this behaviour
		/// </summary>
		/// <returns></returns>
		inline virtual entt::hashed_string::hash_type GetTypeNameHash() {
			return entt::hashed_string(Utils::StringTools::SanitizeClassName(typeid(*this).name()).c_str()).value();
		}

		inline virtual entt::meta_handle GetHandle() = 0;

		inline virtual const char* GetTypeName() { return typeid(*this).name(); }

	protected:
		friend struct BehaviourBinding;

		IBehaviour() = default;

		/*
		 * Invoked once for each type of script, to register it with the entt reflection system
		 */
		virtual void RegisterMeta() = 0;

		friend class cereal::access;

		template <class Archive>
		void serialize(Archive& ar) {
			ar(CEREAL_NVP(Enabled));
			ar(CEREAL_NVP(Guid));
		}
	};

	/*
	 * The component added to an entt entity to connect a behaviour to a specific entity
	 */
	struct BehaviourBinding {
		std::vector<std::shared_ptr<IBehaviour>> Behaviours;

		/*
		 * Binds an IBehaviour interface to the given entt entity
		 * @param T The type of behaviour to add
		 * @param TArgs The argument types to forward to the behaviour's constructor
		 * @param registry The registry that the entity is part of
		 * @param entity The entity to add the behaviour to
		 * @param args The arguments to forward to the behaviour's constructor
		 */
		template <typename T, typename ... TArgs, typename = typename std::enable_if<std::is_base_of<IBehaviour, T>::value>::type>
		static std::shared_ptr<T> Bind(entt::handle entity, TArgs&&... args) {
			return __Bind<T>(entity, true, std::forward<TArgs>(args)...);
		}

		/*
		 * Binds an IBehaviour interface to the given entt entity, setting it to disabled by default
		 * @param T The type of behaviour to add
		 * @param TArgs The argument types to forward to the behaviour's constructor
		 * @param registry The registry that the entity is part of
		 * @param entity The entity to add the behaviour to
		 * @param args The arguments to forward to the behaviour's constructor
		 */
		template <typename T, typename ... TArgs, typename = typename std::enable_if<std::is_base_of<IBehaviour, T>::value>::type>
		static std::shared_ptr<T> BindDisabled(entt::handle entity, TArgs&&... args) {
			return __Bind<T>(entity, false, std::forward<TArgs>(args)...);
		}

		/*
		 * Checks whether the given entity has a behaviour of the given type
		 * @param T The type of behaviour to check for
		 * @param registry The registry that the entity is part of
		 * @param entity The entity to check
		 * @returns True if a behaviour of type T is attached to entity, or false if otherwise
		 */
		template <typename T, typename = typename std::enable_if<std::is_base_of<IBehaviour, T>::value>::type>
		static bool Has(entt::handle entity) {
			// Check to see if the entity has a behaviour binding attached
			if (entity.has<BehaviourBinding>()) {
				// Get the binding component
				const auto& binding = entity.get<BehaviourBinding>();
				// Iterate over all the pointers in the binding list
				for (const auto& ptr : binding.Behaviours) {
					// If the pointer type matches T, we return true
					if (std::type_index(typeid(*ptr.get())) == std::type_index(typeid(T))) {
						return true;
					}
				}
				return false;
			} else return false;
		}

		/*
		 * Gets the behaviour with the given type from the entity, or nullptr if none exists
		 * @param T The type of behaviour to check for
		 * @param registry The registry that the entity is part of
		 * @param entity The entity to search
		 * @returns The behaviour of type T that is attached to entity, or nullptr if no behaviour of that type is attached
		 */
		template <typename T, typename = typename std::enable_if<std::is_base_of<IBehaviour, T>::value>::type>
		static std::shared_ptr<T> Get(entt::handle entity) {
			// Check to see if the entity has a behaviour binding attached
			if (entity.has<BehaviourBinding>()) {
				// Get the binding component
				const auto& binding = entity.get<BehaviourBinding>();
				// Iterate over all the pointers in the binding list
				for (const auto& ptr : binding.Behaviours) {
					// If the pointer type matches T, we return that behaviour, making sure to cast it back to the requested type
					if (std::type_index(typeid(*ptr.get())) == std::type_index(typeid(T))) {
						return std::dynamic_pointer_cast<T>(ptr);
					}
				}
				return nullptr;
			} else return nullptr;
		}

	private:

		friend class cereal::access;

		template <class Archive>
		void serialize(Archive& ar) {
			ar(CEREAL_NVP(Behaviours));
		}

		inline static bool _isBaseRegistered = false;
		inline static std::set<std::type_index> _registered;

		template <typename T, typename ... TArgs, typename = typename std::enable_if<std::is_base_of<IBehaviour, T>::value>::type>
		static std::shared_ptr<T> __Bind(entt::handle entity, bool enabled, TArgs&&... args) {

			// Static CTOR to add the IBehavior data to the registry
			if (!_isBaseRegistered)
			{
				using namespace entt::literals;
				entt::meta<IBehaviour>().type("IBehaviour"_hs).
					data<&IBehaviour::Enabled>("Enabled"_hs).
					func<&IBehaviour::OnLoad>("OnLoad"_hs).
					func<&IBehaviour::OnUnload>("OnUnload"_hs).
					func<&IBehaviour::Update>("Update"_hs).
					func<&IBehaviour::FixedUpdate>("FixedUpdate"_hs).
					func<&IBehaviour::LateUpdate>("LateUpdate"_hs).
					func<&IBehaviour::RenderGUI>("RenderGUI"_hs);
				_isBaseRegistered = true;
			}

			// Get the binding component
			BehaviourBinding& binding = entity.get_or_emplace<BehaviourBinding>();
			// Make a new behaviour, forwarding the arguments
			const std::shared_ptr<IBehaviour> behaviour = std::make_shared<T>(std::forward<TArgs>(args)...);

			// If this is the first time we bound the behaviour, register it with the reflection system
			if (_registered.emplace(std::type_index(typeid(T))).second) {
				behaviour->RegisterMeta();
			}

			// Append it to the binding component's storage, and invoke the OnLoad
			binding.Behaviours.push_back(behaviour);
			behaviour->Enabled = enabled;
			behaviour->OnLoad(entity);
			return std::dynamic_pointer_cast<T>(behaviour);
		}


	};
}

#define RegisterBehaviourType(TypeName) \
	CEREAL_REGISTER_TYPE(TypeName); \
	CEREAL_REGISTER_POLYMORPHIC_RELATION(::Phobos::Gameplay::IBehaviour, TypeName);