#pragma once
#include <string>
#include <ENTT/entt.hpp>
#include <cereal/cereal.hpp>
#include "Phobos/Utils/guid.hpp"

namespace Phobos::Gameplay {
	/// <summary>
	/// Represents information associated with a game object within our scene, including hierarchy information
	/// </summary>
	struct GameObjectTag final
	{		
		std::string Name;
		uint32_t    HashedName;
		Phobos::Guid    GUID;
		
		GameObjectTag() noexcept;
		GameObjectTag(const std::string& name);
		
		GameObjectTag(const GameObjectTag& other);
		GameObjectTag(GameObjectTag&& other);

		GameObjectTag& operator=(GameObjectTag&& other);
		GameObjectTag& operator=(const GameObjectTag& other);
		
		~GameObjectTag();
		// TODO: we could expand this in the future for properties that all game objects should have

	private:
		inline void __Copy(const GameObjectTag& other);

		inline void __Move(GameObjectTag&& other);
		friend class cereal::access;

		template <class Archive>
		void save(Archive& ar) const {
			ar(CEREAL_NVP(Name));
			ar(CEREAL_NVP(GUID));
		}
		template <class Archive>
		void load(Archive& ar) {
			ar(CEREAL_NVP(Name));
			ar(CEREAL_NVP(GUID));
			HashedName = entt::hashed_string::value(Name.c_str());
		}
	};
}
