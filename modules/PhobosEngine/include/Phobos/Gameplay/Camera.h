#pragma once

#include <memory>
#include <cereal/cereal.hpp>
#include <GLM/glm.hpp>

namespace Phobos::Gameplay {
	/// <summary>
	/// Represents a simple perspective camera for use by first person or third person games
	/// </summary>
	class Camera
	{
	public:
		Camera();
		virtual ~Camera() = default;

		/// <summary>
		/// Gets whether this camera is in orthographic projection mode
		/// </summary>
		bool GetIsOrtho() const { return _isOrtho; }
		/// <summary>
		/// Sets whether this camera is in orthographic projection mode
		/// </summary>
		/// <param name="isOrtho">True if the camera is in orthographic mode, false for perspective</param>
		void SetIsOrtho(bool isOrtho);
		/// <summary>
		/// Toggles the camera's orthographic state, enabling it if it' disabled and disabling it if it is enabled
		/// </summary>
		inline void ToggleOrtho() { SetIsOrtho(!_isOrtho); }
		/// <summary>
		/// Gets the distance from the camera to the top/bottom of the orthographic box when in ortho mode
		/// </summary>
		float GetOrthoHeight() const { return _orthoHeight; }
		/// <summary>
		/// Sets the distance from the camera to the top/bottom of the orthographic box when in ortho mode
		/// </summary>
		/// <param name="orthoHeight">The distance from the camera to the top/bottom of the orthographic box when in ortho mode</param>
		void SetOrthoHeight(float orthoHeight);

		/// <summary>
		/// Notifies this camera that the window has resized, and updates our projection matrix
		/// </summary>
		/// <param name="windowWidth">The new width of the window</param>
		/// <param name="windowHeight">The new height of the window</param>
		void ResizeWindow(int windowWidth, int windowHeight);
		/// <summary>
		/// Sets the vertical field of view in radians for this camera
		/// </summary>
		void SetFovRadians(float value);
		/// <summary>
		/// Sets the vertical field of view in degrees for this camera
		/// </summary>
		void SetFovDegrees(float value);
		/// <summary>
		/// Gets the field of view of the camera, in degrees
		/// </summary>
		float GetFovDegrees() const { return glm::degrees(_fovRadians); }

		/// <summary>
		/// Gets the projection matrix for this camera
		/// </summary>
		const glm::mat4& GetProjection() const { return _projection; }

		/// <summary>
		/// Gets whether or not this camera is the main camera for a scene
		/// </summary>
		/// <returns></returns>
		bool GetIsMainCamera();

	protected:
		bool  _isMainCamera;
		bool  _isOrtho;
		float _orthoHeight;

		float _nearPlane;
		float _farPlane;
		float _fovRadians;
		float _aspectRatio;

		glm::mat4 _projection;

		// A dirty flag that indicates whether we need to re-calculate our projection matrix
		mutable bool _isDirty;

		// Recalculates the projection matrix
		void __CalculateProjection();

		friend class cereal::access;

		template <class Archive>
		void serialize(Archive& ar) {
			ar(
				cereal::make_nvp("MainCamera", _isMainCamera),
				cereal::make_nvp("Ortho", _isOrtho),
				cereal::make_nvp("OrthoHeight", _orthoHeight),
				cereal::make_nvp("NearPlane", _nearPlane),
				cereal::make_nvp("FarPlane", _farPlane),
				cereal::make_nvp("FOV_Radians", _fovRadians)
			);
		}
	};
}