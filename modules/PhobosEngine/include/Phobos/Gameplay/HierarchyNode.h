#pragma once
#include <ENTT/entity/handle.hpp>
#include <cereal/cereal.hpp>

namespace Phobos::Gameplay {

	struct HierarchyDirtyFlag final { };

	class HierarchyNode final
	{
	public:
		entt::entity Parent;
		entt::entity FirstChild;
		entt::entity Previous;
		entt::entity Next;
		bool         ExistsInHierachy;

		HierarchyNode() :
			Parent(entt::null),
			FirstChild(entt::null),
			Previous(entt::null),
			Next(entt::null),
			ExistsInHierachy(false) { }

		static void AddChild(entt::handle parent, entt::handle child);
		static void SetChildIndex(entt::handle child, size_t index);
		static void MoveChildUp(entt::handle child);
		static void MoveChildDown(entt::handle child);
		static void RemoveChild(entt::handle child);

		template <typename T>
		static T* TryGetComponentInParent(entt::handle object, entt::handle* outParent = nullptr, bool checkObject = false) {
			HierarchyNode& node = object.get<HierarchyNode>();
			T* result = nullptr;
			if (checkObject) {
				result = object.try_get<T>();
			}
			if (result == nullptr) {
				if (node.Parent != entt::null) {
					result = TryGetComponentInParent<T>(entt::handle(*object.registry(), node.Parent), outParent, true);
				}
			}

			if (result != nullptr && outParent != nullptr) {
				*outParent = object;
			}
			return result;
		}
		template <typename T, typename AbortType>
		static T* TryGetComponentInParent(entt::handle object, entt::handle* outParent = nullptr, AbortType* outAbort = nullptr, bool checkObject = false) {
			HierarchyNode& node = object.get<HierarchyNode>();
			T* result = nullptr;
			AbortType* abort = nullptr;
			if (checkObject) {
				result = object.try_get<T>();
				abort = object.try_get<AbortType>();
			}
			if ((result != nullptr | abort != nullptr) && outParent != nullptr) {
				*outParent = object;
			}
			if (result == nullptr && abort == nullptr && node.Parent != entt::null) {
				result = TryGetComponentInParent<T, AbortType>(entt::handle(*object.registry(), node.Parent), outParent, abort, true);
			}

			if (outAbort != nullptr && abort != nullptr) {
				*outAbort = *abort;
			}
			return result;
		}
		
		template <typename T>
		static T* TryGetFirstComponentInChildren(entt::handle object, bool depthFirst = true, bool checkObject = false) {
			HierarchyNode& node = object.get<HierarchyNode>();
			T* result = nullptr;
			if (checkObject) {
				result = object.try_get<T>();
			}
			if (result == nullptr) {
				entt::entity current = node.FirstChild;

				if (depthFirst) {
					while (result == nullptr && current != entt::null) {
						entt::handle child = entt::handle(entt::handle(*object.registry(), current));
						result = child.try_get<T>();
						if (result != nullptr) {
							result = TryGetFirstComponentInChildren<T>(child, depthFirst, true);
						}
						HierarchyNode& currentNode = object.registry()->get<HierarchyNode>(current);
						current = currentNode.Next;
					}
				}
				else {
					current = node.FirstChild;
					while (result == nullptr && current != entt::null) {
						entt::handle child = entt::handle(entt::handle(*object.registry(), current));
						result = child.try_get<T>();
						HierarchyNode& currentNode = object.registry()->get<HierarchyNode>(current);
						entt::entity next = currentNode.Next;
						current = next;
					}
					current = node.FirstChild;
					while (result == nullptr && current != entt::null) {
						entt::handle child = entt::handle(entt::handle(*object.registry(), current));
						result = TryGetFirstComponentInChildren<T>(child, depthFirst, true);
						HierarchyNode& currentNode = object.registry()->get<HierarchyNode>(current);
						entt::entity next = currentNode.Next;
						current = next;
					}
				}
			}
			return result;
		}
		
	protected:
		static void __CutNode(entt::registry& reg, entt::entity host);
		static void __InsertBefore(entt::registry& reg, entt::entity newNode, entt::entity sibling);
		static void __InsertAfter(entt::registry& reg, entt::entity newNode, entt::entity sibling);
		static void __ValidateNode(entt::registry& reg, entt::entity entity, HierarchyNode& node, bool recurse = false);
		template <typename Tfunc>
		static void __PatchNodeOrRoot(entt::registry& reg, entt::entity node, Tfunc&& func) {
			if (node == entt::null) {
				HierarchyNode& node = reg.ctx_or_set<HierarchyNode>();
				func(node);
			} else {
				reg.patch<HierarchyNode, Tfunc>(node, std::forward<Tfunc>(func));
			}
		}
		friend class cereal::access;
		template <class Archive>
		void save(Archive& ar) const {
			ar(
				cereal::make_nvp("Parent", Parent),
				cereal::make_nvp("FirstChild", FirstChild),
				cereal::make_nvp("Previous", Previous),
				cereal::make_nvp("Next", Next),
				cereal::make_nvp("ExistsInHierachy", ExistsInHierachy)
			);
		}

		template <class Archive>
		void load(Archive& ar) {
			ar(
				cereal::make_nvp("Parent", Parent),
				cereal::make_nvp("FirstChild", FirstChild),
				cereal::make_nvp("Previous", Previous),
				cereal::make_nvp("Next", Next),
				cereal::make_nvp("ExistsInHierachy", ExistsInHierachy)
			);
		}
	};
}	