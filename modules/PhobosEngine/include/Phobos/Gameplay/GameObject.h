#pragma once
#include <ENTT/entity/handle.hpp>

#include "Phobos/Application.h"
#include "Phobos/Gameplay/Transform.h"

namespace Phobos::Gameplay
{
	class GameScene;
	
	struct GameObject final {
		GameObject(const entt::handle& handle = entt::handle());
		operator entt::handle() const;
		operator entt::entity() const;

		void SetParent(GameObject other);
		GameObject GetParent() const;

		void Destroy();
		
		void IterateChilren(std::function<void(GameObject)> function, bool recursive = true, bool depthFirst = false);

		Transform& GetTransform() const;

		void SetName(const std::string& name);
		const std::string& GetName() const;

		entt::registry& Registry();
		const entt::registry& Registry() const;

		template <typename T, typename ...TFuncs>
		T& Patch(TFuncs&&... funcs) {
			return _handle.patch<T>(std::forward<TFuncs>(funcs)...);
		}
		
		template <typename T>
		T& Get() {
			return _handle.get<T>();
		}
		template <typename T>
		const T& Get() const{
			return _handle.get<T>();
		}

		template <typename T>
		bool Has() const {
			return _handle.has<T>();
		}

		template <typename T, typename std::enable_if_t<std::is_empty<T>::value, bool> = true>
		void ApplyTag() {
			if (!_handle.has<T>())
				_handle.emplace<T>();
		}
		
		template <typename T, typename... TArgs, typename std::enable_if_t<!std::is_empty<T>::value, bool> = false>
		T& GetOrAdd(TArgs&&... args) {
			return _handle.get_or_emplace<T>(std::forward<TArgs>(args)...);
		}
		template <typename T, typename... TArgs, typename std::enable_if_t<std::is_empty<T>::value, bool> = false>
		void GetOrAdd(TArgs&&... args) {
			static_assert("Cannot call GetOrAdd on empty component types! See ApplyTag instead!");
		}

		template <typename T, typename...TArgs>
		T& Add(TArgs&&... args) {
			return _handle.emplace<T>(std::forward<TArgs>(args)...);
		}

		bool IsValid() const;
		
	protected:
		friend class GameScene;
		entt::handle _handle;

		friend class cereal::access;

		template <class Archive>
		void save(Archive& ar) const {
			ar(_handle.entity());
		}

		template <class Archive>
		void load(Archive& ar) {
			entt::entity entity;
			ar(entity);
			_handle = entt::handle(Application::Instance().ActiveRegistry(), entity);
		}
	};
}
