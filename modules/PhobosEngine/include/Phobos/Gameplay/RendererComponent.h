#pragma once
#include "Phobos/Graphics/VertexArrayObject.h"
#include "Phobos/Graphics/ShaderMaterial.h"
#include <cereal/cereal.hpp>

namespace Phobos::Gameplay {
	class RendererComponent {
	public:
		Graphics::VertexArrayObject::sptr Mesh;
		Graphics::ShaderMaterial::sptr    Material;

		RendererComponent& SetMesh(const Phobos::Graphics::VertexArrayObject::sptr& mesh) { Mesh = mesh; return *this; }
		RendererComponent& SetMaterial(const Graphics::ShaderMaterial::sptr& material) { Material = material; return *this; }
		
	private:
		friend class cereal::access;

		template <typename Archive>
		void save(Archive& ar) const
		{
			Guid mat_guid = Material? Material->GetGUID() : Guid();
			ar(cereal::make_nvp("Material", mat_guid));
			Guid mesh_guid = Mesh ? Mesh->GetGUID() : Guid();
			ar(cereal::make_nvp("Mesh", mesh_guid));
		}
		template <typename Archive>
		void load(Archive& ar)
		{
			Guid mat_guid;
			ar(cereal::make_nvp("Material", mat_guid));
			Guid mesh_guid;
			ar(cereal::make_nvp("Mesh", mesh_guid));
			if (mat_guid.isValid()) {
				Material = Utils::AssetDatabase::Get().FindByGuid<Graphics::ShaderMaterial>(mat_guid);
			}
			if (mesh_guid.isValid()) {
				Mesh = Utils::AssetDatabase::Get().FindByGuid<Graphics::VertexArrayObject>(mesh_guid);
			}
		}

		inline static entt::meta_any MetaData =
			entt::meta<RendererComponent>().type().
			prop(entt::hashed_string("_name"), "Renderer").
			ctor().
			data<&Phobos::Gameplay::RendererComponent::Material>(entt::hashed_string("Material")).prop(entt::hashed_string("_name"), "Material").
			data<&Phobos::Gameplay::RendererComponent::Mesh>(entt::hashed_string("Mesh")).prop(entt::hashed_string("_name"), "Mesh");
	};
}