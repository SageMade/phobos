#pragma once
#include <Phobos/EnumToString.h>

namespace Phobos
{

	DECLARE_ENUM(EditorRenderType,
	Default = 0,
		Color = 1,
		Angle = 2,
		Drag = 3
		);


	DECLARE_ENUM(EditorHintFields,
	Invalid,
		RenderType,
		Min,
		Max,
		Power
		);

	struct EditorHints {
		EditorRenderType RenderType;
		float            Min;
		float            Max;
		float            Power;
		EditorHints() : RenderType(EditorRenderType::Default), Min(0), Max(0), Power(1.0f) { }
		EditorHints(EditorRenderType type) : RenderType(type), Min(0), Max(0), Power(1.0f) { }
		EditorHints(float min, float max, float power = 1.0f) :
			RenderType(EditorRenderType::Default), Min(min), Max(max), Power(power) { }
		EditorHints(EditorRenderType type, float min, float max, float power = 1.0f) :
			RenderType(type), Min(min), Max(max), Power(power) { }
	};
}
