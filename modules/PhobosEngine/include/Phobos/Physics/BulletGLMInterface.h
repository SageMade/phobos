#pragma once
#include <bullet/LinearMath/btVector3.h>
#include <bullet/LinearMath/btQuaternion.h>
#include <GLM/glm.hpp>
#include <GLM/gtc/quaternion.hpp>

constexpr glm::vec3 ToGLM(const btVector3& value) {
	return glm::vec3(value.x(), value.y(), value.z());
}

inline btVector3 FromGLM(const glm::vec3& value) {
	return btVector3(value.x, value.y, value.z);
}

constexpr glm::quat ToGLM(const btQuaternion& value) {
	return glm::quat(value.x(), value.y(), value.z(), value.w());
}

inline btQuaternion FromGLM(const glm::quat& value) {
	return btQuaternion(value.x, value.y, value.z, value.w);
}
