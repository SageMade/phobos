#pragma once
#include <memory>
#include <bullet/btBulletDynamicsCommon.h>
#include <cereal/access.hpp>
#include <cereal/details/traits.hpp>
#include <ENTT/entity/handle.hpp>
#include <GLM/glm.hpp>

#include "Phobos/EnumToString.h"
#include "Phobos/Logging.h"

namespace Phobos::Gameplay {
	class GameScene;
}

namespace Phobos::Physics {
	class PhysicsSystem;

	ENUM(ColliderType, int,
		 Unknown,
		 Box,
		 Sphere,
		 Capsule,
		 CapsuleX,
		 CapsuleY,
		 Cone,
		 ConeX,
		 ConeY,
		 Cylinder,
		 CylinderX,
		 CylinderY,
		 Plane,
		 Terrain
	);

	constexpr bool ColliderTypeHasCustomData(ColliderType type) {
		return type == ColliderType::Terrain;
	}

	ENUM(TerrainAxis, int,
		 X = 0,
		 Y = 1,
		 Z = 2
	);
	ENUM(TerrainDataType, int,
		 Uint8 = PHY_UCHAR,
		 Float = PHY_FLOAT,
		 Double = PHY_DOUBLE
	);

	struct ColliderShapeDirtyTag {};
	
	class Collider
	{
	public:
		Collider() : _type(ColliderType::Unknown), _shape(nullptr), _offset(glm::vec3(0.0f)), _rotationEulerDeg(glm::vec3(0.0f)) { }
		Collider(ColliderType type) : _type(type), _shape(nullptr), _offset(glm::vec3(0.0f)), _rotationEulerDeg(glm::vec3(0.0f)) { }
		~Collider() = default;
		Collider(const Collider& other) = default;
		Collider(Collider&& other) = default;
		Collider& operator=(const Collider& other) = default;
		Collider& operator=(Collider&& other) = default;

		Collider& SetType(ColliderType type);
		ColliderType GetType() const { return _type; }

		Collider& SetOffset(const glm::vec3& value);
		const glm::vec3& GetOffset() const { return _offset; }

		Collider& SetColliderData(float value);
		Collider& SetColliderData(float valueA, float valueB);
		Collider& SetColliderData(const glm::vec2& value);
		Collider& SetColliderData(float valueA, float valueB, float valueC);
		Collider& SetColliderData(const glm::vec3& value);
		Collider& SetColliderData(float valueA, float valueB, float valueC, float valueD);
		Collider& SetColliderData(const glm::vec4& value);
		glm::vec4 GetColliderData() const { return _shapeData; }
		
		template <typename T>
		Collider& SetColliderDataPtr(const T& data) {
			_shapeDataPtr = malloc(sizeof(T));
			*((T*)_shapeDataPtr) = data;
			_shapeDataPtrSize = sizeof(T);
			return *this;
		}
		template <typename T>
		T* GetColliderDataPtr() {
			LOG_ASSERT(sizeof(T) == _shapeDataPtrSize, "Attempting to get data of a different size!!");
			return (T*)_shapeDataPtr;
		}
		template <typename T>
		const T* GetColliderDataPtr() const {
			LOG_ASSERT(sizeof(T) == _shapeDataPtrSize, "Attempting to get data of a different size!!");
			return (const T*)_shapeDataPtr;
		}

		struct TerrainData {
			size_t          Width;
			size_t          Height;
			void*           HeightMap;
			float           HeightScale;
			float           MinHeight;
			float           MaxHeight;
			TerrainAxis     Axis;
			TerrainDataType DataType;
			bool            FlipQuadEdges;

			TerrainData() :
				Width(256),
				Height(256),
				HeightMap(nullptr),
				HeightScale(1.0f),
				MinHeight(-1.0f),
				MaxHeight(-1.0f),
				Axis(TerrainAxis::Z),
				DataType(TerrainDataType::Float),
				FlipQuadEdges(false) {}

			inline void Allocate() {
				if (HeightMap !=nullptr) {
					LOG_WARN("Re-allocating terrain heightmap collider data");
					free(HeightMap);
				}
				size_t dataSize = DataType == TerrainDataType::Float ? sizeof(float) : DataType == TerrainDataType::Double ? sizeof(double) : sizeof(uint8_t);
				dataSize *= Width * Height;
				HeightMap = malloc(dataSize);
			}
			inline void Free() {
				if (HeightMap != nullptr) {
					free(HeightMap);
				}
				HeightMap = nullptr;
			}

			inline size_t GetDataSize() const {
				size_t result =  DataType == TerrainDataType::Float ? sizeof(float) : DataType == TerrainDataType::Double ? sizeof(double) : sizeof(uint8_t);
				return result * Width * Height;
			}
		};
		
	private:
		std::shared_ptr<btCollisionShape> _shape;
		ColliderType _type;
		glm::vec4 _shapeData;
		void*     _shapeDataPtr;
		size_t    _shapeDataPtrSize;
		glm::vec3 _offset;
		glm::vec3 _rotationEulerDeg;
		entt::handle _handle;
		btCompoundShape* _hostShape;
		entt::handle _hostRigidBody;
		int _childIndex;

		friend class ::Phobos::Gameplay::GameScene;
		friend class PhysicsSystem;
		friend class cereal::access;
		
		template <class Archive>
		void save(Archive& ar) const {
			ar(
				cereal::make_nvp("Offset", _offset),
				cereal::make_nvp("RotationEuler", _rotationEulerDeg),
				cereal::make_nvp("ShapeType", _type)
			);
			switch (_type) {
				case ColliderType::Box:
					ar(cereal::make_nvp("Bounds", (glm::vec3)_shapeData));
					break;
				case ColliderType::Sphere:
					ar(cereal::make_nvp("Radius", _shapeData.x));
					break;
				case ColliderType::Capsule:
				case ColliderType::CapsuleX:
				case ColliderType::CapsuleY:
				case ColliderType::Cone:
				case ColliderType::ConeX:
				case ColliderType::ConeY:
					ar(cereal::make_nvp("Radius", _shapeData.x));
					ar(cereal::make_nvp("Height", _shapeData.y));
					break;
				case ColliderType::Cylinder:
				case ColliderType::CylinderX:
				case ColliderType::CylinderY:
					ar(cereal::make_nvp("Bounds", (glm::vec3)_shapeData));
					break;
				case ColliderType::Plane:
					ar(cereal::make_nvp("Normal", (glm::vec3)_shapeData));
					ar(cereal::make_nvp("Constant", _shapeData.w));
					break;
				case ColliderType::Terrain:
					ar(cereal::make_nvp("HasData", _shapeDataPtr != nullptr));
					if (_shapeDataPtr != nullptr) {
						TerrainData* data = (TerrainData*)_shapeDataPtr;
						ar(cereal::make_nvp("Width", data->Width));
						ar(cereal::make_nvp("Height", data->Height));
						ar(cereal::make_nvp("Axis", data->Axis));
						ar(cereal::make_nvp("HeightScale", data->HeightScale));
						ar(cereal::make_nvp("MinHeight", data->MinHeight));
						ar(cereal::make_nvp("MaxHeight", data->MaxHeight));
						ar(cereal::make_nvp("DataType", data->DataType));
						ar(cereal::make_nvp("FlipQuadEdges", data->FlipQuadEdges));
						size_t dataSize = data->DataType == TerrainDataType::Float ? sizeof(float) :
							data->DataType == TerrainDataType::Double ? sizeof(double) : sizeof(uint8_t);
						dataSize *= data->Width * data->Height;
						ar(cereal::make_nvp("DataSize", dataSize));
						if constexpr (cereal::traits::is_text_archive<Archive>::value) {
							ar.saveBinaryValue(data->HeightMap, dataSize, "Data");
						} else {
							ar(cereal::make_nvp("Data", cereal::binary_data((uint8_t*)data->HeightMap, dataSize)));
						}
					}
					break;
			}
		}

		template <class Archive>
		void load(Archive& ar) {
			ar(
				cereal::make_nvp("Offset", _offset),
				cereal::make_nvp("RotationEuler", _rotationEulerDeg),
				cereal::make_nvp("ShapeType", _type)
			);
			glm::vec3 temp;
			switch (_type)
			{
				case ColliderType::Box:
				case ColliderType::Cylinder:
				case ColliderType::CylinderX:
				case ColliderType::CylinderY:
					ar(cereal::make_nvp("Bounds", temp));
					_shapeData = glm::vec4(temp, 0.0f);
					break;
				case ColliderType::Sphere:
					ar(cereal::make_nvp("Radius", _shapeData.x));
					break;
				case ColliderType::Capsule:
				case ColliderType::CapsuleX:
				case ColliderType::CapsuleY:
				case ColliderType::Cone:
				case ColliderType::ConeX:
				case ColliderType::ConeY:
					ar(cereal::make_nvp("Radius", _shapeData.x));
					ar(cereal::make_nvp("Height", _shapeData.y));
					break;
				case ColliderType::Plane:
					ar(cereal::make_nvp("Normal", temp));
					_shapeData = glm::vec4(temp, 0.0f);
					ar(cereal::make_nvp("Constant", _shapeData.w));
					break;
				case ColliderType::Terrain:
					bool hasData;
					ar(cereal::make_nvp("HasData", hasData));
					if (hasData) {
						TerrainData data;
						ar(cereal::make_nvp("Width", data.Width));
						ar(cereal::make_nvp("Height", data.Height));
						ar(cereal::make_nvp("Axis", data.Axis));
						ar(cereal::make_nvp("HeightScale", data.HeightScale));
						ar(cereal::make_nvp("MinHeight", data.MinHeight));
						ar(cereal::make_nvp("MaxHeight", data.MaxHeight));
						ar(cereal::make_nvp("DataType", data.DataType));
						ar(cereal::make_nvp("FlipQuadEdges", data.FlipQuadEdges));
						size_t dataSize = data.DataType == TerrainDataType::Float ? sizeof(float) :
							data.DataType == TerrainDataType::Double ? sizeof(double) : sizeof(uint8_t);
						dataSize *= data.Width * data.Height;
						ar(cereal::make_nvp("DataSize", dataSize)); 
						data.Allocate();
						if constexpr (cereal::traits::is_text_archive<Archive>::value) {
							ar.loadBinaryValue((uint8_t*)data.HeightMap, dataSize, "Data");
						} else {
							ar(cereal::make_nvp("Data", cereal::binary_data((uint8_t*)data.HeightMap, dataSize)));
						}
						SetColliderDataPtr(data);
					}
					break;
			}
		}
		
	};
}
