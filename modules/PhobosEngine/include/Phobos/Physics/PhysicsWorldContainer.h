#pragma once
#include "PhysicsWorld.h"
#include "Phobos/Gameplay/IBehaviour.h"

namespace Phobos::Physics
{
	class PhysicsWorldContainer final
	{
	public:
		PhysicsWorld::sptr World;
	};
}