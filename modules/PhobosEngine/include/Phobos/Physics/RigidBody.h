#pragma once
#include <bullet/btBulletDynamicsCommon.h>

#include "Phobos/Logging.h"
#include "Phobos/EnumToString.h"
#include <memory>
#include <cereal/access.hpp>
#include <GLM/gtc/epsilon.hpp>

namespace Phobos::Physics {
	class PhysicsWorld;
	typedef std::shared_ptr<PhysicsWorld> PhysicsWorldSptr;
	
	ENUM(ColliderGroup, int,
		 Dynamic = 1,
		 Terrain = 2,
		 Trigger = 4,
		 All = 0x7FFFFFFF
	);

	class RigidBody
	{
	public:
		RigidBody() :
			_body(nullptr), _motionState(nullptr),
			_mass(0.0f), _massDirty(true),
			_group(1), _mask(0x7FFFFFFF), _isGroupMaskDirty(true),
			_alwaysActive(false), _isKinematic(false),
			_isDebugDrawEnabled(true),
			_damping(0.0f), _angularDamping(0.005f) { }
		~RigidBody() = default;
		RigidBody(const RigidBody& other) = default;
		RigidBody(RigidBody&& other) = default;
		RigidBody& operator =(const RigidBody& other) = default;
		RigidBody& operator =(RigidBody&& other) = default;

		RigidBody& SetMass(float value) {
			_massDirty = (glm::epsilonNotEqual(value, _mass, glm::epsilon<float>()));
			_mass = value;
			return *this;
		}
		float GetMass() const { return _mass; }

		RigidBody& SetGroup(int value) {
			_isGroupMaskDirty = (value != _group);
			_group = value;
			return *this;
		}
		int GetGroup() const { return _group; }

		RigidBody& SetLinearDamping(float value);
		float GetLinearDamping() const { return _damping; }

		RigidBody& SetAngularDamping(float value);
		float GetAngularDamping() const { return _angularDamping; }
		
		RigidBody& SetMask(int value) {
			_isGroupMaskDirty = (value != _mask);
			_mask = value;
			return *this;
		}
		int GetMask() const { return _mask; }

		RigidBody& SetAlwaysActive(bool value);
		bool GetAlwaysActive() const { return _alwaysActive; }

		RigidBody& SetIsKinematic(bool value);
		bool GetIsKinematic() const { return _alwaysActive; }

		RigidBody& SetIsDebugDrawEnabled(bool value);
		bool GetIsDebugDrawEnabled() const { return _isDebugDrawEnabled; }

		RigidBody& SetPhysicsWorld(const PhysicsWorldSptr& ptr);
		const PhysicsWorldSptr& GetPhysicsWorld() const { return _physicsWorld; }

	private:
		btRigidBody* _body;
		std::shared_ptr<btCompoundShape> _compoundShape;
		btMotionState* _motionState;
		btTransform _originState;
		PhysicsWorldSptr _physicsWorld;
		float _angularDamping;
		float _damping;
		
		friend class PhysicsSystem;

		float _mass;
		bool _massDirty;

		int _group;
		int _mask;
		bool _isGroupMaskDirty;

		bool _alwaysActive;
		bool _isKinematic;
		bool _isDebugDrawEnabled;

		friend class cereal::access;

		template <class Archive>
		void serialize(Archive& ar) {
			ar(
				cereal::make_nvp("Mass", _mass),
				cereal::make_nvp("AngularDamping", _angularDamping),
				cereal::make_nvp("LinearDamping", _damping),
				cereal::make_nvp("Group", _group),
				cereal::make_nvp("Mask", _mask),
				cereal::make_nvp("AlwaysActive", _alwaysActive),
				cereal::make_nvp("Kinematic", _isKinematic),
				cereal::make_nvp("DebugDraw", _isDebugDrawEnabled)
			);
		}
	};
}
