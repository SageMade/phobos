#pragma once
#include <memory>
#include <vector>
#include <bullet/btBulletDynamicsCommon.h>

namespace Phobos::Physics {
	class BulletDebugDraw;
	class RigidBody;
	class Collider;
	class PhysicsWorld;
	
	class PhysicsSystem
	{
	public:
		static PhysicsSystem& Get() {
			if (_instance == nullptr) {
				_instance = new PhysicsSystem();
			}
			return *_instance;
		}

		void Update();
		std::shared_ptr<PhysicsWorld> CreateWorld() const;

		void DestroyRigidBody(RigidBody& body);
		static std::shared_ptr<btCollisionShape> CreateCollisionShape(Collider& collider);

	private:
		inline static PhysicsSystem* _instance = nullptr;
		friend class PhysicsWorld;
		PhysicsSystem();

		std::vector<PhysicsWorld*> _worlds;

				
		Physics::BulletDebugDraw* _debugDraw;
	};
}