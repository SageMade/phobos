#pragma once
#include <memory>
#include <bullet/btBulletDynamicsCommon.h>
#include <GLM/glm.hpp>

#include "Phobos/Gameplay/Scene.h"

namespace Phobos::Physics
{
	class PhysicsSystem;
	
	class PhysicsWorld
	{
	public:
		typedef std::shared_ptr<PhysicsWorld> sptr;
		
		PhysicsWorld();
		~PhysicsWorld();

		btDynamicsWorld* GetWorld() const { return _bulletPhsyicsWorld; }
		void SetRootTransform(const glm::mat4& value);
		const glm::mat4& GetRootTransform() const;
		const glm::mat4& GetInverseRootTransform() const { return _inverseRootTransform; }
		bool HasNonIdentityRoot() const { return _hasNonIdentityRoot; }
		
	protected:
		friend class PhysicsSystem;

		btDynamicsWorld*          _bulletPhsyicsWorld;
		btCollisionConfiguration* _collisionConfig;
		btCollisionDispatcher*    _collisionDispatcher;
		btBroadphaseInterface*    _broadphaseInterface;
		btConstraintSolver*       _constraintSolver;
		bool                      _hasNonIdentityRoot;
		glm::mat4                 _transformRoot;
		glm::mat4                 _inverseRootTransform; // Updated by the physics system
	};
}
