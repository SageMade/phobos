#pragma once

#ifdef WINDOWS_GLFW

#include "Phobos/Input/InputManager.h"
#include "GLFW/glfw3.h"

namespace Phobos::Input {
	class GlfwInput : public Phobos::Input::InputManager {
	public:
		GlfwInput();
		virtual ~GlfwInput();
		
	protected:
		ButtonState __GetKeyState(KeyCode key) override;
		ButtonState __GetMouseState(MouseButton button) override;
		glm::vec2 __GetMousePos() override;
		glm::vec2 __GetMouseScroll() override;
		glm::vec2 __GetMouseScrollDelta() override;
		void __Poll() override;
		void __Init(void* windowPtr) override;

		GLFWwindow* m_Window;
		char m_PrevKeys[(size_t)PHOBOS_KEY_LAST + 1];
		char m_PrevMouse[(size_t)PHOBOS_MOUSEBUTTON_LAST + 1];
	};
}
#endif
