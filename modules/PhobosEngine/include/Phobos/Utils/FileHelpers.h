#pragma once
#include <string>
#include <filesystem>

namespace Phobos::Utils {
	class FileHelpers
	{
	public:
		FileHelpers() = delete;
		static std::string ReadFile(const std::string& filename);
		static std::string ReadResolveIncludes(const std::string& filename);
	};
}