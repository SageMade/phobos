#pragma once
#include <map>
#include <cereal/cereal.hpp>
#include <filesystem>
#include <cereal/archives/binary.hpp>
#include <cereal/archives/portable_binary.hpp>
#include <cereal/archives/xml.hpp>
#include <cereal/archives/json.hpp>
#include <cereal/types/polymorphic.hpp>
#include "Phobos/Utils/typeindex.hpp"
#include "Phobos/Utils/IAsset.h"
#include "Phobos/Logging.h"

namespace Phobos::Utils {
	class AssetDatabase
	{
	public:
		/// <summary>
		/// Gets the asset database singleton instance
		/// </summary>
		static inline AssetDatabase& Get() {
			if (_instance == nullptr) {
				_instance = new AssetDatabase();
			}
			return *_instance;
		}
		/// <summary>
		/// Frees the asset database instance
		/// </summary>
		static inline void Free() {
			if (_instance != nullptr) {
				delete _instance;
				_instance = nullptr;
			}
		}

		#pragma region Member Funcs 
		/// <summary>
		/// Registers a new resource with the asset database
		/// </summary>
		/// <typeparam name="AssetType">The type of asset to register, must extend IAsset</typeparam>
		/// <param name="resource">The resource to register, must have a unique GUID</param>
		template <typename AssetType, typename = typename std::enable_if<std::is_base_of<IAsset, AssetType>::value>::type>
		void Register(const std::shared_ptr<AssetType>& resource)
		{
			auto pool = _GetPool<AssetType>();

			auto it = pool->GuidMap.find(resource->GetGUID());
			if (it == pool->GuidMap.end()) {
				pool->GuidMap[resource->GetGUID()] = resource;
			} else {
				LOG_WARN("Duplicate GUID found in resources! Are you trying to register the same resource twice?");
			}
		}

		/// <summary>
		/// Finds an asset of the given type by it's GUID
		/// </summary>
		/// <typeparam name="AssetType">The type of asset to find, must extend IAsset</typeparam>
		/// <param name="guid">The GUID of the asset to find</param>
		/// <returns>The asset that was found, or nullptr if none was found</returns>
		template <typename AssetType, typename = typename std::enable_if<std::is_base_of<IAsset, AssetType>::value>::type>
		std::shared_ptr<AssetType> FindByGuid(const Phobos::Guid& guid)
		{
			std::shared_ptr<AssetPool_t<AssetType>> pool = _GetPool<AssetType>();
			auto it = pool->GuidMap.find(guid);
			if (it == pool->GuidMap.end()) {
				return nullptr;
			} else {
				return it->second;
			}
		}

		inline std::shared_ptr<IAsset> FindAssetByGuid(const Phobos::Guid& guid) {
			auto it = _pools.begin();
			while (it != _pools.end()) {
				IAsset::sptr result = it->second->FindByAssetGuid(guid);
				if (result) return result;
				++it;
			}
			return nullptr;
		}

		/// <summary>
		/// Finds the first asset with the given name
		/// </summary>
		/// <typeparam name="AssetType">The type of asset to find, must extend IAsset</typeparam>
		/// <param name="name">The name of the asset to find</param>
		/// <returns>The asset that was found, or nullptr if none was found</returns>
		template <typename AssetType, typename = typename std::enable_if<std::is_base_of<IAsset, AssetType>::value>::type>
		std::shared_ptr<AssetType> FindFirstByName(const std::string& name)
		{
			auto pool = _GetPool<AssetType>();
			auto it = pool->GuidMap.begin();
			while (it != pool->GuidMap.end())
			{
				// Check if value of this entry matches with given value
				if (it->second->GetName() == name)
				{
					return it->second;
				}
				// Go to next entry in map
				it++;
			}
			return nullptr;
		}

		/// <summary>
		/// Loads an asset from the given file name, or returns the existing asset loaded from the file if it has been loaded already
		/// Uses the asset's LoadFromFile method internally, requires a default constructor to be publicly visible
		/// </summary>
		/// <typeparam name="AssetType">The type of asset to find, must extend IAsset</typeparam>
		/// <param name="filename">The path to the file to load</param>
		/// <returns>A smart pointer to the asset loaded from the given file name</returns>
		template <typename AssetType, typename = typename std::enable_if<std::is_base_of<IAsset, AssetType>::value>::type>
		std::shared_ptr<AssetType> LoadFromFile(const std::string& filename)
		{
			auto pool = _GetPool<AssetType>();
			entt::hashed_string::hash_type hashValue = entt::hashed_string::value(filename.c_str());
			if (pool->FilenameMap.count(hashValue) == 0) {
				std::shared_ptr<AssetType> resource = std::make_shared<AssetType>();
				resource->LoadFromFile(filename);
				resource->SetName(std::filesystem::path(filename).stem().string());
				Register(resource);
				pool->FilenameMap[hashValue] = resource;
				return resource;
			} else {
				return pool->FilenameMap[hashValue];
			}

		}

		/// <summary>
		/// Creates a new asset, and assigns it the given name
		/// </summary>
		/// <typeparam name="AssetType">The type of asset to find, must extend IAsset</typeparam>
		/// <param name="assetName">The name of the newly created asset</param>
		/// <returns>A smart pointer to the new asset</returns>
		template <typename AssetType, typename = typename std::enable_if<std::is_base_of<IAsset, AssetType>::value>::type>
		std::shared_ptr<AssetType> Create(const std::string& assetName)
		{
			std::shared_ptr<AssetType> resource = std::make_shared<AssetType>();
			resource->SetName(assetName);
			Register(resource);
			return resource;
		}

		/// <summary>
		/// Removes an asset from the database. Note that this will not immediately free the memory that the asset is using,
		/// as other instances may be maintaining references to the asset. Once all instances with a handle to the asset are
		/// destroyed, the asset will be freed
		/// </summary>
		/// <typeparam name="AssetType">The type of asset to find, must extend IAsset</typeparam>
		/// <param name="resource">The resource to remove from the asset database</param>
		/// <returns>True if the asset was removed, false if otherwise</returns>
		template <typename AssetType, typename = typename std::enable_if<std::is_base_of<IAsset, AssetType>::value>::type>
		bool RemoveAsset(const std::shared_ptr<AssetType>& resource) {
			auto pool = _GetPool<AssetType>();
			std::map<Phobos::Guid, std::shared_ptr<AssetType>>& map = pool->GuidMap;

			auto it = map.begin();
			while (it != map.end())
			{
				// Check if value of this entry matches with given value
				if (it->second == resource)
				{
					map.erase(it);

					// Scan and erase entry from the file name map
					auto& fMap = pool->FilenameMap;
					auto fIt = map.begin();
					while (fIt != map.end())
					{
						if (fIt->second == resource) {
							fMap.erase(fIt);
							break;
						}
						fIt++;
					}
					return true;
				}
				// Go to next entry in map
				it++;
			}
			return false;
		}

		/// <summary>
		/// Removes an asset from the database. Note that this will not immediately free the memory that the asset is using,
		/// as other instances may be maintaining references to the asset. Once all instances with a handle to the asset are
		/// destroyed, the asset will be freed
		/// </summary>
		/// <typeparam name="AssetType">The type of asset to find, must extend IAsset</typeparam>
		/// <param name="guid">The GUID of the asset to remove from the asset database</param>
		/// <returns>True if the asset was removed, false if otherwise</returns>
		template <typename AssetType, typename = typename std::enable_if<std::is_base_of<IAsset, AssetType>::value>::type>
		bool RemoveAsset(const Phobos::Guid& guid) {
			auto pool = _GetPool<AssetType>();
			std::map<Phobos::Guid, std::shared_ptr<AssetType>>& map = pool->GuidMap;

			auto it = map.find(guid);
			if (it == map.end()) {
				return false;
			} else {
				map.erase(it);
				return true;
			}
		}

		/// <summary>
		/// Saves all assets of the given type into an archive
		/// </summary>
		/// <typeparam name="...TAssetTypes">The types of assets to serialize, must extend IAsset</typeparam>
		/// <typeparam name="Archive">The type of archive to store the assets into</typeparam>
		/// <param name="ar">The archive to store the assets into</param>
		template <typename ... TAssetTypes, typename Archive, typename = void>
		void SaveAssets(Archive& ar) {
			_SaveAssets<Archive, TAssetTypes...>(ar);
		}

		/// <summary>
		/// Loads all assets of the given type from the archive
		/// </summary>
		/// <typeparam name="...TAssetTypes">The types of assets to load, must extend IAsset</typeparam>
		/// <typeparam name="Archive">The type of archive to load the assets from</typeparam>
		/// <param name="ar">The archive to load the assets from</param>
		template <typename ... TAssetTypes, typename Archive, typename = void>
		void LoadAssets(Archive& ar)
		{
			_LoadAssets<Archive, TAssetTypes...>(ar);
		}

		/// <summary>
		/// Returns an iterator that iterates over key-value pairs of assets.
		/// The first value in the pair will be the asset's GUID, and the second value will be the
		/// pointer to the asset itself
		/// </summary>
		/// <typeparam name="AssetType">The type of asset to iterate over, must extend IAsset</typeparam>
		template <typename AssetType, typename = typename std::enable_if<std::is_base_of<IAsset, AssetType>::value>::type>
		const std::map<Phobos::Guid, std::shared_ptr<AssetType>>& GetIterator() {
			return _GetPool<AssetType>()->GuidMap;
		}

		#pragma endregion

	private:
		inline static AssetDatabase* _instance;

		// This dummy class lets us store all the pointers to our asset pools in one map
		struct AssetPool {
			typedef std::shared_ptr<AssetPool> sptr;

			/// <summary>
			/// We need at least one pure virtual function to be compiled, so we log when new resource pools get allocated
			/// </summary>
			virtual void NotifyPoolAlloc() = 0;

			/// <summary>
			/// Finds the underlying IAsset pointer in the pool
			/// </summary>
			/// <param name="guid">The GUID of the asset to find</param>
			/// <returns>The asset in the pool, or nullptr</returns>
			virtual IAsset::sptr FindByAssetGuid(const Phobos::Guid& guid) = 0;
		};
		/// <summary>
		/// Implementation for a datastore for assets of a single type
		/// </summary>
		/// <typeparam name="AssetType">The type of asset the pool stores, must extent IAsset</typeparam>
		template <typename AssetType, typename = typename std::enable_if<std::is_base_of<IAsset, AssetType>::value>::type>
		struct AssetPool_t : public AssetPool {
			// Assets are stored mapped by their GUID for quick access
			std::map<Phobos::Guid, std::shared_ptr<AssetType>> GuidMap;
			std::map<entt::hashed_string::hash_type, std::shared_ptr<AssetType>> FilenameMap;
			typedef std::shared_ptr<AssetPool_t<AssetType>> sptr;

			/// <summary>
			/// We need at least one pure virtual function to be compiled, so we log when new resource pools get allocated
			/// </summary>
			void NotifyPoolAlloc() override {
				LOG_INFO("Allocating asset pool for type {}", typeid(AssetType).name());
			}

			IAsset::sptr FindByAssetGuid(const Phobos::Guid& guid) {
				auto it = GuidMap.find(guid);
				if (it != GuidMap.end())
					return it->second;
				else
					return nullptr;
			}
		};

		/// <summary>
		/// Gets the underlying pool for the given asset type
		/// </summary>
		/// <typeparam name="AssetType">The type of asset the pool stores, must extent IAsset</typeparam>
		/// <returns>A pointer to an asset pool for the given type</returns>
		template <typename AssetType, typename = typename std::enable_if<std::is_base_of<IAsset, AssetType>::value>::type>
		std::shared_ptr<AssetPool_t<AssetType>> _GetPool()
		{
			// Search for the pool based off the type index
			auto it = _pools.find(std::type_index(typeid(AssetType)));
			// If no pool was found, create one
			if (it == _pools.end()) {
				it = _pools.emplace(std::type_index(typeid(AssetType)), std::make_shared<AssetPool_t<AssetType>>()).first;
				it->second->NotifyPoolAlloc();
			}
			// Upcast the pointer to the pool that was found
			return std::dynamic_pointer_cast<AssetPool_t<AssetType>>(it->second);
		}
		// Map of pools, keyed on the type index
		std::map<std::type_index, AssetPool::sptr> _pools;

		template <typename Archive>
		void _SaveAssets(Archive& ar) { }

		template <typename Archive, typename Next, typename ... TAssetTypes>
		void _SaveAssets(Archive& ar) {
			std::map<Phobos::Guid, std::shared_ptr<Next>>& map = _GetPool<Next>()->GuidMap;
			ar(cereal::make_nvp(typeid(Next).name(), map));
			_SaveAssets<Archive, TAssetTypes...>(ar);
		}

		template <typename Archive>
		void _LoadAssets(Archive& ar) { }

		template <typename Archive, typename Next, typename ... TAssetTypes>
		void _LoadAssets(Archive& ar) {
			std::map<Phobos::Guid, std::shared_ptr<Next>>& map = _GetPool<Next>()->GuidMap;
			ar(cereal::make_nvp(typeid(Next).name(), map));

			_LoadAssets<Archive, TAssetTypes...>(ar);
		}
	};
}