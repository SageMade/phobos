#pragma once
#include "Phobos/Utils/MeshFactory.h"

namespace Phobos::Utils {
	class ObjLoader
	{
	public:
		static Graphics::VertexArrayObject::sptr LoadFromFile(const std::string& filename, const glm::vec4& inColor = glm::vec4(1.0f));

	protected:
		ObjLoader() = default;
		~ObjLoader() = default;
	};
}