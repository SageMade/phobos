#pragma once
#include "guid.hpp"
#include <cereal/cereal.hpp>
#include <cereal/archives/binary.hpp>
#include <cereal/archives/portable_binary.hpp>
#include <cereal/archives/xml.hpp>
#include <cereal/archives/json.hpp>
#include <cereal/types/polymorphic.hpp>
#include "Phobos/Utils/GlmCerealization.h"

namespace Phobos {
	class IAsset {
	public:
		IAsset(const IAsset& other) = delete;
		IAsset(IAsset&& other) = delete;
		IAsset& operator=(const IAsset& other) = delete;
		IAsset& operator=(IAsset&& other) = delete;

		typedef std::shared_ptr<IAsset> sptr;

		virtual ~IAsset() = default;

		const Phobos::Guid& GetGUID() const { return _guid; }
		const std::string& GetName() const { return _name; }
		virtual void SetName(const std::string& name) { _name = name; }

		/// <summary>
		/// Gets the underlying OpenGL handle that this class is wrapping
		/// </summary>
		virtual uint32_t GetHandle() const { return _handle; }

		virtual void LoadFromFile(const std::string& name) { }

	protected:
		Phobos::Guid _guid;
		std::string _name;
		uint32_t _handle;
		inline IAsset() : _guid(Phobos::Guid::New()), _name(""), _handle(0) { };

		friend class cereal::access;

		template <typename Archive>
		void save(Archive& ar) const
		{
			ar(
				cereal::make_nvp("GUID", _guid),
				cereal::make_nvp("Name", _name)
			);
		}

		template <typename Archive>
		void load(Archive& ar)
		{
			ar(
				cereal::make_nvp("GUID", _guid),
				cereal::make_nvp("Name", _name)
			);
		}
	};
}