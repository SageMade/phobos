#include "Phobos/Utils/FileHelpers.h"
#include <fstream>

#include "Phobos/Logging.h"
#include "Phobos/Utils/StringUtils.h"

namespace Phobos::Utils {
std::string FileHelpers::ReadFile(const std::string& filename)
{
	std::string result;
	std::ifstream in(filename, std::ios::in | std::ios::binary); // ifstream closes itself due to RAII
	if (in)
	{
		in.seekg(0, std::ios::end);
		size_t size = in.tellg();
		if (size != -1) {
			result.resize(size);
			in.seekg(0, std::ios::beg);
			in.read(&result[0], size);
		} else {
			LOG_ERROR("Could not read from file '{0}'", filename);
		}
	} else {
		LOG_ERROR("Could not open file '{0}'", filename);
	}

	return result;
}

std::string FileHelpers::ReadResolveIncludes(const std::string& filename)
{
	std::string result = ReadFile(filename);
	const std::filesystem::path folder = std::filesystem::path(filename).parent_path();

	const char* includeToken = "#include";
	const size_t includeTokenLen = const_strlen(includeToken);
	{
		size_t seek = result.find(includeToken, 0);
		while (seek != std::string::npos) {
			size_t eol = result.find_first_of("\r\n", seek);
			LOG_ASSERT(eol != std::string::npos, "Syntax error, no eol found after type token");

			size_t begin = seek + includeTokenLen + 1;
			std::string path = result.substr(begin, eol - begin);
			StringTools::Trim(path);
			StringTools::Trim(path, '"');
			std::filesystem::path target = folder / path;

			LOG_ASSERT(std::filesystem::exists(target), "File does not exist");
			std::string replacement = FileHelpers::ReadResolveIncludes(target.string());

			result.replace(seek, eol - seek, replacement);
			seek = result.find(includeToken, seek + replacement.length());
		}
	}

	return result;
}
}
