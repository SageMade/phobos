#include "Phobos/Input/InputManager.h"
#include "Phobos/Logging.h"

#ifdef WINDOWS_GLFW
#include "Phobos/Input/GLFWInput.h"
#endif

namespace Phobos::Input {

	InputManager* InputManager::m_Instance = nullptr;

	#define PROXY(func, ...) \
		LOG_ASSERT(m_Instance != nullptr, "TTK Input has not been initialized!"); \
		return m_Instance->func(__VA_ARGS__);

	#define CALL(func, ...) \
		LOG_ASSERT(m_Instance != nullptr, "TTK Input has not been initialized!"); \
		m_Instance->func(__VA_ARGS__)

	bool InputManager::GetKeyDown(KeyCode key) {
		LOG_ASSERT(m_Instance != nullptr, "TTK Input has not been initialized!");
		return *m_Instance->__GetKeyState(key) & *ButtonState::Pressed;
	}
	bool InputManager::GetKeyPressed(KeyCode key) {
		LOG_ASSERT(m_Instance != nullptr, "TTK Input has not been initialized!");
		return m_Instance->__GetKeyState(key) == ButtonState::Pressed;
	}
	bool InputManager::GetKeyReleased(KeyCode key) {
		LOG_ASSERT(m_Instance != nullptr, "TTK Input has not been initialized!");
		return m_Instance->__GetKeyState(key) == ButtonState::Released;
	}
	ButtonState InputManager::GetKeyState(KeyCode key) { PROXY(__GetKeyState, key); }

	bool InputManager::GetMouseDown(MouseButton button) {
		LOG_ASSERT(m_Instance != nullptr, "TTK Input has not been initialized!");
		return *m_Instance->__GetMouseState(button) & *ButtonState::Pressed;
	}
	bool InputManager::GetMousePressed(MouseButton button) {
		LOG_ASSERT(m_Instance != nullptr, "TTK Input has not been initialized!");
		return m_Instance->__GetMouseState(button) == ButtonState::Pressed;
	}
	bool InputManager::GetMouseReleased(MouseButton button) {
		LOG_ASSERT(m_Instance != nullptr, "TTK Input has not been initialized!");
		return m_Instance->__GetMouseState(button) == ButtonState::Released;
	}
	ButtonState InputManager::GetMouseState(MouseButton button) { PROXY(__GetMouseState, button); }

	float InputManager::GetMouseX() {
		LOG_ASSERT(m_Instance != nullptr, "TTK Input has not been initialized!");
		return m_Instance->__GetMousePos().x;
	}
	float InputManager::GetMouseY() {
		LOG_ASSERT(m_Instance != nullptr, "TTK Input has not been initialized!");
		return m_Instance->__GetMousePos().y;
	}
	glm::vec2 InputManager::GetMousePos() { PROXY(__GetMousePos); }
	glm::vec2 InputManager::GetMouseScroll() { PROXY(__GetMouseScroll); }
	glm::vec2 InputManager::GetMouseScrollDelta() { PROXY(__GetMouseScrollDelta); }

	void InputManager::Poll() { PROXY(__Poll); }

	void InputManager::Init(void* windowPtr) {
		#ifdef WINDOWS_GLFW
		m_Instance = new Phobos::Input::GlfwInput();
		#elif TTK_GLUT
		m_instance = new GlutInput();
		#else
		static_assert("Must specify either TTK_GLFW or TTK_GLUT!");
		#endif
		m_Instance->__Init(windowPtr);
	}

	void InputManager::Uninitialize() {
		LOG_ASSERT(m_Instance != nullptr, "TTK Input has not been initialized!");
		delete m_Instance;
		m_Instance = nullptr;
	}
}