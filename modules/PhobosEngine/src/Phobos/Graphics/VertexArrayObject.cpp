#include "Phobos/Graphics/VertexArrayObject.h"
#include "Phobos/Graphics/IndexBuffer.h"
#include "Phobos/Graphics/VertexBuffer.h"
#include "Phobos/Logging.h"

namespace Phobos::Graphics {
	VertexArrayObject::VertexArrayObject() : IAsset(),
		_indexBuffer(nullptr),
		_vertexCount(0),
		_instanceCount(0)
	{
		glCreateVertexArrays(1, &_handle);
	}

	VertexArrayObject::~VertexArrayObject()
	{
		if (_handle != 0) {
			glDeleteVertexArrays(1, &_handle);
			_handle = 0;
		}
	}

	void VertexArrayObject::SetDebugName(const std::string& name) {
		glObjectLabel(GL_VERTEX_ARRAY, _handle, name.length(), name.c_str());
		_name = name;
	}

	void VertexArrayObject::SetIndexBuffer(const IndexBuffer::sptr& ibo) {
		_indexBuffer = ibo;
		Bind();
		if (_indexBuffer != nullptr) _indexBuffer->Bind();
		else IndexBuffer::UnBind();
		UnBind();
	}

	void VertexArrayObject::AddVertexBuffer(const VertexBuffer::sptr& buffer, const std::vector<BufferAttribute>& attributes)
	{
		const uint32_t stride = attributes[0].Stride;
		const uint32_t instanceDivisor = attributes[0].InstanceDivisor;

		for (int ix = 1; ix < attributes.size(); ix++) {
			LOG_ASSERT(stride == attributes[ix].Stride, "Stride mismatch");
			LOG_ASSERT(instanceDivisor == attributes[ix].InstanceDivisor, "InstanceDivisor mismatch");
		}
		GLsizei& elementCountField = instanceDivisor == 0 ? _vertexCount : _instanceCount;
		if (elementCountField == 0) {
			elementCountField = buffer->GetElementCount();
		} else {
			LOG_ASSERT(buffer->GetElementCount() == elementCountField, "All buffers bound to a VAO should be of the same size in our implementation!");
		}

		VertexBufferBinding binding;
		binding.Buffer = buffer;
		binding.Attributes = attributes;
		binding.BindingIndex = _vertexBuffers.size();
		_vertexBuffers.push_back(binding);

		glVertexArrayVertexBuffer(_handle, binding.BindingIndex, buffer->GetHandle(), 0, attributes[0].Stride);
		for (const BufferAttribute& attrib : attributes) {
			glEnableVertexArrayAttrib(_handle, attrib.Slot);
			glVertexArrayAttribBinding(_handle, attrib.Slot, binding.BindingIndex);
			glVertexArrayAttribFormat(_handle, attrib.Slot, attrib.Size, attrib.Type, attrib.Normalized, attrib.Offset);
			//glVertexAttribPointer(attrib.Slot, attrib.Size, attrib.Type, attrib.Normalized, attrib.Stride, (void*)attrib.Offset);
		}
		if (attributes[0].InstanceDivisor > 0) {
			glad_glVertexArrayBindingDivisor(_handle, binding.BindingIndex, attributes[0].InstanceDivisor);
		}
	}

	void VertexArrayObject::Bind() const {
		glBindVertexArray(_handle);
		__BoundHandle = _handle;
	}

	uint32_t VertexArrayObject::UnBind() {
		glBindVertexArray(0);
		uint32_t result = __BoundHandle;
		__BoundHandle = 0;
		return result;
	}

	void VertexArrayObject::Render(RenderMode mode) const {
		Bind();
		if (_instanceCount == 0) {
			if (_indexBuffer != nullptr) {
				glDrawElements(GL_RENDERMODE_MAP[*mode], _indexBuffer->GetElementCount(), _indexBuffer->GetElementType(), nullptr);
			} else {
				glDrawArrays(GL_RENDERMODE_MAP[*mode], 0, GetElementCount(mode, _vertexCount));
			}
		} else {
			if (_indexBuffer != nullptr) {
				glDrawElementsInstanced(GL_RENDERMODE_MAP[*mode], _indexBuffer->GetElementCount(), _indexBuffer->GetElementType(), nullptr, _instanceCount);
			} else {
				glDrawArraysInstanced(GL_RENDERMODE_MAP[*mode], 0, GetElementCount(mode, _vertexCount), _instanceCount);
			}
		}
		UnBind();
	}
}