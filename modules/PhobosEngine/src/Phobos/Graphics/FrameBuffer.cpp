#include "Phobos/Graphics/FrameBuffer.h"
#include "Phobos/Logging.h"
#include <GLM/glm.hpp>

namespace  Phobos::Graphics {
	uint32_t FrameBuffer::__DrawBufferHandle = 0;
	uint32_t FrameBuffer::__ReadBufferHandle = 0;
	
	FrameBuffer::RenderBuffer::RenderBuffer() :
		RendererID(0),
		Resource(0),
		IsRenderBuffer(false),
		Description(RenderBufferDesc()) { }

	FrameBuffer::FrameBuffer(uint32_t width, uint32_t height, uint8_t numSamples) {
		myWidth = width;
		myHeight = height;
		myBinding = RenderTargetBinding::None;
		LOG_ASSERT(myWidth > 0, "Width must be greater than zero!");
		LOG_ASSERT(myHeight > 0, "Height must be greater than zero!");
		myNumSamples = glm::clamp(numSamples, (uint8_t)1, (uint8_t)_limits.MAX_NUM_SAMPLES);
		isValid = false;

		glCreateFramebuffers(1, &_handle);

		if (myNumSamples > 1) {
			myUnsampledFrameBuffer = std::make_shared<FrameBuffer>(width, height, 1);
		}
	}

	FrameBuffer::~FrameBuffer()
	{
		LOG_INFO("Deleting frame buffer with ID: {}", _handle);
		glDeleteFramebuffers(1, &_handle);
	}

	Texture2D::sptr FrameBuffer::GetAttachment(RenderTargetAttachment attachment) const {
		if (IsColorAttachment(attachment) && myNumSamples > 1) {
			return myUnsampledFrameBuffer->GetAttachment(attachment);
		} else {
			auto it = myLayers.find(attachment);
			if (it != myLayers.end()) {
				if (!it->second.IsRenderBuffer) {
					return std::dynamic_pointer_cast<Texture2D>(it->second.Resource);
				} else
					return nullptr;
			} else
				return nullptr;
		}
	}

	void FrameBuffer::Resize(uint32_t newWidth, uint32_t newHeight) {
		LOG_ASSERT(newWidth > 0, "Width must be greater than zero!");
		LOG_ASSERT(newHeight > 0, "Height must be greater than zero!");

		if (newWidth != myWidth || newHeight != myHeight) {
			myWidth = newWidth;
			myHeight = newHeight;
			for (auto& kvp : myLayers) {
				AddAttachment(kvp.second.Description, false);
			}
			Validate();
		}

		if (myNumSamples > 1) {
			myUnsampledFrameBuffer->Resize(newWidth, newHeight);
		}
	}

	void FrameBuffer::AddAttachment(const RenderBufferDesc& desc, bool showWarnings)
	{
		// Remove any existing render buffers bound to that slot
		if (myLayers.find(desc.Attachment) != myLayers.end()) {
			if (showWarnings) { LOG_WARN("A target is already bound to slot, deleting existing target"); }
			if (myLayers[desc.Attachment].IsRenderBuffer)
				glDeleteRenderbuffers(1, &myLayers[desc.Attachment].RendererID);
			else
				myLayers[desc.Attachment].Resource = nullptr;
		}
		// If this is a new attachment, and it's a color, we need to update our DrawBuffers
		else if (desc.Attachment >= RenderTargetAttachment::Color0 && desc.Attachment <= RenderTargetAttachment::Color7) {  // NEW
			myDrawBuffers.push_back(desc.Attachment);
			glNamedFramebufferDrawBuffers(_handle, myDrawBuffers.size(), reinterpret_cast<const GLenum*>(myDrawBuffers.data()));
		}

		RenderBuffer& buffer = myLayers[desc.Attachment];
		buffer.Description = desc;
		buffer.IsRenderBuffer = !desc.ShaderReadable;

		// Handling for when we can use renderbuffers instead of textures
		if (buffer.IsRenderBuffer) {
			glCreateRenderbuffers(1, &buffer.RendererID); 

			// Enable multisampling on the buffer if required
			if (myNumSamples > 1)
				glNamedRenderbufferStorageMultisample(buffer.RendererID, myNumSamples, *desc.Format, myWidth, myHeight);
			else
				glNamedRenderbufferStorage(buffer.RendererID, *desc.Format, myWidth, myHeight);

			// Attach the renderbuffer to our RenderTarget
			glNamedFramebufferRenderbuffer(_handle, *desc.Attachment, GL_RENDERBUFFER, buffer.RendererID);
		}
		// We are going to use a texture as a backing resource
		else {
			// Create a descriptor for the image
			Texture2DDescription imageDesc = Texture2DDescription();
			imageDesc.Width = myWidth;
			imageDesc.Height = myHeight;
			imageDesc.HorizontalWrap = imageDesc.VerticalWrap = WrapMode::ClampToEdge;
			imageDesc.MinificationFilter = MinFilter::Linear;
			imageDesc.Format = (InternalFormat)desc.Format;
			imageDesc.NumSamples = IsColorAttachment(desc.Attachment) ? myNumSamples : 1;
			imageDesc.GenerateMipMaps = false;

			// Create the image, and store it's info in our buffer tag
			Texture2D::sptr image = std::make_shared<Texture2D>(imageDesc);
			buffer.Resource = image;
			buffer.RendererID = image->GetHandle();

			glNamedFramebufferTexture(_handle, *desc.Attachment, image->GetHandle(), 0);

			if (myNumSamples > 1 && IsColorAttachment(desc.Attachment)) {
				myUnsampledFrameBuffer->AddAttachment(desc, showWarnings);
			}
		}
		isValid = false;
	}

	bool FrameBuffer::Validate()
	{
		if (myNumSamples > 1) {
			myUnsampledFrameBuffer->Validate();
		}

		GLenum result = glCheckNamedFramebufferStatus(_handle, GL_FRAMEBUFFER);
		if (result != GL_FRAMEBUFFER_COMPLETE) {
			switch (result) {
			case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
				LOG_ERROR("Rendertarget failed to validate. One of the attachment points is framebuffer incomplete."); break;
			case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
				LOG_ERROR("Rendertarget failed to validate. There are no attachments!"); break;
			case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
				LOG_ERROR("Rendertarget failed to validate. Draw buffer is incomplete."); break;
			case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
				LOG_ERROR("Rendertarget failed to validate. Read buffer is incomplete."); break;
			case GL_FRAMEBUFFER_UNSUPPORTED:
				LOG_ERROR("Rendertarget failed to validate. Check the formats of the attached targets"); break;
			case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:
				LOG_ERROR("Rendertarget failed to validate. Check the multisampling parameters on all attached targets"); break;
			case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS:
				LOG_ERROR("Rendertarget failed to validate for unknown reason!"); break;
			case GL_FRAMEBUFFER_INCOMPLETE_VIEW_TARGETS_OVR:
				LOG_ERROR("Rendertarget failed to validate. Multiview targets issue!"); break;
			default: LOG_ERROR("Rendertarget failed to validate for unknown reason!");
			}
			isValid = false;
			return false;
		} else {
			isValid = true;
			return true;
		}
	}

	void FrameBuffer::Bind(int slot) const {
		GetAttachment(RenderTargetAttachment::Color0)->Bind(slot);
	}

	void FrameBuffer::Bind(int slot, RenderTargetAttachment attachment) {
		GetAttachment(attachment)->Bind(slot);
	}

	void FrameBuffer::Bind(RenderTargetBinding bindMode, bool multisampled) const {
		if (multisampled && myNumSamples > 1) {
			myUnsampledFrameBuffer->Bind(bindMode);
		} else {
			myBinding = bindMode;
			glBindFramebuffer((GLenum)bindMode, _handle);
			if (*myBinding == GL_DRAW_FRAMEBUFFER | *myBinding == GL_FRAMEBUFFER)
				__DrawBufferHandle = _handle;
			if (*myBinding == GL_READ_FRAMEBUFFER | *myBinding == GL_FRAMEBUFFER)
				__ReadBufferHandle = _handle;
		}
	}

	void FrameBuffer::UnBind() const {
		if (myBinding != RenderTargetBinding::None) {
			if (myNumSamples > 1) {
				glBindFramebuffer(GL_READ_FRAMEBUFFER, _handle);
				glBindFramebuffer(GL_DRAW_FRAMEBUFFER, myUnsampledFrameBuffer->_handle);
				for (auto& kvp : myLayers) {
					if (IsColorAttachment(kvp.first)) {
						glReadBuffer(*kvp.first);
						glDrawBuffer(*kvp.first);
						Blit({ 0, 0, myWidth, myHeight }, { 0, 0, myWidth, myHeight }, BufferFlags::Color, MagFilter::Linear);
					}
				}
				glBindFramebuffer(GL_READ_FRAMEBUFFER, __ReadBufferHandle);
				glBindFramebuffer(GL_DRAW_FRAMEBUFFER, __DrawBufferHandle);
			}
			glBindFramebuffer((GLenum)myBinding, 0);
			myBinding = RenderTargetBinding::None;
			if (*myBinding == GL_DRAW_FRAMEBUFFER | *myBinding == GL_FRAMEBUFFER)
				__DrawBufferHandle = 0;
			if (*myBinding == GL_READ_FRAMEBUFFER | *myBinding == GL_FRAMEBUFFER)
				__ReadBufferHandle = 0;
		}
		if (myNumSamples > 1 && myUnsampledFrameBuffer->myBinding != RenderTargetBinding::None) {
			myUnsampledFrameBuffer->UnBind();
		}
	}

	void FrameBuffer::Blit(const glm::ivec4& srcBounds, const glm::ivec4& dstBounds, BufferFlags flags, MagFilter filterMode) {
		glBlitFramebuffer(
			srcBounds.x, srcBounds.y, srcBounds.x + srcBounds.z, srcBounds.y + srcBounds.w,
			dstBounds.x, dstBounds.y, dstBounds.x + dstBounds.z, dstBounds.y + dstBounds.w,
			*flags, *filterMode);
	}

	FrameBuffer::sptr FrameBuffer::Clone() const {
		auto result = std::make_shared<FrameBuffer>(myWidth, myHeight, myNumSamples);

		for (auto& kvp : myLayers) {
			result->AddAttachment(kvp.second.Description);
		}
		result->Validate();
		return result;
	}

	void FrameBuffer::SetDebugName(const std::string& value) {
		glObjectLabel(GL_FRAMEBUFFER, _handle, -1, value.c_str());
		// Pass the name down the call chain
		IAsset::SetName(value);

		for (const auto& attachment : myLayers) {
			char name[128];
			sprintf_s(name, 128, "%s_%s", value.c_str(), (~attachment.first).c_str());
			if (attachment.second.IsRenderBuffer) {
				glObjectLabel(GL_RENDERBUFFER, attachment.second.RendererID, -1, name);
			} else {
				glObjectLabel(GL_TEXTURE, attachment.second.RendererID, -1, name);
			}
		}
	}
}