#include "Phobos/Graphics/ShaderMaterial.h"

#include <ENTT/entt.hpp>

namespace Phobos::Graphics {
	using namespace Utils;

	ShaderMaterial::ShaderMaterial()
		: _shader(nullptr), RenderLayer(0), IsTransparent(false), DepthWrite(true), DepthRead(true)
	{
	}

	ShaderMaterial::~ShaderMaterial() {
		LOG_INFO("Deleting material");
	}

	void ShaderMaterial::Apply()
	{
		if (_shader != nullptr) {
			int slot = 1;
			for (auto& [hash, data] : _data) {
				ShaderDataTypecode typeCode = GetShaderDataTypeCode(data.Type);
				if (typeCode == ShaderDataTypecode::Texture) {
					ITexture::sptr texture = std::dynamic_pointer_cast<ITexture>(_resources[hash]);
					if (texture != nullptr)
						texture->Bind(slot);
					else
						ITexture::Unbind(slot);
					_shader->SetUniform(data.Location, data.Type, &slot);
					slot++;
				} else {
					_shader->SetUniform(data.Location, data.Type, data.ArraySize > 1 ? data.ArrayBlock : data.Value, data.ArraySize);
				}
			}
		}
	}

	void ShaderMaterial::SetShader(const Shader::sptr shader) {
		if (_shader != shader) {
			_shader = shader;

			if (shader == nullptr)
			{
				_data.clear();
				_resources.clear();
			} else {
				auto it = _data.begin();
				while (it != _data.cend()) {
					Shader::UniformInfo info;
					if (shader->FindUniform(it->second.Name, info)) {
						it->second.Location = info.Location;
						auto resIt = _resources.find(it->first);
						if (resIt != _resources.end()) _resources.erase(resIt);
						++it;
					} else {
						it = _data.erase(it);
					}
				}

				for (auto& uniform : shader->GetUniforms()) {
					//entt::hashed_string::value_type hash = entt::hashed_string::value(uniform.Name.c_str());
					if (_data.find(uniform.Name) == _data.end()) {
						Set(uniform.Name, uniform.Type, uniform.DefaultValueBuffer);
					}
				}
			}
		}
	}

	ShaderMaterial::sptr ShaderMaterial::Clone() const {
		ShaderMaterial::sptr result = AssetDatabase::Get().Create<ShaderMaterial>(GetName());
		result->SetShader(_shader);
		result->_data = _data;
		result->_resources = _resources;
		result->RenderLayer = RenderLayer;
		result->IsTransparent = IsTransparent;
		result->DepthRead = DepthRead;
		result->DepthWrite = DepthWrite;
		return result;
	}

	void ShaderMaterial::Set(const std::string& name, ShaderDataType type, const void* value, size_t count) {
		entt::hashed_string::value_type hash;
		UniformData& data = _GetUniform(name, &hash);
		if (data.Type != type) {
			if (data.Type != ShaderDataType::None)
				LOG_ERROR("Type mismatch for \"{}\", uniform is {}, passed {} ()", name, data.Type, type, _shader->GetName())
		} else {
			if (GetShaderDataTypeCode(type) == ShaderDataTypecode::Texture) {
				*(ResourceBlock*)(data.Value) = *(ResourceBlock*)value;
				_resources[name] = AssetDatabase::Get().FindAssetByGuid(((ResourceBlock*)value)->ResourceGuid);
			} else {
				if (data.ArraySize > 1) {
					memcpy(data.ArrayBlock, value, ShaderDataTypeSize(type) * count);
				} else {
					memcpy(data.Value, value, ShaderDataTypeSize(type));
				}
			}
		}
	}

	ShaderMaterial::UniformData& ShaderMaterial::_GetUniform(const std::string& name, entt::hashed_string::value_type* hashOut) {
		UniformData& data = _data[name];
		if (data.Location == -2) {
			Shader::UniformInfo uniform;
			if (_shader->FindUniform(name, uniform, true)) {
				if (!uniform.Configurable) {
					_data[name].Location = -1;
					LOG_WARN("Skipping material property \"{}\", not set to be a configurable property", name);
				} else {
					data.Location = uniform.Location;
					data.Type = uniform.Type;
					data.Name = uniform.Name;
					data.ArraySize = uniform.ArraySize;
					data.HasEditor = uniform.EditorHints != nullptr;
					if (uniform.ArraySize > 1) {
						data.ArrayBlock = malloc(ShaderDataTypeSize(uniform.Type) * data.ArraySize);
						memset(data.ArrayBlock, 0, ShaderDataTypeSize(uniform.Type) * data.ArraySize);
						if (uniform.DefaultArrayData != nullptr) {
							memcpy(data.ArrayBlock, uniform.DefaultArrayData, ShaderDataTypeSize(uniform.Type) * data.ArraySize);
						}
					} else {
						memcpy(data.Value, uniform.DefaultValueBuffer, ShaderDataTypeSize(uniform.Type));
					}
				}
			} else {
				_data[name].Location = -1;
				LOG_WARN("Skipping material property \"{}\", not found in shader", name);
			}
		}
		if (hashOut != nullptr) {
			*hashOut = entt::hashed_string::value(name.c_str());
		}
		return data;
	}

}
