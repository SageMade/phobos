#include "Phobos/Graphics/Texture2D.h"
#include <GLM/glm.hpp>

namespace Phobos::Graphics {
	Texture2D::Texture2D(const Texture2DDescription& description) :
		ITexture(), _description(description)
	{
		LOG_ASSERT(description.Width <= _limits.MAX_TEXTURE_SIZE, "Texture width must be < {}", _limits.MAX_TEXTURE_SIZE);
		LOG_ASSERT(description.Height <= _limits.MAX_TEXTURE_SIZE, "Texture height must be < {}", _limits.MAX_TEXTURE_SIZE);

		_RecreateTexture();
	}

	void Texture2D::_RecreateTexture() {
		if (_handle != 0) {
			glDeleteTextures(1, &_handle);
			_handle = 0;
		}

		_description.NumSamples = glm::clamp(_description.NumSamples, 1u, (uint32_t)_limits.MAX_NUM_SAMPLES);
		_description.MipmapLevels = _description.NumSamples > 1u ? 1u : _description.MipmapLevels;
		_description.MipmapLevels = _description.GenerateMipMaps && _description.MipmapLevels == 0 ?
			(1 + floor(log2(glm::max(_description.Width, _description.Height)))) :
			(_description.MipmapLevels);

		if (_description.GenerateMipMaps && _description.NumSamples > 1) {
			LOG_WARN("[Texture2D] Attempting to create a texture with mipmaps and multi-sampling, multisampling parameters will be ignored!");
		}

		glCreateTextures(_description.NumSamples > 1 ? GL_TEXTURE_2D_MULTISAMPLE : GL_TEXTURE_2D, 1, &_handle);

		if (_description.MaxAnisotropic < 0.0f) {
			_description.MaxAnisotropic = ITexture::GetLimits().MAX_ANISOTROPY;
		}

		if (_description.Width * _description.Height > 0 && _description.Format != InternalFormat::Unknown)
		{
			if (_description.NumSamples == 1) {
				glTextureStorage2D(_handle, _description.GenerateMipMaps ? _description.MipmapLevels : 1, *_description.Format, _description.Width, _description.Height);

				glTextureParameteri(_handle, GL_TEXTURE_WRAP_S, *_description.HorizontalWrap);
				glTextureParameteri(_handle, GL_TEXTURE_WRAP_T, *_description.VerticalWrap);
				glTextureParameteri(_handle, GL_TEXTURE_MIN_FILTER, *_description.MinificationFilter);
				glTextureParameteri(_handle, GL_TEXTURE_MAG_FILTER, *_description.MagnificationFilter);
				glTextureParameterf(_handle, GL_TEXTURE_MAX_ANISOTROPY, _description.MaxAnisotropic);
			} else {
				glTextureStorage2DMultisample(_handle, _description.NumSamples, *_description.Format, _description.Width, _description.Height, true);
			}
		}
	}

	void Texture2D::LoadData(const Texture2DData::sptr& data) {
		LOG_ASSERT(_description.NumSamples == 1, "Cannot upload data to a multi-sampled texture!"); // NEW
		
		if (_description.Width != data->GetWidth() ||
			_description.Height != data->GetHeight())
		{
			_description.Width = data->GetWidth();
			_description.Height = data->GetHeight();

			if (_description.Format == InternalFormat::Unknown) {
				_description.Format = data->GetRecommendedFormat();
			}

			_RecreateTexture();
		}

		// Align the data store to the size of a single component in
		// See https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glPixelStore.xhtml
		int componentSize = (GLint)GetTexelComponentSize(data->GetPixelType());
		glPixelStorei(GL_PACK_ALIGNMENT, componentSize);

		// Upload our data to our image
		glTextureSubImage2D(_handle, 0, 0, 0, _description.Width, _description.Height, *data->GetFormat(), *data->GetPixelType(), data->GetDataPtr());

		if (_description.GenerateMipMaps) {
			glGenerateTextureMipmap(_handle);
		}
	}

	Texture2D::sptr Texture2D::LoadAndCreateFromFile(const std::string& path) {
		Texture2D::sptr result = Texture2D::Create();
		result->LoadFromFile(path);
		return result;
	}
	void Texture2D::LoadFromFile(const std::string& path) {
		Texture2DData::sptr data = Texture2DData::LoadFromFile(path);
		LOG_ASSERT(data != nullptr, "Failed to load image from file!");
		LoadData(data);
	}

	void Texture2D::SetMinFilter(MinFilter filter) {
		_description.MinificationFilter = filter;
		if (_handle != 0) {
			glTextureParameteri(_handle, GL_TEXTURE_MIN_FILTER, (GLenum)_description.MinificationFilter);
		}
	}

	void Texture2D::SetMagFilter(MagFilter filter) {
		_description.MagnificationFilter = filter;
		if (_handle != 0) {
			glTextureParameteri(_handle, GL_TEXTURE_MAG_FILTER, (GLenum)_description.MagnificationFilter);
		}
	}

	void Texture2D::SetWrapS(WrapMode mode) {
		_description.HorizontalWrap = mode;
		if (_handle != 0) {
			glTextureParameteri(_handle, GL_TEXTURE_WRAP_S, (GLenum)_description.HorizontalWrap);
		}
	}

	void Texture2D::SetWrapT(WrapMode mode) {
		_description.VerticalWrap = mode;
		if (_handle != 0) {
			glTextureParameteri(_handle, GL_TEXTURE_WRAP_T, (GLenum)_description.VerticalWrap);
		}
	}

	void Texture2D::SetAnisotropicFiltering(float level)
	{
		if (level < 0.0f) {
			level = ITexture::GetLimits().MAX_ANISOTROPY;
		}
		_description.MaxAnisotropic = level;
		if (_handle != 0) {
			glTextureParameterf(_handle, GL_TEXTURE_MAX_ANISOTROPY, _description.MaxAnisotropic);
		}
	}
}
