#include "Phobos/Graphics/Shader.h"
#include "Phobos/Logging.h"
#include "Phobos/Utils/FileHelpers.h"
#include "Phobos/EditorHints.h"
#include <fstream>
#include <regex>
#include <sstream>

#include "Phobos/Utils/StringUtils.h"

namespace Phobos::Graphics {
	using namespace Utils;
	
	Shader::Shader() : IAsset()
	{
		_handle = glCreateProgram();
		memset(_shaders, 0, sizeof(_shaders));
	}

	Shader::~Shader() {
		if (_handle != 0) {
			glDeleteProgram(_handle);
			_handle = 0;
			LOG_INFO("Deleting shader program");

			for (auto& uniform : _uniforms) {
				if (uniform.DefaultArrayData != nullptr) {
					free(uniform.DefaultArrayData);
				}
				if (uniform.EditorHints != nullptr) {
					delete uniform.EditorHints;
				}
			}
		}
	}

	bool Shader::LoadShaderPart(const char* source, ShaderStageType type)
	{
		// Creates a new shader part (VS, FS, GS, etc...)
		GLuint handle = glCreateShader(ToGLStage(type));

		// Load the GLSL source and compile it
		glShaderSource(handle, 1, &source, nullptr);
		glCompileShader(handle);

		// Get the compilation status for the shader part
		GLint status = 0;
		glGetShaderiv(handle, GL_COMPILE_STATUS, &status);

		if (status == GL_FALSE) {
			// Get the size of the error log
			GLint logSize = 0;
			glGetShaderiv(handle, GL_INFO_LOG_LENGTH, &logSize);

			// Create a new character buffer for the log
			char* log = new char[logSize];

			// Get the log
			glGetShaderInfoLog(handle, logSize, &logSize, log);

			// Dump error log
			LOG_ERROR("Failed to compile shader part:\n{}", log);

			// Clean up our log memory
			delete[] log;

			// Delete the broken shader result
			glDeleteShader(handle);
			handle = 0;
			throw std::runtime_error("Failed to compile shader part!");
		}
		if (_shaders[*type] != 0) {
			glDeleteShader(_shaders[*type]);
		}
		_shaders[*type] = handle;

		if (_filePath.empty()) {
			_shaderSources[type] = source;
		}

		return status != GL_FALSE;
	}

	bool Shader::LoadShaderPartFromFile(const char* path, ShaderStageType type) {
		std::ifstream file(path);
		if (!file.is_open()) {
			LOG_ERROR("File not found: {}", path);
			throw std::runtime_error("File not found, see logs for more information");
		}

		std::stringstream stream;
		stream << file.rdbuf();
		bool result = LoadShaderPart(stream.str().c_str(), type);
		file.close();
		return result;
	}

	void Shader::LoadFromFile(const std::string& filename)
	{
		_filePath = filename;
		std::string source = FileHelpers::ReadResolveIncludes(filename);
		std::map<std::string, EditorHints> hints;
		std::map<ShaderStageType, std::string> parts = _Preprocess(source, &hints);
		for (auto& kvp : parts) {
			LoadShaderPart(kvp.second.c_str(), kvp.first);
		}
		Link();
		for (const auto& [name, hint] : hints) {
			SetUniformEditorHints(name, hint);
			SetUniformConfigurable(name, true);
		}
	}

	void Shader::SetUniform(int location, ShaderDataType type, const void* data, int count, bool transposed)
	{
		switch (type)
		{
		case ShaderDataType::Float:   glProgramUniform1fv(_handle, location, count, static_cast<const float*>(data)); break;
		case ShaderDataType::Float2:  glProgramUniform2fv(_handle, location, count, static_cast<const float*>(data)); break;
		case ShaderDataType::Float3:  glProgramUniform3fv(_handle, location, count, static_cast<const float*>(data)); break;
		case ShaderDataType::Float4:  glProgramUniform4fv(_handle, location, count, static_cast<const float*>(data)); break;
		case ShaderDataType::Mat2:    glProgramUniformMatrix2fv(_handle, location, count, transposed, static_cast<const float*>(data)); break;
		case ShaderDataType::Mat3:    glProgramUniformMatrix3fv(_handle, location, count, transposed, static_cast<const float*>(data)); break;
		case ShaderDataType::Mat4:    glProgramUniformMatrix4fv(_handle, location, count, transposed, static_cast<const float*>(data)); break;
		case ShaderDataType::Mat2x3:  glProgramUniformMatrix2x3fv(_handle, location, count, transposed, static_cast<const float*>(data)); break;
		case ShaderDataType::Mat2x4:  glProgramUniformMatrix2x4fv(_handle, location, count, transposed, static_cast<const float*>(data)); break;
		case ShaderDataType::Mat3x2:  glProgramUniformMatrix3x2fv(_handle, location, count, transposed, static_cast<const float*>(data)); break;
		case ShaderDataType::Mat3x4:  glProgramUniformMatrix3x4fv(_handle, location, count, transposed, static_cast<const float*>(data)); break;
		case ShaderDataType::Mat4x2:  glProgramUniformMatrix4x2fv(_handle, location, count, transposed, static_cast<const float*>(data)); break;
		case ShaderDataType::Mat4x3:  glProgramUniformMatrix4x3fv(_handle, location, count, transposed, static_cast<const float*>(data)); break;
		case ShaderDataType::Int:     glProgramUniform1iv(_handle, location, count, static_cast<const int*>(data)); break;
		case ShaderDataType::Int2:    glProgramUniform2iv(_handle, location, count, static_cast<const int*>(data)); break;
		case ShaderDataType::Int3:    glProgramUniform3iv(_handle, location, count, static_cast<const int*>(data)); break;
		case ShaderDataType::Int4:    glProgramUniform4iv(_handle, location, count, static_cast<const int*>(data)); break;
		case ShaderDataType::Uint:    glProgramUniform1uiv(_handle, location, count, static_cast<const uint32_t*>(data));  break;
		case ShaderDataType::Uint2:   glProgramUniform2uiv(_handle, location, count, static_cast<const uint32_t*>(data)); break;
		case ShaderDataType::Uint3:   glProgramUniform3uiv(_handle, location, count, static_cast<const uint32_t*>(data)); break;
		case ShaderDataType::Uint4:   glProgramUniform4uiv(_handle, location, count, static_cast<const uint32_t*>(data)); break;
		case ShaderDataType::Double:  glProgramUniform1dv(_handle, location, count, static_cast<const double*>(data)); break;
		case ShaderDataType::Double2: glProgramUniform2dv(_handle, location, count, static_cast<const double*>(data)); break;
		case ShaderDataType::Double3: glProgramUniform3dv(_handle, location, count, static_cast<const double*>(data)); break;
		case ShaderDataType::Double4: glProgramUniform4dv(_handle, location, count, static_cast<const double*>(data)); break;
		case ShaderDataType::Dmat2:   glProgramUniformMatrix2dv(_handle, location, count, transposed, static_cast<const double*>(data)); break;
		case ShaderDataType::Dmat3:   glProgramUniformMatrix3dv(_handle, location, count, transposed, static_cast<const double*>(data)); break;
		case ShaderDataType::Dmat4:   glProgramUniformMatrix4dv(_handle, location, count, transposed, static_cast<const double*>(data)); break;
		case ShaderDataType::Dmat2x3: glProgramUniformMatrix2x3dv(_handle, location, count, transposed, static_cast<const double*>(data)); break;
		case ShaderDataType::Dmat2x4: glProgramUniformMatrix2x4dv(_handle, location, count, transposed, static_cast<const double*>(data)); break;
		case ShaderDataType::Dmat3x2: glProgramUniformMatrix3x2dv(_handle, location, count, transposed, static_cast<const double*>(data)); break;
		case ShaderDataType::Dmat3x4: glProgramUniformMatrix3x4dv(_handle, location, count, transposed, static_cast<const double*>(data)); break;
		case ShaderDataType::Dmat4x2: glProgramUniformMatrix4x2dv(_handle, location, count, transposed, static_cast<const double*>(data)); break;
		case ShaderDataType::Dmat4x3: glProgramUniformMatrix4x3dv(_handle, location, count, transposed, static_cast<const double*>(data)); break;
		case ShaderDataType::Tex1D:
		case ShaderDataType::Tex1D_Array:
		case ShaderDataType::Tex1D_Shadow:
		case ShaderDataType::Tex1D_ShadowArray:
		case ShaderDataType::Tex2D:
		case ShaderDataType::Tex2D_Rect:
		case ShaderDataType::Tex2D_Rect_Shadow:
		case ShaderDataType::Tex2D_Array:
		case ShaderDataType::Tex2D_Shadow:
		case ShaderDataType::Tex2D_ShadowArray:
		case ShaderDataType::Tex2D_Multisample:
		case ShaderDataType::Tex2D_MultisampleArray:
		case ShaderDataType::Tex3D:
		case ShaderDataType::TexCube:
		case ShaderDataType::TexCubeShadow:
		case ShaderDataType::Tex1D_Int:
		case ShaderDataType::Tex1D_Int_Array:
		case ShaderDataType::Tex2D_Int:
		case ShaderDataType::Tex2D_Int_Rect:
		case ShaderDataType::Tex2D_Int_Array:
		case ShaderDataType::Tex2D_Int_Multisample:
		case ShaderDataType::Tex2D_Int_MultisampleArray:
		case ShaderDataType::Tex3D_Int:
		case ShaderDataType::TexCube_Int:
		case ShaderDataType::Tex1D_Uint:
		case ShaderDataType::Tex2D_Uint_Rect:
		case ShaderDataType::Tex1D_Uint_Array:
		case ShaderDataType::Tex2D_Uint:
		case ShaderDataType::Tex2D_Uint_Array:
		case ShaderDataType::Tex2D_Uint_Multisample:
		case ShaderDataType::Tex2D_Uint_MultisampleArray:
		case ShaderDataType::Tex3D_Uint:
		case ShaderDataType::TexCube_Uint:
		case ShaderDataType::BufferTexture:
		case ShaderDataType::BufferTextureInt:
		case ShaderDataType::BufferTextureUint:
			glProgramUniform1iv(_handle, location, count, static_cast<const int*>(data));
		case ShaderDataType::None: break;
		default:
		{
			static std::map<ShaderDataType, bool> loggedWarns;
			if (!loggedWarns[type]) {
				LOG_WARN("No support for uniforms of type \"{}\", first seen in \"{}\", skipping...", type, GetName());
				loggedWarns[type] = true;
			}
		}
		}
	}

	bool Shader::Link()
	{
		LOG_ASSERT(_shaders[0] != 0 && _shaders[1] != 0, "Must attach both a vertex and fragment shader!");

		// Attach our two shaders
		for (int ix = 0; ix <= *ShaderStageType::Max; ix++) {
			if (_shaders[ix] != 0) {
				glAttachShader(_handle, _shaders[ix]);
			}
		}

		// Perform linking
		glLinkProgram(_handle);

		// Remove shader parts to save space (we can do this since we only needed the shader parts to compile an actual shader program)
		for (int ix = 0; ix <= *ShaderStageType::Max; ix++) {
			if (_shaders[ix] != 0) {
				glDetachShader(_handle, _shaders[ix]);
				glDeleteShader(_shaders[ix]);
				_shaders[ix] = 0;
			}
		}

		GLint status = 0;
		glGetProgramiv(_handle, GL_LINK_STATUS, &status);

		if (status == GL_FALSE)
		{
			// Get the length of the log
			GLint length = 0;
			glGetProgramiv(_handle, GL_INFO_LOG_LENGTH, &length);

			if (length > 0) {
				// Read the log from openGL
				char* log = new char[length];
				glGetProgramInfoLog(_handle, length, &length, log);
				LOG_ERROR("Shader failed to link:\n{}", log);
				delete[] log;
			} else {
				LOG_ERROR("Shader failed to link for an unknown reason!");
			}
		}

		_Introspect();

		return status != GL_FALSE;
	}

	void Shader::Bind() {
		glUseProgram(_handle);
	}

	void Shader::UnBind() {
		glUseProgram(0);
	}

	bool Shader::FindUniform(const std::string& name, UniformInfo& out, bool includeNonConfigurable)
	{
		for (UniformInfo& uniform : _uniforms) {
			if (uniform.Name == name) {
				if (!uniform.Configurable && !includeNonConfigurable) {
					return false;
				} else {
					out = uniform;
					return true;
				}
			}
		}
		return false;
	}

	void Shader::SetUniformConfigurable(const std::string& name, bool isConfigurable)
	{
		for (UniformInfo& uniform : _uniforms) {
			if (uniform.Name == name) {
				uniform.Configurable = isConfigurable;
				return;
			}
		}
	}

	void Shader::SetUniformEditorHints(const std::string& name, const EditorHints& hints)
	{
		for (UniformInfo& uniform : _uniforms) {
			if (uniform.Name == name) {
				if (uniform.EditorHints == nullptr) {
					uniform.EditorHints = new EditorHints(hints);
				} else {
					*uniform.EditorHints = hints;
				}
				return;
			}
		}
	}

	EditorHints* Shader::GetEditorHints(int location) {
		for (UniformInfo& uniform : _uniforms) {
			if (uniform.Location == location) {
				return uniform.EditorHints;
			}
		}
		return nullptr;
	}

	void Shader::SetName(const std::string& name) {
		IAsset::SetName(name);
		glObjectLabel(GL_PROGRAM, _handle, name.length(), name.c_str());
	}

	void Shader::_Introspect() {
		_InstrospectUniforms();
		_IntrospectUniformBlocks();
	}

	void Shader::_InstrospectUniforms()
	{
		int numInputs = 0;
		glGetProgramInterfaceiv(_handle, GL_UNIFORM, GL_ACTIVE_RESOURCES, &numInputs);

		for (int ix = 0; ix < numInputs; ix++) {
			static GLenum pNames[] = {
				GL_NAME_LENGTH,
				GL_TYPE,
				GL_ARRAY_SIZE,
				GL_LOCATION
			};
			int numProps = 0;
			int props[4];
			glGetProgramResourceiv(_handle, GL_UNIFORM, ix, 4, pNames, 4, &numProps, props);

			if (props[3] == -1)
				continue;

			UniformInfo e;
			std::string name;
			name.resize(props[0] - 1);
			int length = 0;
			glGetProgramResourceName(_handle, GL_UNIFORM, ix, props[0], &length, &name[0]);
			e.Name = name;
			e.Type = FromGLShaderDataType(props[1]);
			e.Location = props[3];
			e.ArraySize = props[2];
			if (e.ArraySize > 1) {
				e.Name = e.Name.substr(0, e.Name.find('['));
			}
			memset(e.DefaultValueBuffer, 0, ShaderDataTypeSize(e.Type));
			e.DefaultArrayData = nullptr;
			e.EditorHints = nullptr;
			e.Configurable = GetShaderDataTypeCode(e.Type) != ShaderDataTypecode::Matrix;
			LOG_TRACE("\t\tDetected a new uniform: {}[{}]({}) -> {}", e.Name, e.ArraySize, e.Location, e.Type);
			_uniforms.push_back(e);
			_uniformLocs[e.Name] = e.Location;
		}
	}

	void Shader::_IntrospectUniformBlocks() {
		int numBlocks = 0;
		glGetProgramInterfaceiv(_handle, GL_UNIFORM_BLOCK, GL_ACTIVE_RESOURCES, &numBlocks);

		for (int ix = 0; ix < numBlocks; ix++) {
			static GLenum pNamesBlockProperties[] = {
				GL_NUM_ACTIVE_VARIABLES,
				GL_BUFFER_BINDING,
				GL_BUFFER_DATA_SIZE,
				GL_NAME_LENGTH
			};
			int results[4];
			glGetProgramResourceiv(_handle, GL_UNIFORM_BLOCK, ix, 4, pNamesBlockProperties, 4, NULL, results);

			if (!results[0])
				continue;

			static GLenum pNamesActiveVars[] = {
				GL_ACTIVE_VARIABLES
			};
			std::vector<int> activeVars(results[0]);
			glGetProgramResourceiv(_handle, GL_UNIFORM_BLOCK, ix, 1, pNamesActiveVars, results[0], NULL, &activeVars[0]);

			UniformBlockInfo block;
			block.Binding = results[1];
			block.SizeInBytes = results[2];
			block.NumVariables = results[0];
			std::string name;
			name.resize(results[3] - 1);
			glGetProgramResourceName(_handle, GL_UNIFORM_BLOCK, ix, results[3], NULL, &name[0]);
			block.Binding = glGetProgramResourceIndex(_handle, GL_UNIFORM_BLOCK, name.c_str());
			block.Name = name;
			block.SubUniforms.reserve(results[0]);
			block.BlockIndex = glGetUniformBlockIndex(_handle, name.c_str());

			LOG_TRACE("\t\tDetected a new uniform block \"{}\" with {} variables bound at {} ", block.Name, block.NumVariables, block.Binding);

			for (int v = 0; v < results[0]; v++) {
				static GLenum pNames[] = {
					GL_NAME_LENGTH,
					GL_TYPE,
					GL_ARRAY_SIZE,
					GL_LOCATION
				};
				int props[4];
				glGetProgramResourceiv(_handle, GL_UNIFORM, activeVars[v], 4, pNames, 4, NULL, props);
				UniformInfo var;
				std::string name;
				name.resize(props[0] - 1);
				glGetProgramResourceName(_handle, GL_UNIFORM, activeVars[v], props[0], NULL, &name[0]);
				var.Name = name;
				var.Type = FromGLShaderDataType(props[1]);
				var.Location = props[3];
				var.ArraySize = props[2];
				if (var.ArraySize > 1) {
					var.Name = var.Name.substr(0, var.Name.find('['));
				}
				LOG_TRACE("\t\tDetected a new uniform: {}[{}] -> {}", var.Name, var.ArraySize, var.Type);
				block.SubUniforms.push_back(var);
			}

			_uniformBlocks.push_back(block);
		}
	}

	std::map<ShaderStageType, std::string> Shader::_Preprocess(const std::string& source, std::map<std::string, EditorHints>* editorHints)
	{
		if (editorHints != nullptr) {
			std::map<std::string, EditorHints>& map = *editorHints;
			const char* editorToken = "#pragma editor";
			const size_t editorTokenLen = const_strlen(editorToken);

			std::regex uniform_regex = std::regex(R"LIT(\s*(?:layout \(.*\)\s*)?uniform\s+[\w]+\s+([\w]+)\;)LIT");

			size_t seek = source.find(editorToken, 0);
			std::vector<std::string> assignPairs;
			while (seek != std::string::npos) {
				size_t eol = source.find_first_of("\r\n", seek);
				LOG_ASSERT(eol != std::string::npos, "Syntax error, no eol found after #pragma editor");
				size_t typeStart = seek + editorTokenLen;
				std::string editorParams = source.substr(typeStart, eol - typeStart);
				StringTools::Trim(editorParams);
				EditorHints hints;
				if (editorParams != "") {
					assignPairs.clear();
					StringTools::Split(editorParams, assignPairs, ",");
					for (std::string& pair : assignPairs) {
						StringTools::Trim(pair);
						auto it = pair.find("=");
						std::string key = pair.substr(0, it);
						StringTools::Trim(key);
						EditorHintFields field = ParseEditorHintFields(key, EditorHintFields::Invalid);
						std::string value = it == std::string::npos ? "" : pair.substr(it + 1);
						StringTools::Trim(value);
						if (field != EditorHintFields::Invalid) {
							switch (field) {
							case EditorHintFields::RenderType:
							{
								EditorRenderType type = ParseEditorRenderType(value, EditorRenderType::Default);
								hints.RenderType = type;
							} break;
							case EditorHintFields::Min:
							{
								float v = atof(value.c_str());
								hints.Min = v;
							} break;
							case EditorHintFields::Max:
							{
								float v = atof(value.c_str());
								hints.Max = v;
							} break;
							case EditorHintFields::Power:
							{
								float v = atof(value.c_str());
								hints.Power = v;
							} break;
							case EditorHintFields::Invalid:;
							default: break;;
							}
						} else {
							LOG_WARN("\"{}\" is not a valid editor hint field", key);
						}
					}
				}

				size_t endNextLine = source.find_first_of("\r\n", eol + 2);
				std::string nextLine = source.substr(eol + 2, endNextLine - eol - 2);
				std::cmatch matches;
				if (std::regex_match(nextLine.c_str(), matches, uniform_regex)) {
					std::string uniformName = matches[1];
					if (map.find(uniformName) != map.end()) {
						LOG_WARN("Duplicate editor hints for \"{}\", skipping second", uniformName);
					} else {
						map[uniformName] = hints;
					}
				} else {
					LOG_WARN("Syntax error, line following editor hints is not a uniform");
				}

				seek = source.find(editorToken, endNextLine);
			}
		}

		std::map<ShaderStageType, std::string> result;
		const char* typeToken = "#type";
		const size_t tokenLength = const_strlen(typeToken);

		std::string shared = "";

		// Handle includes
		size_t seek = source.find(typeToken, 0);

		if (seek != std::string::npos) {
			shared = source.substr(0, seek);
		}

		while (seek != std::string::npos) {
			size_t eol = source.find_first_of("\r\n", seek);
			LOG_ASSERT(eol != std::string::npos, "Syntax error, no eol found after type token");
			size_t typeStart = seek + tokenLength + 1;
			std::string type = source.substr(typeStart, eol - typeStart);
			StringTools::Trim(type);
			ShaderStageType stageType = ParseShaderStageType(type, ShaderStageType::Unknown);
			LOG_ASSERT(stageType != ShaderStageType::Unknown, "Syntax error, type not one of the allowed values");

			size_t nextLine = source.find_first_not_of("\r\n", eol);
			LOG_ASSERT(nextLine != std::string::npos, "Syntax error, type must be followed by implementation");
			seek = source.find(typeToken, nextLine);

			result[stageType] = shared + ((seek == std::string::npos) ? source.substr(nextLine) : source.substr(nextLine, seek - nextLine));

		}
		return result;
	}

	int Shader::GetUniformLocation(const std::string& name) {
		// Search the map for the given name
		std::unordered_map<std::string, int>::const_iterator it = _uniformLocs.find(name);
		int result = -1;

		// If our entry was not found, we call glGetUniform and store it for next time
		if (it == _uniformLocs.end()) {
			result = glGetUniformLocation(_handle, name.c_str());
			_uniformLocs[name] = result;

			if (result == -1) {
				LOG_WARN("Ignoring uniform \"{}\"", name);
			}
		}
		// Otherwise, we had a value in the map, return it
		else {
			result = it->second;
		}

		return result;
	}
}
