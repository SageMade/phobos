#include "Phobos/Gameplay/Camera.h"

#include <GLM/gtc/matrix_transform.hpp>

namespace Phobos::Gameplay {
	Camera::Camera() :
		_isOrtho(false),
		_orthoHeight(1.0f),
		_nearPlane(0.1f),
		_farPlane(1000.0f),
		_fovRadians(glm::radians(90.0f)),
		_aspectRatio(1.0f),
		_projection(glm::mat4(1.0f)),
		_isDirty(true)
	{
		__CalculateProjection();
	}

	void Camera::SetIsOrtho(bool isOrtho) {
		_isOrtho = isOrtho;
		__CalculateProjection();
	}

	void Camera::SetOrthoHeight(float orthoHeight) {
		_orthoHeight = orthoHeight;
		__CalculateProjection();
	}

	void Camera::ResizeWindow(int windowWidth, int windowHeight) {
		if (windowWidth * windowHeight > 0) {
			_aspectRatio = static_cast<float>(windowWidth) / static_cast<float>(windowHeight);
			__CalculateProjection();
		}
	}

	void Camera::SetFovRadians(float value) {
		_fovRadians = value;
		__CalculateProjection();
	}

	void Camera::SetFovDegrees(float value) {
		SetFovRadians(glm::radians(value));
	}

	bool Camera::GetIsMainCamera() {
		return _isMainCamera;
	}

	void Camera::__CalculateProjection() {
		if (_isOrtho) {
			_projection = glm::ortho(
				-_orthoHeight * _aspectRatio, _orthoHeight * _aspectRatio,
				-_orthoHeight, _orthoHeight,
				_nearPlane, _farPlane);
		} else {
			_projection = glm::perspective(_fovRadians, _aspectRatio, _nearPlane, _farPlane);
		}
		_isDirty = false;
	}

}