#include "Phobos/Gameplay/GameObject.h"


#include "Phobos/Application.h"
#include "Phobos/Logging.h"
#include "Phobos/Gameplay/GameObjectTag.h"
#include "Phobos/Gameplay/HierarchyNode.h"
#include "Phobos/Gameplay/Scene.h"

namespace Phobos::Gameplay
{
	GameObject::GameObject(const entt::handle& handle) : _handle(handle) { }
	GameObject::operator entt::basic_handle<entt::entity>() const { return _handle; }
	GameObject::operator entt::entity() const { return _handle; }

	void GameObject::SetParent(GameObject other) {
		HierarchyNode::AddChild(other, _handle);
	}
	
	GameObject GameObject::GetParent() const {
		LOG_ASSERT(IsValid(), "GameObject is no longer valid!");
		return GameObject(entt::handle(*_handle.registry(), _handle.get<HierarchyNode>().Parent));
	}

	void GameObject::Destroy() {
		Application::Instance().ActiveScene->Destroy(*this);
	}


	void GameObject::IterateChilren(std::function<void(GameObject)> function, bool recursive, bool depthFirst)
	{
		auto& node = _handle.get<HierarchyNode>();
		entt::entity current = node.FirstChild;

		while (current != entt::null) {
			GameObject child = GameObject(entt::handle(*_handle.registry(), current));
			function(child);
			if (recursive && depthFirst) {
				child.IterateChilren(function, recursive, depthFirst);
			}
			HierarchyNode& currentNode = _handle.registry()->get<HierarchyNode>(current);
			entt::entity next = currentNode.Next;
			LOG_ASSERT(next != current, "Next == Current, hierachy has been corrupted!");
 			LOG_ASSERT(next != _handle, "Next == Parent, hierachy has been corrupted!");
			current = next;
		}
		if (recursive && !depthFirst) {
			current = node.FirstChild;
			while (current != entt::null) {
				GameObject child = GameObject(entt::handle(*_handle.registry(), current));
				child.IterateChilren(function, recursive, depthFirst);
				HierarchyNode& currentNode = _handle.registry()->get<HierarchyNode>(current);
				entt::entity next = currentNode.Next;
				LOG_ASSERT(next != current, "Next == Current, hierachy has been corrupted!");
				LOG_ASSERT(next != _handle, "Next == Parent, hierachy has been corrupted!");
				current = next;
			}
		}
	}

	Transform& GameObject::GetTransform() const {
		LOG_ASSERT(IsValid(), "GameObject is no longer valid!");
		return _handle.get<Transform>();
	}

	void GameObject::SetName(const std::string& name) {
		LOG_ASSERT(IsValid(), "GameObject is no longer valid!");
		GameObjectTag& tag = _handle.get<GameObjectTag>();
		tag.Name = name;
		tag.HashedName = entt::basic_hashed_string<char>(name.c_str()).value();
	}

	const std::string& GameObject::GetName() const {
		LOG_ASSERT(IsValid(), "GameObject is no longer valid!");
		return _handle.get<GameObjectTag>().Name;
	}

	entt::registry& GameObject::Registry() {
		return *_handle.registry();
	}

	const entt::registry& GameObject::Registry() const {
		return *_handle.registry();
	}

	bool GameObject::IsValid() const {
		return (bool)_handle;
	}
}
