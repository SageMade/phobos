#include "Phobos/Gameplay/GameObjectTag.h"

namespace Phobos::Gameplay
{
	using namespace entt::literals;
	
	GameObjectTag::GameObjectTag() noexcept:
		Name(""),
		HashedName(""_hs),
		GUID(Phobos::Guid::New())
	{
	}

	GameObjectTag::GameObjectTag(const std::string& name)
	{
		Name = name;
		HashedName = entt::hashed_string::value(name.c_str());
		GUID = Phobos::Guid::New();
	}

	GameObjectTag::GameObjectTag(const GameObjectTag& other): GameObjectTag()
	{
		__Copy(other);
	}

	GameObjectTag::GameObjectTag(GameObjectTag&& other): GameObjectTag()
	{
		__Move(std::move(other));
	}

	GameObjectTag& GameObjectTag::operator=(GameObjectTag&& other)
	{
		__Move(std::move(other));
		return *this;
	}

	GameObjectTag& GameObjectTag::operator=(const GameObjectTag& other)
	{
		__Copy(other);
		return *this;
	}

	GameObjectTag::~GameObjectTag() = default;

	void GameObjectTag::__Copy(const GameObjectTag& other)
	{
		Name = other.Name;
		HashedName = other.HashedName;
		GUID = Phobos::Guid::New();
	}

	void GameObjectTag::__Move(GameObjectTag&& other)
	{
		Name = other.Name;
		HashedName = other.HashedName;
		GUID = other.GUID;
	}
}
