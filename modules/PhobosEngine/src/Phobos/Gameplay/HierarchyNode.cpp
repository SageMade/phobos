#include "Phobos/Gameplay/HierarchyNode.h"

#include "Phobos/Logging.h"
#include "Phobos/Gameplay/GameObjectTag.h"

namespace Phobos::Gameplay {
	void HierarchyNode::AddChild(entt::handle parent, entt::handle child) {
		LOG_ASSERT(child, "Child node is not a valid handle!")
		entt::registry& reg = *child.registry();
		HierarchyNode& node = child.get<HierarchyNode>();
		if (node.ExistsInHierachy && node.Parent == parent) {
			return;
		} else if (node.ExistsInHierachy) {
			__CutNode(reg, child);
		}

		HierarchyNode& parentNode = parent ? reg.get<HierarchyNode>(parent) : reg.ctx_or_set<HierarchyNode>();
		if (parentNode.FirstChild != entt::null) {
			__InsertBefore(reg, child, parentNode.FirstChild);
		} else {
			// Patch our node to be before the sibling and share it's parent
			reg.patch<HierarchyNode>(child, [&](HierarchyNode& node) {
				node.Previous = entt::null;
				node.Next = entt::null;
				node.Parent = parent;
				node.ExistsInHierachy = true;
			});
			__PatchNodeOrRoot(reg, parent, [&](HierarchyNode& parentNodeInstance) {
				parentNodeInstance.FirstChild = child;
			});
		}
	}

	void HierarchyNode::SetChildIndex(entt::handle child, size_t index)
	{
		LOG_ASSERT(child, "Child node is invalid!");
		entt::registry& reg = *child.registry();
		HierarchyNode& currentNode = child.get<HierarchyNode>();

		if (index == 0) {
			AddChild(entt::handle(reg, currentNode.Parent), child);
		} else {
			size_t ix = 0;
			HierarchyNode& parent = reg.get<HierarchyNode>(currentNode.Parent);
			HierarchyNode& current = reg.get<HierarchyNode>(parent.FirstChild);
			entt::entity currentEntity = parent.FirstChild;
			while (ix < index - 1 && current.Next != entt::null) {
				currentEntity = current.Next;
				current = reg.get<HierarchyNode>(current.Next);
				ix++;
			}
			__InsertBefore(reg, child, currentEntity);
		}
	}

	void HierarchyNode::MoveChildUp(entt::handle child)
	{
		LOG_ASSERT(child, "Child node is invalid!");
		entt::registry& reg = *child.registry();
		HierarchyNode& currentNode = child.get<HierarchyNode>();
		if (currentNode.Previous != entt::null) {
			HierarchyNode& swapNode = reg.get<HierarchyNode>(currentNode.Previous);
			entt::entity swap = currentNode.Previous;
			entt::entity prev = swapNode.Previous;
			entt::entity next = currentNode.Next;
			if (swapNode.Previous != entt::null) {
				reg.patch<HierarchyNode>(swapNode.Previous, [&](HierarchyNode& node) {
					node.Next = child;
				});
			}
			if (currentNode.Next != entt::null) {
				reg.patch<HierarchyNode>(currentNode.Next, [&](HierarchyNode& node) {
					node.Previous = swap;
				});
			}
			reg.patch<HierarchyNode>(child, [&](HierarchyNode& node) {
				node.Previous = prev;
				node.Next = swap;
			});
			reg.patch<HierarchyNode>(swap, [&](HierarchyNode& node) {
				node.Previous = child;
				node.Next = next;
			});

			HierarchyNode& parentNode = currentNode.Parent != entt::null ? reg.get<HierarchyNode>(currentNode.Parent) : reg.ctx_or_set<HierarchyNode>();
			if (parentNode.FirstChild == swap) {
				__PatchNodeOrRoot(reg, currentNode.Parent, [&](HierarchyNode& node) {
					node.FirstChild = child;
				});
			}
		}
	}

	void HierarchyNode::MoveChildDown(entt::handle child)
	{
		LOG_ASSERT(child, "Child node is invalid!");
		entt::registry& reg = *child.registry();
		HierarchyNode& currentNode = child.get<HierarchyNode>();
		if (currentNode.Next != entt::null) {
			HierarchyNode& swapNode = reg.get<HierarchyNode>(currentNode.Next);
			entt::entity swap = currentNode.Next;
			entt::entity prev = currentNode.Previous;
			entt::entity next = swapNode.Next;
			if (currentNode.Previous != entt::null) {
				reg.patch<HierarchyNode>(currentNode.Previous, [&](HierarchyNode& node) {
					node.Next = swap;
				});
			}
			if (swapNode.Next != entt::null) {
				reg.patch<HierarchyNode>(swapNode.Next, [&](HierarchyNode& node) {
					node.Previous = child;
				});
			}
			reg.patch<HierarchyNode>(child, [&](HierarchyNode& node) {
				node.Previous = swap;
				node.Next = next;
			});
			reg.patch<HierarchyNode>(swap, [&](HierarchyNode& node) {
				node.Previous = prev;
				node.Next = child;
			});
			
			HierarchyNode& parentNode = currentNode.Parent != entt::null ? reg.get<HierarchyNode>(currentNode.Parent) : reg.ctx_or_set<HierarchyNode>();
			if (parentNode.FirstChild == child) {
				__PatchNodeOrRoot(reg, currentNode.Parent, [&](HierarchyNode& node) {
					node.FirstChild = swap;
				});
			}

		}
	}

	void HierarchyNode::RemoveChild(entt::handle child) {
		if (child.valid()) {
			__CutNode(*child.registry(), child);
		}
	}
	
	void HierarchyNode::__CutNode(entt::registry& reg, entt::entity entity)
	{
		HierarchyNode& node = reg.get<HierarchyNode>(entity);
		entt::entity next = node.Next;
		entt::entity prev = node.Previous;
		entt::entity parent = node.Parent;
		
		// Patch the node to unhook from hierarchy
		node = reg.patch<HierarchyNode>(entity, [&](HierarchyNode& node) {
			node.Previous = entt::null;
			node.Next = entt::null;
			node.Parent = entt::null;
			node.ExistsInHierachy = false;
		});
		
		// If the previous node is not null, point it to our next node
		if (prev != entt::null) {
			reg.patch<HierarchyNode>(prev, [&next](HierarchyNode& previousNode) {
				previousNode.Next = next;
			});
		}
		// If the next node is not null, point it's previous node to our previous
		if (next != entt::null) {
			reg.patch<HierarchyNode>(next, [&prev](HierarchyNode& nextNode) {
				nextNode.Previous = prev;
			});
		}
		// If this is the first node in the parent, we'll update it's first child pointer to the next element in the sequence
		HierarchyNode& parentNode = parent != entt::null ? reg.get<HierarchyNode>(parent) : reg.ctx_or_set<HierarchyNode>();
		if (parentNode.FirstChild == entity) {
			__PatchNodeOrRoot(reg, parent, [&](HierarchyNode& node) {
				node.FirstChild = next;
			});
		}
		__ValidateNode(reg, parent, parentNode, false);
	}

	void HierarchyNode::__InsertBefore(entt::registry& reg, entt::entity newNode, entt::entity sibling)
	{
		HierarchyNode& node = reg.get<HierarchyNode>(newNode);
		HierarchyNode& nextNode = reg.get<HierarchyNode>(sibling);
		entt::entity   priorNode = nextNode.Previous;
		// If they are already the prior node, we can skip the insert
		if (priorNode == newNode)
			return;

		// Remove node from hierarchy if it's already there
		if (node.ExistsInHierachy) {
			__CutNode(reg, newNode);
		}
				
		entt::entity   parent = nextNode.Parent;
		
		// If the previous node is not null, point it to our next node
		if (priorNode != entt::null) {
			reg.patch<HierarchyNode>(priorNode, [&](HierarchyNode& prevNode) {
				prevNode.Next = newNode;
			});
		}
		// Patch the sibling node so that prev points to the new node
		reg.patch<HierarchyNode>(sibling, [&](HierarchyNode& sibling) {
			sibling.Previous = newNode;
		});
		// Patch our node to be before the sibling and share it's parent
		reg.patch<HierarchyNode>(newNode, [&](HierarchyNode& node) {
			node.Previous = priorNode;
			node.Next = sibling;
			node.Parent = parent;
			node.ExistsInHierachy = true;
		});
		// If our sibling was the first node in the parent, we'll update it's first child pointer to the next element in the sequence
		HierarchyNode& parentNode = parent != entt::null ? reg.get<HierarchyNode>(parent) : reg.ctx_or_set<HierarchyNode>();
		if (parentNode.FirstChild == sibling) {
			__PatchNodeOrRoot(reg, parent, [&](HierarchyNode& node) {
				node.FirstChild = newNode;
			});
		}
		__ValidateNode(reg, entt::null, reg.ctx_or_set<HierarchyNode>(), true);
	}

	void HierarchyNode::__InsertAfter(entt::registry& reg, entt::entity newNode, entt::entity sibling)
	{
		HierarchyNode& siblingNode = reg.get<HierarchyNode>(sibling);
		entt::entity nextNode = siblingNode.Next;
		entt::entity parent = siblingNode.Parent;
		// If they are already the prior node, we can skip the insert
		if (nextNode == newNode)
			return;

		HierarchyNode& node = reg.get<HierarchyNode>(newNode);
		// Remove node from hierarchy if it's already there
		if (node.ExistsInHierachy) {
			__CutNode(reg, newNode);
		}

		// If the previous node is not null, point it to our next node
		if (siblingNode.Next != entt::null) {
			reg.patch<HierarchyNode>(siblingNode.Next, [&](HierarchyNode& sibling) {
				sibling.Previous = newNode;
			});
		}
		// Patch the sibling node so that prev points to the new node
		reg.patch<HierarchyNode>(sibling, [&](HierarchyNode& sibling) {
			sibling.Next = newNode;
		});
		// Patch our node to be before the sibling and share it's parent
		reg.patch<HierarchyNode>(newNode, [&](HierarchyNode& node) {
			node.Previous = sibling;
			node.Next = nextNode;
			node.Parent = siblingNode.Parent;
			node.ExistsInHierachy = true;
		});
		HierarchyNode& parentNode = parent != entt::null ? reg.get<HierarchyNode>(parent) : reg.ctx_or_set<HierarchyNode>();
		__ValidateNode(reg, parent, parentNode, false);
	}

	void HierarchyNode::__ValidateNode(entt::registry& reg, entt::entity entity, HierarchyNode& node, bool recurse)
	{
		entt::entity current = node.FirstChild;
		GameObjectTag nodeTag;
		if (entity != entt::null) {
			nodeTag = reg.get<GameObjectTag>(entity);
		}

		std::set<entt::entity> visited;
		while (current != entt::null) {
			HierarchyNode& currentNode = reg.get<HierarchyNode>(current);
			GameObjectTag currentNodeTag = reg.get<GameObjectTag>(current);
			LOG_ASSERT(current != entity, "Current == Parent");
			LOG_ASSERT(currentNode.Next != current, "Next == Current");
			LOG_ASSERT(visited.emplace(current).second, "Node has been visited twice!");
			if (recurse) {
				__ValidateNode(reg, current, currentNode, recurse);
			}
			current = currentNode.Next;
		}
	}
}
