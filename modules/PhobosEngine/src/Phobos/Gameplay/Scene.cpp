#include "Phobos/Gameplay/Scene.h"

#include <fstream>


#include "Phobos/Gameplay/Transform.h"
#include "Phobos/Gameplay/GameObjectTag.h"
#include "Phobos/Logging.h"
#include "Phobos/Gameplay/HierarchyNode.h"
#include "Phobos/Physics/PhysicsSystem.h"
#include "Phobos/Physics/Collider.h"
#include "Phobos/Gameplay/IBehaviour.h"

namespace Phobos::Gameplay {
	
	GameScene::GameScene(const std::string& name) {
		Name = name;
		_physics = Physics::PhysicsSystem::Get().CreateWorld();
		_isHierarchyDirty = true;
		_registry.on_update<HierarchyNode>().connect<&GameScene::__OnHierarchyUpdated>(this);
		_registry.on_construct<HierarchyNode>().connect<&GameScene::__OnHierarchyUpdated>(this);
		_registry.on_destroy<HierarchyNode>().connect<&GameScene::__OnHierarchyNodeDestroyed>(this);
		_registry.on_destroy<HierarchyNode>().connect<&GameScene::__OnHierarchyUpdated>(this);
		_registry.on_construct<Physics::Collider>().connect<&GameScene::__OnColliderAdded>(this);
		_registry.on_construct<TransformDirtyFlag>().connect<&GameScene::__OnTransformMarkedDirty>(this);

		RegisterComponentType<Transform>();
		RegisterComponentType<HierarchyNode>();
		RegisterComponentType<GameObjectTag>();
	}

	GameObject GameScene::CreateEntity(const std::string& name) {
		entt::entity entity = _registry.create();
		GameObject result = entt::handle(_registry, entity);
		// pass the handle to the transform constructor
		_registry.emplace<Transform>(entity, result);
		_registry.emplace<GameObjectTag>(entity, name);
		_registry.emplace<HierarchyNode>(entity);
		result.SetParent(entt::handle(_registry, entt::null));
		return result;
	}

	GameObject GameScene::CreateEntity(const std::string& name, const GameObject parent)
	{
		entt::entity entity = _registry.create();
		GameObject result = entt::handle(_registry, entity);
		// pass the handle to the transform constructor
		_registry.emplace<Transform>(entity, result);
		_registry.emplace<GameObjectTag>(entity, name);
		_registry.emplace<HierarchyNode>(entity);
		result.SetParent(parent);	
		return result;
	}

	GameObject GameScene::CreateEntity(const GameObject prefab, const std::string& name) {
		LOG_ASSERT(_prefabRegistry.valid(prefab), "Entity is not a valid prefab! You may need to call CreatePrefab(entity_id) first!");

		const entt::entity instance = StampEntity(_prefabRegistry, prefab, _registry);
		return entt::handle(_registry, instance);
	}

	void GameScene::Destroy(GameObject object) {
		object.ApplyTag<DeleteObjectTag>();
	}

	GameObject GameScene::FindFirst(const std::string& name) {
		entt::entity result = entt::null;
		uint32_t hash = entt::hashed_string::value(name.c_str());
		const auto view = _registry.view<GameObjectTag>();

		for (const entt::entity& entity : view) {
			const GameObjectTag& tag = _registry.get<GameObjectTag>(entity);
			if (tag.HashedName == hash && tag.Name == name) {
				return entt::handle(_registry, entity);
			}
		}
		return entt::handle(_registry, entt::null);
	}

	void GameScene::Poll() {
		_registry.sort<DeleteObjectTag>(std::bind(&GameScene::__SortOnHierarchy, this, std::placeholders::_1, std::placeholders::_2));
		_registry.view<DeleteObjectTag>().each([this](entt::entity entity) {
			_registry.destroy(entity);
		});
		UpdateTransforms();
	}

	entt::handle GameScene::StampEntity(const entt::registry& from, entt::entity src, entt::registry& to) {
		entt::entity dst = to.create();
		from.visit(src, [&from, &to, src, dst](const entt::type_info type_id) {
			_typeFuncs[type_id.seq()].Stamp(from, src, to, dst);
		});
		return entt::handle(to, dst);
	}

	bool GameScene::HasComponent(const entt::registry& reg, entt::entity entity, entt::id_type meta_id) {
		LOG_ASSERT(_typeFuncs[meta_id].Has, "No function bound for type");
		return _typeFuncs[meta_id].Has(reg, entity);
	}

	entt::meta_handle GameScene::GetComponent(const entt::registry& reg, entt::entity entity, entt::id_type meta_id) {
		LOG_ASSERT(_typeFuncs[meta_id].Get, "No function bound for type");
		return _typeFuncs[meta_id].Get(reg, entity);
	}

	void GameScene::NotifyHierarchyDirty() {
		_isHierarchyDirty = true;
	}

	void GameScene::UpdateTransforms() {
		if (_registry.try_ctx<HierarchyDirtyFlag>()) {
			_registry.sort<HierarchyNode>(std::bind(&GameScene::__SortOnHierarchy, this, std::placeholders::_1, std::placeholders::_2));
			_registry.sort<BehaviourBinding>(std::bind(&GameScene::__SortOnHierarchy, this, std::placeholders::_1, std::placeholders::_2));
			_registry.unset<HierarchyDirtyFlag>();
		}

		_registry.sort<TransformDirtyFlag>(std::bind(&GameScene::__SortOnHierarchy, this, std::placeholders::_1, std::placeholders::_2));
		_registry.view<TransformDirtyFlag>().each([this](entt::entity entity) {
			Transform& transform = _registry.get<Transform>(entity);
			transform.UpdateWorldMatrix();
			transform._position = transform._localTransform[3];
			transform._rotation = glm::quat_cast(transform._localTransform);
			transform._rotationEulerDeg = glm::degrees(glm::eulerAngles(transform._rotation));
		});
		_registry.clear<TransformDirtyFlag>();
	}

	bool GameScene::Save(const std::string& path) const {
		std::ofstream file(path);
		if (file) {
			SceneOutputArchiveType ar(file);
			const HierarchyNode& root_node = _registry.ctx<HierarchyNode>();
			ar(CEREAL_NVP(root_node));
			ar(cereal::make_nvp("MainCamera", MainCamera));
			const SceneSerializer& snapshot = entt::snapshot(_registry).entities(ar);
			for (const auto& kvp : _typeFuncs) {
				kvp.second.Serialize(snapshot, ar);
			}
			return true;
		} else {
			LOG_WARN("Failed to save scene, could not create file");
			return false;
		}
	}

	bool GameScene::Load(const std::string& path) {
		std::ifstream file(path);
		if (file) {
			SceneInputArchiveType ar(file);
			_registry = entt::registry();
			_deletionQueue = std::set<GameObject>();
			_registry.unset<HierarchyNode>();			
			HierarchyNode& root_node = _registry.ctx_or_set<HierarchyNode>();
			ar(CEREAL_NVP(root_node));
			ar(cereal::make_nvp("MainCamera", MainCamera));
			const auto& snapshot = entt::snapshot_loader(_registry).entities(ar);
			for (const auto& kvp : _typeFuncs) {
				kvp.second.Deserialize(snapshot, ar);
			}
			_registry.ctx_or_set<HierarchyDirtyFlag>();
			_registry.view<Transform>().each([this](entt::entity e, Transform& t) {
				_registry.emplace_or_replace<TransformDirtyFlag>(e);
			});
			_registry.view<Physics::Collider>().each([this](entt::entity e, Physics::Collider& t) {
				_registry.emplace_or_replace<Physics::ColliderShapeDirtyTag>(e);
				t._handle = entt::handle(_registry, e);
			});
			UpdateTransforms();
			return true;
		} else {
			LOG_WARN("Failed to load scene, could not find file");
			return false;
		}
	}

	void GameScene::__OnHierarchyUpdated(entt::registry& reg, entt::entity entity) {
		_registry.ctx_or_set<HierarchyDirtyFlag>();
	}

	void GameScene::__OnHierarchyNodeDestroyed(entt::registry& reg, entt::entity entity) {
		HierarchyNode::RemoveChild(entt::handle(reg, entity));
	}

	void GameScene::__OnTransformMarkedDirty(entt::registry& reg, entt::entity entity)
	{
		GameObject obj = entt::handle(reg, entity);
		// Note we don't need to recurse, the hooks should do that!
		obj.IterateChilren([](GameObject child) { child.ApplyTag<TransformDirtyFlag>(); });
	}

	void GameScene::__OnColliderAdded(entt::registry& reg, entt::entity e) {
		reg.get<Physics::Collider>(e)._handle = entt::handle(reg, e);
		reg.emplace_or_replace<Physics::ColliderShapeDirtyTag>(e);
	}

	bool GameScene::__SortOnHierarchy(const entt::entity lhs, const entt::entity rhs) {
		const auto& l = _registry.get<HierarchyNode>(lhs);
		const auto& r = _registry.get<HierarchyNode>(rhs);
		return
			r.Parent == lhs || l.Next == rhs ||
			(!(l.Parent == rhs || r.Next == lhs) &&
			 (l.Parent < r.Parent || (l.Parent == r.Parent && &l < &r)));
	}

	bool GameScene::__SortOnHierarchyInverted(const entt::entity lhs, const entt::entity rhs) {
		return !__SortOnHierarchy(lhs, rhs);
	}
}
