#include "Phobos/Application.h"
#include "Phobos/Gameplay/Scene.h"
#include "imgui.h"

namespace Phobos {
	bool Application::HasFocus() const {
		return glfwGetWindowAttrib(Window, GLFW_FOCUSED) && (!ImGui::IsAnyWindowFocused() | GameHasFocus);
	}

	entt::registry& Application::ActiveRegistry() {
		return ActiveScene->Registry();
	}
}
