#include "Phobos/Physics/Collider.h"


#include "Phobos/Application.h"
#include "Phobos/Gameplay/Scene.h"
#include "Phobos/Logging.h"
#include "Phobos/Physics/PhysicsSystem.h"
#include "Phobos/Utils/StringUtils.h"


namespace Phobos::Physics {
	using namespace entt::literals;
	auto COLLIDER_META = entt::meta<Phobos::Physics::Collider>()
		.type()
		.ctor()
		.data<&Phobos::Physics::Collider::SetType, &Phobos::Physics::Collider::GetType>("Type"_hs);
	
	Collider& Collider::SetType(ColliderType type) {
		if (type != _type) {
			if (_type == ColliderType::Terrain) {
				if (_shapeDataPtr != nullptr) {
					((TerrainData*)_shapeDataPtr)->Free();
				}
			}
			if (ColliderTypeHasCustomData(_type) && _shapeDataPtr != nullptr) {
				free(_shapeDataPtr);
				_shapeDataPtr = nullptr;
				_shapeDataPtrSize = 0;
			}
			_type = type;
			_handle.emplace_or_replace<ColliderShapeDirtyTag>();
		}
		return *this;
	}

	Collider& Collider::SetOffset(const glm::vec3& value) {
		_offset = value;
		_handle.emplace_or_replace<ColliderShapeDirtyTag>();
		return *this;
	}

	Collider& Collider::SetColliderData(float value) {
		_shapeData.x = value;
		_handle.emplace_or_replace<ColliderShapeDirtyTag>();
		return *this;
	}

	Collider& Collider::SetColliderData(float valueA, float valueB) {
		_shapeData.x = valueA;
		_shapeData.y = valueB;
		_handle.emplace_or_replace<ColliderShapeDirtyTag>();
		return *this;
	}

	Collider& Collider::SetColliderData(const glm::vec2& value) {
		_shapeData.x = value.x;
		_shapeData.y = value.y;
		_handle.emplace_or_replace<ColliderShapeDirtyTag>();
		return *this;
	}

	Collider& Collider::SetColliderData(float valueA, float valueB, float valueC) {
		_shapeData.x = valueA;
		_shapeData.y = valueB;
		_shapeData.z = valueC;
		_handle.emplace_or_replace<ColliderShapeDirtyTag>();
		return *this;
	}

	Collider& Collider::SetColliderData(const glm::vec3& value) {
		_shapeData.x = value.x;
		_shapeData.y = value.y;
		_shapeData.z = value.z;
		_handle.emplace_or_replace<ColliderShapeDirtyTag>();
		return *this;
	}

	Collider& Collider::SetColliderData(float valueA, float valueB, float valueC, float valueD) {
		_shapeData.x = valueA;
		_shapeData.y = valueB;
		_shapeData.z = valueC;
		_shapeData.w = valueD;
		_handle.emplace_or_replace<ColliderShapeDirtyTag>();
		return *this;
	}

	Collider& Collider::SetColliderData(const glm::vec4& value) {
		_shapeData.x = value.x;
		_shapeData.y = value.y;
		_shapeData.z = value.z;
		_shapeData.w = value.w;
		_handle.emplace_or_replace<ColliderShapeDirtyTag>();
		return *this;
	}
}
