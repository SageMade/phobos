#include "Phobos/Physics/RigidBody.h"
#include "Phobos/Physics/PhysicsWorld.h"
#include "Phobos/Physics/PhysicsSystem.h"
#include <ENTT/entt.hpp>

#include "Phobos/Utils/StringUtils.h"


namespace Phobos::Physics {
	using namespace entt::literals;
	auto RIGIDBODY_META = entt::meta<Phobos::Physics::RigidBody>()
		.type(entt::hashed_string(Phobos::Utils::StringTools::SanitizeClassName(typeid(Phobos::Physics::RigidBody).name()).c_str()).value())
		.ctor()
		.data<&Phobos::Physics::RigidBody::SetAlwaysActive, &Phobos::Physics::RigidBody::GetAlwaysActive>("AlwaysActive"_hs).prop("_name"_hs, "Always Active")
		.data<&Phobos::Physics::RigidBody::SetIsKinematic, &Phobos::Physics::RigidBody::GetIsKinematic>("IsKinematic"_hs).prop("_name"_hs, "Is Kinematic")
		.data<&Phobos::Physics::RigidBody::SetIsDebugDrawEnabled, &Phobos::Physics::RigidBody::GetIsDebugDrawEnabled>("IsDebugDrawEnabled"_hs).prop("_name"_hs, "Is Debug Drawn")
		.data<&Phobos::Physics::RigidBody::SetLinearDamping, &Phobos::Physics::RigidBody::GetLinearDamping>("LinearDamping"_hs).prop("_name"_hs, "Linear Damping").prop("_min"_hs, 0.0f)
		.data<&Phobos::Physics::RigidBody::SetAngularDamping, &Phobos::Physics::RigidBody::GetAngularDamping>("AngularDamping"_hs).prop("_name"_hs, "Angular Damping").prop("_min"_hs, 0.0f);
	
	RigidBody& RigidBody::SetLinearDamping(float value) {
		_damping = value;
		if (_body != nullptr) {
			_body->setDamping(_damping, _angularDamping);
		}
		return *this;
	}

	RigidBody& RigidBody::SetAngularDamping(float value) {
		_angularDamping = value;
		if (_body != nullptr) {
			_body->setDamping(_damping, _angularDamping);
		}
		return *this;
	}

	RigidBody& RigidBody::SetAlwaysActive(bool value) {
		if (_body != nullptr) {
			_body->setActivationState(value ? DISABLE_DEACTIVATION : 1);
		}
		_alwaysActive = value;
		return *this;
	}

	RigidBody& RigidBody::SetIsKinematic(bool value) {
		if (_body != nullptr) {
			_body->setCollisionFlags(value ?
									 (_body->getCollisionFlags() | btCollisionObject::CF_KINEMATIC_OBJECT) :
									 (_body->getCollisionFlags() & ~btCollisionObject::CF_KINEMATIC_OBJECT));
		}
		_isKinematic = value;
		return *this;
	}

	RigidBody& RigidBody::SetIsDebugDrawEnabled(bool value) {
		if (_body != nullptr) {
			_body->setCollisionFlags(!value ?
									 (_body->getCollisionFlags() | btCollisionObject::CF_DISABLE_VISUALIZE_OBJECT) :
									 (_body->getCollisionFlags() & ~btCollisionObject::CF_DISABLE_VISUALIZE_OBJECT)
			);
		}
		_isDebugDrawEnabled = value;
		return *this;
	}

	RigidBody& RigidBody::SetPhysicsWorld(const PhysicsWorldSptr& ptr)
	{
		if (_body != nullptr) {
			PhysicsSystem::Get().DestroyRigidBody(*this);
		}
		_physicsWorld = ptr;
		return *this;
	}
}
