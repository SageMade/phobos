#include "Phobos/Physics/PhysicsWorld.h"

#include "Phobos/Gameplay/Transform.h"
#include "Phobos/Physics/PhysicsSystem.h"

//#include <algorithm>

namespace Phobos::Physics {
	PhysicsWorld::PhysicsWorld() :
		_bulletPhsyicsWorld(nullptr),
		_transformRoot(glm::mat4(1.0f)),
		_inverseRootTransform(glm::mat4(1.0f)),
		_hasNonIdentityRoot(false)
	{

	}

	PhysicsWorld::~PhysicsWorld() {
		auto& worlds = PhysicsSystem::Get()._worlds;
		worlds.erase(std::remove(worlds.begin(), worlds.end(), this), worlds.end());
		delete _bulletPhsyicsWorld;
	}

	void PhysicsWorld::SetRootTransform(const glm::mat4& value) {
		static glm::mat4 IDENTITY = glm::mat4(1.0f);
		_transformRoot = value;
		_hasNonIdentityRoot = _transformRoot != IDENTITY;
		_inverseRootTransform = glm::inverse(_transformRoot);
	}

	const glm::mat4& PhysicsWorld::GetRootTransform() const {
		return _transformRoot;
	}
}