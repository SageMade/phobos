#include "Phobos/Physics/BulletDebugDrawer.h"
#include "Phobos/Physics/BulletGLMInterface.h"
#include "Phobos/Logging.h"
#include "Phobos/Graphics/DebugDrawer.h"

namespace Phobos::Physics {
	BulletDebugDraw::BulletDebugDraw() {
	}

	void BulletDebugDraw::drawLine(const btVector3& from, const btVector3& to, const btVector3& color) {
		Graphics::DebugDrawer::Get().DrawLine(ToGLM(from), ToGLM(to), ToGLM(color));
	}

	void BulletDebugDraw::drawLine(const btVector3& from, const btVector3& to, const btVector3& fromColor, const btVector3& toColor)
	{
		Graphics::DebugDrawer::Get().DrawLine(ToGLM(from), ToGLM(to), ToGLM(fromColor), ToGLM(toColor));
	}

	void BulletDebugDraw::drawContactPoint(const btVector3& PointOnB, const btVector3& normalOnB, btScalar distance,
										   int lifeTime, const btVector3& color) {

	}

	void BulletDebugDraw::reportErrorWarning(const char* warningString) {
		LOG_WARN(warningString);
	}

	void BulletDebugDraw::draw3dText(const btVector3& location, const char* textString) {

	}

	void BulletDebugDraw::setDebugMode(int debugMode)
	{
		m_debugMode = debugMode;
	}
}	