#include "Phobos/Physics/PhysicsSystem.h"

#include <GLM/gtc/matrix_transform.hpp>

#include "Phobos/Physics/PhysicsWorld.h"

#include <GLM/gtc/type_ptr.hpp>
#include "Phobos/Physics/Collider.h"
#include "Phobos/Physics/RigidBody.h"
#include "Phobos/Gameplay/Scene.h"
#include "Phobos/Application.h"
#include "Phobos/Gameplay/Transform.h"
#include "Phobos/Gameplay/Timing.h"
#include "Phobos/Physics/BulletDebugDrawer.h"
#include "Phobos/Physics/BulletDebugDrawer.h"
#include "Phobos/Physics/PhysicsWorldContainer.h"
#include "Phobos/Gameplay/HierarchyNode.h"
#include "Phobos/Physics/BulletGLMInterface.h"

#include <bullet/BulletCollision/CollisionShapes/btHeightfieldTerrainShape.h>
#define GLM_ENABLE_EXPERIMENTAL
#include <memory>
#include <GLM/gtx/matrix_decompose.hpp>

namespace Phobos::Physics {
	using namespace Gameplay;
	PhysicsSystem::PhysicsSystem() :
		_debugDraw(nullptr),
		_worlds(std::vector<PhysicsWorld*>())
	{
		_debugDraw = new Physics::BulletDebugDraw();
		_debugDraw->setDebugMode(btIDebugDraw::DBG_DrawWireframe);
	}

	std::shared_ptr<btCollisionShape> PhysicsSystem::CreateCollisionShape(Collider& collider) {
		switch (collider._type)
		{
			case ColliderType::Box:
				return std::make_shared<btBoxShape>(FromGLM((glm::vec3)collider._shapeData));
			case ColliderType::Sphere:
				return std::make_shared<btSphereShape>(collider._shapeData.x);
			case ColliderType::Capsule:
				return std::make_shared<btCapsuleShapeZ>(collider._shapeData.x, collider._shapeData.y);
			case ColliderType::CapsuleX:
				return std::make_shared<btCapsuleShapeX>(collider._shapeData.x, collider._shapeData.y);
			case ColliderType::CapsuleY:
				return std::make_shared<btCapsuleShape>(collider._shapeData.x, collider._shapeData.y);
			case ColliderType::Cone:
				return std::make_shared<btConeShapeZ>(collider._shapeData.x, collider._shapeData.y);
			case ColliderType::ConeX:
				return std::make_shared<btConeShapeX>(collider._shapeData.x, collider._shapeData.y);
			case ColliderType::ConeY:
				return std::make_shared<btConeShape>(collider._shapeData.x, collider._shapeData.y);
			case ColliderType::Cylinder:
				return std::make_shared<btCylinderShapeZ>(FromGLM((glm::vec3)collider._shapeData));
			case ColliderType::CylinderX:
				return std::make_shared<btCylinderShapeX>(FromGLM((glm::vec3)collider._shapeData));
			case ColliderType::CylinderY:
				return std::make_shared<btCylinderShape>(FromGLM((glm::vec3)collider._shapeData));
			case ColliderType::Plane:
				return std::make_shared<btStaticPlaneShape>(FromGLM((glm::vec3)collider._shapeData), collider._shapeData.w);
			case ColliderType::Terrain: {
				const Collider::TerrainData* data = collider.GetColliderDataPtr<Collider::TerrainData>();
				if (data)
					return std::make_shared<btHeightfieldTerrainShape>(
						data->Width, data->Height, 
						data->HeightMap, 
						data->HeightScale, 
						data->MinHeight, data->MaxHeight, 
						(int)data->Axis, 
						(PHY_ScalarType)data->DataType, 
						data->FlipQuadEdges
					);
				else
					return nullptr;
			}
			case ColliderType::Unknown:
			default:
				return nullptr;
		}
	}

	void PhysicsSystem::Update()
	{
		const GameScene::sptr& scene = Application::Instance().ActiveScene;
		entt::registry& registry = scene->Registry();

		auto worldView = registry.view<Transform, PhysicsWorldContainer>();
		worldView.each([&](entt::entity e, Transform& transform, PhysicsWorldContainer& worldContainer) {
			if (worldContainer.World != nullptr) {
				worldContainer.World->SetRootTransform(transform.WorldTransform());
			}
		});


		auto view = registry.view<RigidBody, Transform>();
		view.each([&](entt::entity e, RigidBody& body, Transform& t) {
			btTransform transform;
			// We can only operate on bodies with shapes
			if (body._body == nullptr)
			{
				std::shared_ptr<btCompoundShape> cShape = std::make_shared<btCompoundShape>(true, 1);
				body._compoundShape = cShape; // to keep the shape alive
				
				cShape->setLocalScaling(FromGLM(t.GetLocalScale()));
				btVector3 localInertia = btVector3(0.0f, 0.0f, 0.0f);
				if (body._mass > 0.0f)
					cShape->calculateLocalInertia(body._mass, localInertia);

				glm::mat4 glTransform = t.WorldTransform();
				if (body._physicsWorld != nullptr && body._physicsWorld->HasNonIdentityRoot()) {
					glTransform = body._physicsWorld->_inverseRootTransform * glTransform;
				}
				glm::mat4 scaleStripped = glm::translate(glm::mat4(1.0f), (glm::vec3)glTransform[3]) * glm::mat4_cast(glm::quat_cast(glTransform));
				transform.setFromOpenGLMatrix(glm::value_ptr(scaleStripped));
				body._motionState = new btDefaultMotionState(transform);

				btRigidBody::btRigidBodyConstructionInfo info(body._mass, body._motionState, cShape.get(), localInertia);
				info.m_angularDamping = body._angularDamping;
				info.m_linearDamping = body._damping;
				body._body = new btRigidBody(info);

				if (body._physicsWorld == nullptr) {
					scene->_physics->GetWorld()->addRigidBody(body._body, body._group, body._mask);
				} else {
					body._physicsWorld->_bulletPhsyicsWorld->addRigidBody(body._body, body._group, body._mask);
				}
				body._massDirty = false;
				body._isGroupMaskDirty = false;
				body._originState = transform;

				if (body._isKinematic) {
					body._body->setCollisionFlags(body._body->getCollisionFlags() | btCollisionObject::CF_KINEMATIC_OBJECT);
				}
				if (!body._isDebugDrawEnabled) {
					body._body->setCollisionFlags(body._body->getCollisionFlags() | btCollisionObject::CF_DISABLE_VISUALIZE_OBJECT);
				}
				if (body._alwaysActive) {
					body._body->setActivationState(DISABLE_DEACTIVATION);
				}

				// If the mass has changed, we need to do some recalculating
				if (body._massDirty) {
					btVector3 localInertia = btVector3(0.0f, 0.0f, 0.0f);
					if (body._mass > 0.0f)
						body._compoundShape->calculateLocalInertia(body._mass, localInertia);
					body._body->setMassProps(body._mass, localInertia);
					if (body._mass == 0.0f) {
						body._body->setGravity(btVector3(0, 0, 0));
						body._body->setCollisionFlags(body._body->getCollisionFlags() | btCollisionObject::CF_STATIC_OBJECT);
					} else {
						body._body->setCollisionFlags(body._body->getCollisionFlags() & ~btCollisionObject::CF_STATIC_OBJECT);
					}
					body._massDirty = false;
				}
			}

			// If the collision group or mask has changed, update it
			if (body._isGroupMaskDirty) {
				body._body->getBroadphaseProxy()->m_collisionFilterGroup = body._group;
				body._body->getBroadphaseProxy()->m_collisionFilterMask = body._mask;
				body._isGroupMaskDirty = false;
			}
		});

		// Collider updates
		registry.view<ColliderShapeDirtyTag, Transform, Collider>().each([&registry](entt::entity e, Transform& t, Collider& c) {
			entt::handle bodyParent = entt::handle(registry, entt::null);
			PhysicsWorldContainer* physicsWorld = nullptr;
			RigidBody* hostBody = HierarchyNode::TryGetComponentInParent<RigidBody, PhysicsWorldContainer>(entt::handle(registry, e), &bodyParent, physicsWorld, true);
			if (hostBody == nullptr) {
				if (physicsWorld != nullptr) {
					// Add to the world container's static collider
					LOG_WARN("TODO: Implement physics world static GEO");
				} else {
					// Add to the scene-wide static collider
					LOG_WARN("TODO: Implement global world static GEO");
				}
			}
			else {
				c._hostShape = hostBody->_compoundShape.get();
				c._hostRigidBody = bodyParent;
				if (c._shape != nullptr) {
					hostBody->_compoundShape->removeChildShape(c._shape.get());
				}
				glm::mat4 relative;
				if(bodyParent.entity() != e) {
					const Transform& parentTransform = bodyParent.get<Transform>();
					relative = parentTransform._inverseWorldTransform * t.WorldTransform();
				} else {
					relative = t.LocalTransform();
				}

				glm::vec3 scale, translation, skew;
				glm::vec4 perspective;
				glm::quat orientation;
				glm::decompose(relative, scale, orientation, translation, skew, perspective);

				glm::vec3 position = glm::vec4(c.GetOffset(), 1.0f) * relative;

				auto newShape = CreateCollisionShape(c);
				btTransform shapeTransform;
				shapeTransform.setIdentity();
				shapeTransform.setOrigin(FromGLM(position));
				shapeTransform.setRotation(FromGLM(orientation));
				newShape->setLocalScaling(FromGLM(scale));
				c._childIndex = hostBody->_compoundShape->getNumChildShapes();
				hostBody->_compoundShape->addChildShape(shapeTransform, newShape.get());

				c._shape = newShape;

				btDynamicsWorld* world = Application::Instance().ActiveScene->GetPhysics()->GetWorld();
				// Clear the overlapping pair cache to notify that the shape has changed!
				world->getBroadphase()->getOverlappingPairCache()->cleanProxyFromPairs(hostBody->_body->getBroadphaseHandle(), world->getDispatcher());

				// Mark the body as mass dirty to re-calculate inertia
				hostBody->_massDirty = true;
			}
		});
		registry.clear<ColliderShapeDirtyTag>();

		registry.view<Collider, Transform, TransformDirtyFlag>().each([&registry](entt::entity e, Collider& c, Transform& t) {
			if (c._hostShape != nullptr && c._shape != nullptr && c._hostRigidBody && c._hostRigidBody.entity() != e) {
				const Transform& parentTransform = c._hostRigidBody.get<Transform>();
				glm::mat4 relative = parentTransform._inverseWorldTransform * t.WorldTransform();
				
				glm::vec3 scale, translation, skew;
				glm::vec4 perspective;
				glm::quat orientation;
				glm::decompose(relative, scale, orientation, translation, skew, perspective);

				glm::vec3 position = relative * glm::vec4(c.GetOffset(), 1.0f);
				
				btTransform shapeTransform;
				shapeTransform.setIdentity();
				shapeTransform.setOrigin(FromGLM(position));
				shapeTransform.setRotation(FromGLM(orientation));
				c._shape->setLocalScaling(FromGLM(scale));
				
				c._hostShape->updateChildTransform(c._childIndex, shapeTransform, true);
				c._hostRigidBody.get<RigidBody>()._massDirty = true;
			}
		});
		
		btTransform transform;
		registry.view<RigidBody, Transform, TransformDirtyFlag>().each([&transform](entt::entity e, RigidBody& body, Transform& t) {
			glm::mat4 glTransform = t.WorldTransform();
			if (body._physicsWorld != nullptr && body._physicsWorld->HasNonIdentityRoot()) {
				glTransform = body._physicsWorld->_inverseRootTransform * glTransform;
			}
			// We can normalize the 3x3 part for the matrix to remove the scaling from the transform!
			for(int ix = 0; ix < 3; ix++) {
				float w = glTransform[ix][3];
				glm::vec3 n = glm::normalize((glm::vec3)glTransform[ix]);
				glTransform[ix] = glm::vec4(n, w);
			}
			//glm::vec3 scale, translation, skew;
			//glm::quat rotation;
			//glm::vec4 perspective;
			//glm::decompose(glTransform, scale, rotation, translation, skew, perspective);
			//glm::mat4 scaleStripped = glm::translate(glm::mat4(1.0f), translation) * glm::mat4_cast(rotation);
			transform.setFromOpenGLMatrix(glm::value_ptr(glTransform));
			body._body->activate(true);
			body._body->setWorldTransform(transform);
			body._motionState->setWorldTransform(transform);
		});
		
		scene->_physics->GetWorld()->stepSimulation(Timing::Instance().DeltaTime, 10);

		worldView.each([&](entt::entity e, Transform& transform, PhysicsWorldContainer& worldContainer) {
			if (worldContainer.World != nullptr) {
				worldContainer.World->GetWorld()->stepSimulation(Timing::Instance().DeltaTime, 10);
			}
		});

		auto view2 = registry.view<HierarchyNode, RigidBody, Transform>();
		view2.each([&](entt::entity e, HierarchyNode& node, RigidBody& body, Transform& transform) {
			if (body._body->isActive()) {
				glm::vec3 localScale = transform._scale;
				btTransform& t = body._body->getWorldTransform();
				t.getOpenGLMatrix(&transform._worldTransform[0][0]);
				if (body._physicsWorld != nullptr && body._physicsWorld->HasNonIdentityRoot()) {
					transform._worldTransform = body._physicsWorld->GetRootTransform() * transform._worldTransform;
				}
				transform._worldTransform = glm::scale(transform._worldTransform, localScale);
				if (node.Parent != entt::null) {
					glm::mat4 parent_inverse = registry.get<Transform>(node.Parent)._inverseWorldTransform;
					transform._localTransform = parent_inverse * transform._worldTransform;
					//transform._rotation = glm::quat_cast(parent_inverse) * ToGLM(body._body->getOrientation());
				} else {
					transform._localTransform = transform._worldTransform;
				}												
				transform._worldNormalMatrix = glm::mat3(glm::transpose(glm::inverse(transform._worldTransform)));
				transform._isWorldDirty = false;
				registry.emplace_or_replace<TransformDirtyFlag>(e);
			}
		});
	}

	std::shared_ptr<PhysicsWorld> PhysicsSystem::CreateWorld() const {
		PhysicsWorld::sptr result = std::make_shared<PhysicsWorld>();
		result->_collisionConfig = new btDefaultCollisionConfiguration();
		result->_collisionDispatcher = new btCollisionDispatcher(result->_collisionConfig);
		result->_broadphaseInterface = new btDbvtBroadphase();
		result->_constraintSolver = new btSequentialImpulseConstraintSolver();
		result->_bulletPhsyicsWorld = new btDiscreteDynamicsWorld(
			result->_collisionDispatcher, 
			result->_broadphaseInterface, 
			result->_constraintSolver, 
			result->_collisionConfig
		);
		result->_bulletPhsyicsWorld->setGravity(btVector3(0, 0, -9.81f));
		result->_bulletPhsyicsWorld->setDebugDrawer(_debugDraw);
		return result;
	}
	
	void PhysicsSystem::DestroyRigidBody(RigidBody& body) {
		if (body._physicsWorld == nullptr) {
			Application::Instance().ActiveScene->_physics->GetWorld()->removeRigidBody(body._body);
		} else {
			body._physicsWorld->_bulletPhsyicsWorld->removeRigidBody(body._body);
		}
		delete body._body;
		delete body._motionState;
		body._body = nullptr;
		body._compoundShape = nullptr;
		body._motionState = nullptr;
	}
}
