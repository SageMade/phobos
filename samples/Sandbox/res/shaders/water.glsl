#version 410

#define M_PI 3.1415926535897932384626433832795
#define MAX_WAVES 8

uniform vec3  u_CamPos;

// New in tutorial 06
uniform vec3  a_WaterColor;
uniform float a_WaterClarity; // Mixing value for water albedo and reflection / refraction effects
uniform float a_FresnelPower; // How much reflection is applied
uniform float a_RefractionIndex; // Should be source / material refractive index (1 / 1.33 for water) 
uniform float a_WaterAlpha;

uniform samplerCube s_Environment;
uniform mat3 u_EnvironmentRotation;

uniform mat4 u_ModelViewProjection;

uniform float a_Time;
uniform float a_Gravity;

uniform int   a_EnabledWaves;
uniform vec4  a_Waves[MAX_WAVES];

#type VertexShader
// Heavily inspired by:
// https://catlikecoding.com/unity/tutorials/flow/waves/

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec4 inColor;

layout(location = 0) out vec4 outColor;
layout(location = 1) out vec3 outNormal;
layout(location = 2) out vec3 outWorldPos;
layout(location = 4) out float outSteepness;


vec3 GerstnerWave(vec4 waveInfo, vec3 pos, inout float totalSteepness, inout vec3 tangent, inout vec3 binorm) {
	// Our steepness is how 'sharp' the wave is
	float steepness = waveInfo.z;
	// The wave length is the size of the wave in meters
	float waveLength = waveInfo.w;
	   
	// k is the frequency of the wave
	float k = 2 * M_PI / waveLength;
	// c is the speed of the wave, calculated from wave length and gravity (todo: move gravity to a uniform)
	// If we make gravity lower, the waves will move slower
	float c = sqrt(a_Gravity / k);
	// Extract the wave's direction (todo: normalize on the CPU side)
	vec2  dir = normalize(waveInfo.xy);
	// f is how far along in the direction the wave is at the given time
	float f = k * (dot(dir, pos.xy) - c * a_Time);
	// a is used to calculate our curve over our wavelength, and to prevent loops from forming
	float a = steepness / k;
	
	// Calculate our tangent and binormal factors
	tangent += vec3(
		(-dir.x * dir.x * (steepness * sin(f))),
		(-dir.x * dir.y * (steepness * sin(f))),
		( dir.x * (steepness * cos(f)))
	);
	binorm += vec3(
		(-dir.x * dir.y * (steepness * sin(f))),
		(-dir.y * dir.y * (steepness * sin(f))),
		( dir.y * (steepness * cos(f)))
	);

	// Calculate our position as effected by the wave
	return vec3(
		dir.x * (a * cos(f)),
		dir.y * (a * cos(f)),
		a * sin(f)
	);
}

void main() {
	outColor = inColor;

	vec3 pos = inPosition;
	vec3 tangent = vec3( 1,  0, 0);
	vec3 binorm  = vec3( 0,  1, 0);

	float totalSteepness = 0.0;
	vec3 result = pos;
	for (int ix = 0; ix < a_EnabledWaves && ix < MAX_WAVES; ix++) {
		result += GerstnerWave(a_Waves[ix], pos, totalSteepness, tangent, binorm);
	}
	outNormal = normalize(cross(tangent, binorm));

	outWorldPos = result;
	outSteepness = totalSteepness;

	gl_Position = u_ModelViewProjection * vec4(result, 1);
}

#type FragmentShader
layout(location = 0) in vec4 inColor;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec3 inWorldPos;
layout(location = 4) in float inSteepness;

layout(location = 0) out vec4 outColor;

void main() {
	// Re-normalize our input, so that it is always length 1
	vec3 norm = normalize(inNormal);

	// Determine the direction between the camera and the pixel
	vec3 viewDir = normalize(inWorldPos - u_CamPos);
	
	vec3 reflection = normalize(reflect(viewDir, norm));
	vec3 refraction = normalize(refract(viewDir, norm, a_RefractionIndex));

	vec3 reflected = texture(s_Environment, u_EnvironmentRotation * reflection).rgb;
	vec3 refracted = texture(s_Environment, u_EnvironmentRotation * refraction).rgb;

	vec3 fresnel = vec3(dot(-viewDir, norm)) * a_FresnelPower;

	// Combine our refracted and reflected components
	vec3 environmentVal = refracted * (1.0 - fresnel) + reflected * fresnel;
	
	// We mix together our albedo and our water's clarity
	vec3 result = mix(a_WaterColor, environmentVal * a_WaterColor, a_WaterClarity);

	// TODO: gamma correction

	// Write the output
	outColor = vec4(result, inColor.a * a_WaterAlpha);// * a_ColorMultiplier;
}