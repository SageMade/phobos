#version 410

#include "common/AppUniforms.glsl"

#pragma editor Min=0.0, Max=4.0
uniform vec2  u_TextureScaling;

#pragma editor Min=0.0, Max=1.0
uniform vec3  u_HeightCutoffs;
#pragma editor Min=0.0, Max=1.0
uniform vec3  u_InterpolateFactors;

#pragma editor
uniform float u_MinHeight;
#pragma editor
uniform float u_MaxHeight;

#pragma editor
uniform sampler2D s_Sand;
#pragma editor
uniform sampler2D s_Grass;
#pragma editor
uniform sampler2D s_Rock;
#pragma editor
uniform sampler2D s_Snow;

#pragma editor RenderType=Color
uniform vec3  u_AmbientCol;
#pragma editor Min=0.0, Max=1.0
uniform float u_AmbientStrength;

#pragma editor
uniform vec3  u_LightPos;
#pragma editor RenderType=Color
uniform vec3  u_LightCol;
#pragma editor Min=0.0, Max=1.0
uniform float u_AmbientLightStrength;
#pragma editor Min=0.0, Max=1.0
uniform float u_SpecularLightStrength;
#pragma editor Min=0.0, Max=256.0
uniform float u_Shininess;
// NEW in week 7, see https://learnopengl.com/Lighting/Light-casters for a good reference on how this all works, or
// https://developer.valvesoftware.com/wiki/Constant-Linear-Quadratic_Falloff
#pragma editor Min=0.0, Max=1.0
uniform float u_LightAttenuationConstant;
#pragma editor Min=0.0, Max=1.0
uniform float u_LightAttenuationLinear;
#pragma editor Min=0.0, Max=1.0
uniform float u_LightAttenuationQuadratic;

#pragma editor Min=0.0, Max=1.0
uniform float u_TextureMix;

#type VertexShader

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inColor;
layout(location = 2) in vec3 inNormal;
layout(location = 3) in vec2 inUV;

layout(location = 0) out vec3 outPos;
layout(location = 1) out vec3 outColor;
layout(location = 2) out vec3 outNormal;
layout(location = 3) out vec2 outUV;
layout(location = 4) out vec4 outTexWeights;

float GetTextureWeight(float height, float a, float b) {
	float t = min(max((height-a) / (b-a), 0), 1);
	return t * t * (3 - 2 * t);
}

void main() {

	gl_Position = u_ModelViewProjection * vec4(inPosition, 1.0);

	// Lecture 5
	// Pass vertex pos in world space to frag shader
	outPos = (u_Model * vec4(inPosition, 1.0)).xyz;

	// Normals
	outNormal = u_NormalMatrix * inNormal;

    // Do heightmap stuff here

	// Pass our UV coords to the fragment shader
	outUV = inPosition.xy * u_TextureScaling;

    
    float height = (inPosition.z - u_MinHeight) / (u_MaxHeight - u_MinHeight);

    vec3 upperEdge = u_HeightCutoffs + u_InterpolateFactors;
    vec3 lowerEdge = u_HeightCutoffs - u_InterpolateFactors;

	outTexWeights = vec4(
		GetTextureWeight(height, upperEdge.x, lowerEdge.x),
		min(GetTextureWeight(height, lowerEdge.x, upperEdge.x), GetTextureWeight(height, upperEdge.y, lowerEdge.y)),
		min(GetTextureWeight(height, lowerEdge.y, upperEdge.y), GetTextureWeight(height, upperEdge.z, lowerEdge.z)),
		GetTextureWeight(height, lowerEdge.z, upperEdge.z)
	);

	///////////
	outColor = inColor;

}

#type FragmentShader

layout(location = 0) in vec3 inPos;
layout(location = 1) in vec3 inColor;
layout(location = 2) in vec3 inNormal;
layout(location = 3) in vec2 inUV;
layout(location = 4) in vec4 inTexWeights;

out vec4 frag_color;

// https://learnopengl.com/Advanced-Lighting/Advanced-Lighting
void main() {
	// Lecture 5
	vec3 ambient = u_AmbientLightStrength * u_LightCol;

	// Diffuse
	vec3 N = normalize(inNormal);
	vec3 lightDir = normalize(u_LightPos - inPos);

	float dif = max(dot(N, lightDir), 0.0);
	vec3 diffuse = dif * u_LightCol;// add diffuse intensity

	//Attenuation
	float dist = length(u_LightPos - inPos);
	float attenuation = 1.0f / (
		u_LightAttenuationConstant + 
		u_LightAttenuationLinear * dist +
		u_LightAttenuationQuadratic * dist * dist);

	// Specular
	vec3 viewDir  = normalize(u_CamPos - inPos);
	vec3 h        = normalize(lightDir + viewDir);

	// Get the specular power from the specular map
	float spec = pow(max(dot(N, h), 0.0), u_Shininess); // Shininess coefficient (can be a uniform)
	vec3 specular = u_SpecularLightStrength * spec * u_LightCol; // Can also use a specular color

	// Get the albedo from the diffuse / albedo map
	vec4 sand  = texture(s_Sand, inUV);
	vec4 grass = texture(s_Grass, inUV);
	vec4 stone = texture(s_Rock, inUV);
	vec4 snow  = texture(s_Snow, inUV);
    
	vec4 textureColor = 
        sand  * inTexWeights.x +
        grass * inTexWeights.y +
        stone * inTexWeights.z + 
        snow  * inTexWeights.w;

	vec3 result = (
		(u_AmbientCol * u_AmbientStrength) + // global ambient light
		(ambient + diffuse + specular) * attenuation // light factors from our single light
		) * inColor * textureColor.rgb; // Object color

	frag_color = vec4(result, textureColor.a);
}