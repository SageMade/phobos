#version 410 core
#extension GL_ARB_bindless_texture : enable

#include "common/structures.glsl"

const float NumLights = 4;

layout (location = 0) in vec4 inColor;
layout (location = 1) in vec3 inNormal;
layout (location = 2) in vec3 inWorldPos;
layout (location = 3) in vec2 inTexCoords;
layout (location = 4) in mat3 inTBN;

layout (location = 0) out vec4 outColor;

// material parameters
uniform vec3  a_FresnelFactor;
layout(bindless_sampler) uniform sampler2D s_Albedo;
layout(bindless_sampler) uniform sampler2D s_Metallic;
layout(bindless_sampler) uniform sampler2D s_Roughness;
layout(bindless_sampler) uniform sampler2D s_Normal;
layout(bindless_sampler) uniform sampler2D s_AO;
/*
uniform vec3  a_Albedo;
uniform float a_Metallic;
uniform float a_Roughness;
*/
uniform float a_AO;
uniform float a_NormalFactor;

// lights
uniform vec3 a_LightPositions[4];
uniform vec3 a_LightColors[4];

// Scene settings
uniform vec3  a_AmbientFactor;

const float PI = 3.14159265359;

// Approximates the distribution of microfacets that are aligned with the halfway
// vector for a given roughness
float DistributionGGX(vec3 norm, vec3 h, float roughness)
{
    // Our numerator will be our roughness^4
    float num = pow(roughness, 4);
    // Determine the angle between the surface normal and the half vector, square it
    float NdotH = pow(max(dot(norm, h), 0.0), 2);

    float denom = (NdotH * (num - 1.0) + 1.0);
    denom = PI * denom * denom;

    return num / max(denom, 0.001); // prevent divide by zero for roughness=0.0 and NdotH=1.0
}
// Approximates the amount of microfacet occlusion when looking at a surface from a given angle
float GeometrySchlickGGX(float NdotV, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float num   = NdotV;
    float denom = NdotV * (1.0 - k) + k;

    return num / denom;
}
// Takes into account micofacet occlusion from both the light and eye's perspective
float GeometrySmith(vec3 norm, vec3 toEye, vec3 toLight, float roughness)
{
    // Get our angles between the surface normal and our eye/light
    float NdotV = max(dot(norm, toEye), 0.0);
    float NdotL = max(dot(norm, toLight), 0.0);
    // Calculate the Schlick factor for each
    float ggx2 = GeometrySchlickGGX(NdotV, roughness);
    float ggx1 = GeometrySchlickGGX(NdotL, roughness);

    return ggx1 * ggx2;
}
// Calculates the ratio between the specular and diffuse (how much of the light is reflected)
vec3 fresnelSchlick(float cosTheta, vec3 F0) {
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}

void main()
{		
    // Load the material attributes from the textures
    vec3  a_Albedo    = texture(s_Albedo, inTexCoords).xyz;
    float a_Metallic  = texture(s_Metallic , inTexCoords).r;
    float a_Roughness = texture(s_Roughness, inTexCoords).r;
    float ao          = texture(s_AO, inTexCoords).r * a_AO;
    //float a_AO = 1.0f;

    vec3 texNorm = normalize(texture(s_Normal, inTexCoords).xyz * 2.0 - 1);
    texNorm = inTBN * texNorm;
    vec3 norm = mix(inNormal, texNorm, a_NormalFactor);

    // Make sure the fragment normal is normalized
    vec3 N = normalize(norm);
    // Determine the view direction unit vector
    vec3 V = normalize(EyePos - inWorldPos.xyz);

    // Get our Fresnel-Schlick F0 factor from the material
    // We can use this to add tints to metals
    vec3 F0 = a_FresnelFactor; 
    // Lerp between the Fresnel factor and albedo depending on the level of metallic
    F0 = mix(F0, a_Albedo, a_Metallic);
	           
    // This will accumulate our lighting
    vec3 Lo = vec3(0.0);
    // Iterate over all lights
    for(int i = 0; i < NumLights; ++i) 
    {
        // Determine the to-light vector, and get the half vector from it and the view vector
        vec3 L = normalize(a_LightPositions[i] - inWorldPos.xyz);
        vec3 H = normalize(V + L);
        // Get the distance to the light source
        float dist        = length(a_LightPositions[i] - inWorldPos.xyz);
        // Attenuate light according to the inverse-square law
        float attenuation = 1.0 / (dist * dist);
        vec3 radiance     = a_LightColors[i] * attenuation;        
        
        // cook-torrance brdf
        float NDF = DistributionGGX(N, H, a_Roughness);        
        float G   = GeometrySmith(N, V, L, a_Roughness);      
        vec3  kS    = fresnelSchlick(max(dot(H, V), 0.0), F0);
        vec3 numerator    = NDF * G * kS;
        float denominator = 4.0 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0);

        // Divide our factors, with a check to avoid divide by zero issues
        vec3 specular     = numerator / max(denominator, 0.001);         
        
        // kD will represent the amount of light absorbed (the diffuse)
        vec3 kD = vec3(1.0) - kS;
        // We scale it inversely depending on how metallic our material is
        kD *= 1.0 - a_Metallic;	  
                    
        // Determine angle between normal and light
        float NdotL = max(dot(N, L), 0.0);         
        // Accumulate our light into Lo
        Lo += (kD * a_Albedo / PI + specular) * radiance * NdotL; 
    }   
  
    // Calculate our ambient light factor
    vec3 ambient = a_AmbientFactor * a_Albedo * ao;
    // Our end color is the ambient factor + our light's diffuse and reflected factors
    vec3 color = ambient + Lo;
	
    // Tonemap our color into the [0-1] range
    color = color / (color + vec3(1.0));
    // Gamma correction using our exposure settings
    color = pow(color, vec3(1.0 / Exposure));  
   
    outColor = vec4(color, 1.0);
}  