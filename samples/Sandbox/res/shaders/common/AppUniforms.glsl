/* The model view projection, final matrix for model */
uniform mat4  u_ModelViewProjection;
/* View projection matrix */
uniform mat4  u_ViewProjection;
/* model normal matrix */
uniform mat3  u_NormalMatrix;
/* model matrix */
uniform mat4  u_Model;
/* view matrix */
uniform mat4  u_View;
/* Current scene time, in seconds */
uniform float a_Time;
/* The camera's position, in world space */
uniform vec3  u_CamPos;