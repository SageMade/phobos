#version 420

#include "common/AppUniforms.glsl"

#pragma editor
uniform sampler2D s_Diffuse;
#pragma editor
uniform sampler2D s_Specular;
#pragma editor
uniform sampler2D s_Normal;

#pragma editor RenderType=Color
uniform vec3  u_AmbientCol;
#pragma editor Min=0.0, Max=1.0
uniform float u_AmbientStrength;

uniform vec3  u_LightPos;
#pragma editor RenderType=Color
uniform vec3  u_LightCol;
#pragma editor Min=0.0, Max=1.0
uniform float u_AmbientLightStrength;
#pragma editor Min=0.0, Max=1.0
uniform float u_SpecularLightStrength;
#pragma editor Min=0.0, Max=256.0
uniform float u_Shininess;
// NEW in week 7, see https://learnopengl.com/Lighting/Light-casters for a good reference on how this all works, or
// https://developer.valvesoftware.com/wiki/Constant-Linear-Quadratic_Falloff
#pragma editor Min=0.0, Max=1.0
uniform float u_LightAttenuationConstant;
#pragma editor Min=0.0, Max=1.0
uniform float u_LightAttenuationLinear;
#pragma editor Min=0.0, Max=1.0
uniform float u_LightAttenuationQuadratic;

#type VertexShader
#include "common/PassThroughVertex.glsl"

#type FragmentShader

layout(location = 0) in vec3 inPos;
layout(location = 2) in vec3 inNormal;
layout(location = 3) in vec2 inUV;
layout(location = 4) in mat3 inTBN;

out vec4 frag_color;

// https://learnopengl.com/Advanced-Lighting/Advanced-Lighting
void main() {
	// Lecture 5
	vec3 ambient = u_AmbientLightStrength * u_LightCol;

	mat3 tbn = inTBN;
	tbn[0] = normalize(inTBN[0]);
	tbn[1] = normalize(inTBN[1]);
	tbn[2] = normalize(inTBN[2]);

	// Diffuse
	vec3 uvData = normalize((texture(s_Normal, inUV).xyz * 2) - vec3(1.0));
	vec3 N = normalize(tbn * uvData);
	vec3 lightDir = normalize(u_LightPos - inPos);

	float dif = max(dot(N, lightDir), 0.0);
	vec3 diffuse = dif * u_LightCol;// add diffuse intensity

	//Attenuation
	float dist = length(u_LightPos - inPos);
	float attenuation = 1.0f / (
		u_LightAttenuationConstant +
		u_LightAttenuationLinear * dist +
		u_LightAttenuationQuadratic * dist * dist);

	// Specular
	vec3 viewDir = normalize(u_CamPos - inPos);
	vec3 h = normalize(lightDir + viewDir);

	// Get the specular power from the specular map
	float texSpec = texture(s_Specular, inUV).x;
	float spec = pow(max(dot(N, h), 0.0), u_Shininess); // Shininess coefficient (can be a uniform)
	vec3 specular = u_SpecularLightStrength * texSpec * spec * u_LightCol; // Can also use a specular color

	// Get the albedo from the diffuse / albedo map
	vec4 textureColor = texture(s_Diffuse, inUV);

	vec3 result = (
		(u_AmbientCol * u_AmbientStrength) + // global ambient light
		(ambient + diffuse + specular) * attenuation // light factors from our single light
		) * textureColor.rgb; // Object color

	frag_color = vec4((N / 2) + vec3(0.5), textureColor.a);
}