#include <Phobos/Logging.h>
#include "Phobos.h"
#include <iostream>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <filesystem>
#include <json.hpp>
#include <fstream>
#include <optional>

#include <GLM/glm.hpp>
#include <GLM/gtc/matrix_transform.hpp>
#include <GLM/gtc/type_ptr.hpp>

#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

#include "Phobos/Graphics/IndexBuffer.h"
#include "Phobos/Graphics/VertexBuffer.h"
#include "Phobos/Graphics/VertexArrayObject.h"
#include "Phobos/Graphics/Shader.h"
#include "Phobos/Gameplay/Camera.h"
#include "Behaviours/CameraControlBehaviour.h"
#include "Behaviours/FollowPathBehaviour.h"
#include "Behaviours/SimpleMoveBehaviour.h"
#include "Phobos/Application.h"
#include "Phobos/Gameplay/GameObjectTag.h"
#include "Phobos/Gameplay/IBehaviour.h"
#include "Phobos/Gameplay/Transform.h"
#include "Phobos/Graphics/Texture2D.h"
#include "Phobos/Graphics/Texture2DData.h"
#include "Phobos/Input/InputHelpers.h"
#include "Phobos/Utils/MeshBuilder.h"
#include "Phobos/Utils/MeshFactory.h"
#include "Phobos/Utils/ObjLoader.h"
#include "Phobos/Graphics//VertexTypes.h"
#include "Phobos/Gameplay/Scene.h"
#include "Phobos/Graphics/ShaderMaterial.h"
#include "Phobos/Gameplay/RendererComponent.h"
#include "Phobos/Gameplay/Timing.h"
#include "Phobos/Graphics/TextureCubeMap.h"
#include "Phobos/Graphics/TextureCubeMapData.h"
#include "Phobos/Physics/PhysicsSystem.h"
#include <bullet/btBulletDynamicsCommon.h>


#include "Phobos/Physics/Collider.h"
#include "Phobos/Physics/RigidBody.h"
#include "Phobos/Physics/BulletDebugDrawer.h"
#include "Phobos/Utils/AssetDatabase.h"
#include <stb_perlin.h>
#include <bullet/BulletCollision/CollisionShapes/btHeightfieldTerrainShape.h>


#include "imgui_internal.h"
#include "Phobos/Editor/EditorManager.h"
#include "Phobos/EditorHints.h"
#include "Phobos/Editor/Windows/ShaderWindow.h"
#include "Phobos/Editor/Windows/MaterialWindow.h"
#include "Phobos/Utils/StringUtils.h"
#include "Phobos/Utils/Platform/FileDialogs.h"
#include <Phobos/Editor/Windows/TextureWindow.h>
#include <Phobos/Editor/Windows/CodeWindow.h>



#include "Behaviours/SimpleRotateBehaviour.h"
#include "Phobos/Editor/Windows/HierarchyWindow.h"
#include "Phobos/Editor/Windows/InspectorWindow.h"
#include "Phobos/Graphics/DebugDrawer.h"
#include "Phobos/Graphics/FrameBuffer.h"
#include "Phobos/Input/InputManager.h"
#include "Phobos/Physics/PhysicsWorldContainer.h"


using namespace Phobos;
using namespace Phobos::Editor;
using namespace Phobos::Editor::Windows;
using namespace Phobos::Utils;
using namespace Phobos::Graphics;
using namespace Phobos::Gameplay;
using namespace Phobos::Physics;
using namespace Phobos::Input;

//#define LOG_GL_NOTIFICATIONS

/*
	Handles debug messages from OpenGL
	https://www.khronos.org/opengl/wiki/Debug_Output#Message_Components
	@param source    Which part of OpenGL dispatched the message
	@param type      The type of message (ex: error, performance issues, deprecated behavior)
	@param id        The ID of the error or message (to distinguish between different types of errors, like nullref or index out of range)
	@param severity  The severity of the message (from High to Notification)
	@param length    The length of the message
	@param message   The human readable message from OpenGL
	@param userParam The pointer we set with glDebugMessageCallback (should be the game pointer)
*/
void GlDebugMessage(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam) {
	std::string sourceTxt;
	switch (source) {
		case GL_DEBUG_SOURCE_API: sourceTxt = "DEBUG"; break;
		case GL_DEBUG_SOURCE_WINDOW_SYSTEM: sourceTxt = "WINDOW"; break;
		case GL_DEBUG_SOURCE_SHADER_COMPILER: sourceTxt = "SHADER"; break;
		case GL_DEBUG_SOURCE_THIRD_PARTY: sourceTxt = "THIRD PARTY"; break;
		case GL_DEBUG_SOURCE_APPLICATION: sourceTxt = "APP"; break;
		case GL_DEBUG_SOURCE_OTHER: default: sourceTxt = "OTHER"; break;
	}
	switch (severity) {
		case GL_DEBUG_SEVERITY_LOW:          LOG_INFO("[{}] {}", sourceTxt, message); break;
		case GL_DEBUG_SEVERITY_MEDIUM:       LOG_WARN("[{}] {}", sourceTxt, message); break;
		case GL_DEBUG_SEVERITY_HIGH:         LOG_ERROR("[{}] {}", sourceTxt, message); break;
			#ifdef LOG_GL_NOTIFICATIONS
		case GL_DEBUG_SEVERITY_NOTIFICATION: LOG_INFO("[{}] {}", sourceTxt, message); break;
			#endif
		default: break;
	}
}

GLFWwindow* window;
glm::ivec4 gameViewport;
glm::ivec2 editorWindowSize;

void HandleGameWindowResize(int width, int height)
{
	Application::Instance().ActiveScene->Registry().view<Camera>().each([=](Camera& cam) {
		cam.ResizeWindow(width, height);
	});
	Application::Instance().WindowSize = glm::ivec2(width, height);
}

void GlfwWindowResizedCallback(GLFWwindow* window, int width, int height) {
	glViewport(0, 0, width, height);
	//HandleGameWindowResize(width, height);
	editorWindowSize = glm::ivec2(width, height);
}

bool initGLFW() {
	if (glfwInit() == GLFW_FALSE) {
		LOG_ERROR("Failed to initialize GLFW");
		return false;
	}
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	#ifdef _DEBUG
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, true);
	#endif

	//Create a new GLFW window
	window = glfwCreateWindow(800, 800, "INFR1350U", nullptr, nullptr);
	glfwMakeContextCurrent(window);

	// Set our window resized callback
	glfwSetWindowSizeCallback(window, GlfwWindowResizedCallback);

	// Store the window in the application singleton
	Application::Instance().Window = window;
	Application::Instance().WindowSize = glm::ivec2(800);
	editorWindowSize = glm::ivec2(800);
	Input::InputManager::Init(window);

	return true;
}

bool initGLAD() {
	if (gladLoadGLLoader((GLADloadproc)glfwGetProcAddress) == 0) {
		LOG_ERROR("Failed to initialize Glad");
		return false;
	}
	#ifdef _DEBUG
	// Let OpenGL know that we want debug output, and route it to our handler function
	glEnable(GL_DEBUG_OUTPUT);
	glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
	glDebugMessageCallback(GlDebugMessage, nullptr);
	#endif
	return true;
}

void InitImGui() {
	// Creates a new ImGUI context
	ImGui::CreateContext();
	// Gets our ImGUI input/output 
	ImGuiIO& io = ImGui::GetIO();
	io.Fonts->AddFontDefault();
	// Enable keyboard navigation
	io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
	// Allow docking to our window
	io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;
	// Allow multiple viewports (so we can drag ImGui off our window)
	io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;
	// Allow our viewports to use transparent backbuffers
	io.ConfigFlags |= ImGuiConfigFlags_TransparentBackbuffers;

	// Set up the ImGui implementation for OpenGL
	ImGui_ImplGlfw_InitForOpenGL(window, true);
	ImGui_ImplOpenGL3_Init("#version 410");

	// Dark mode FTW
	ImGui::StyleColorsDark();

	// Get our imgui style
	ImGuiStyle& style = ImGui::GetStyle();
	//style.Alpha = 1.0f;
	if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable) {
		style.WindowRounding = 0.0f;
		style.Colors[ImGuiCol_WindowBg].w = 0.8f;
		//style.Colors[ImGuiCol_ChildBg] = ImVec4(1.0f, 0.9f, 0.9f, 0.8f);
		//style.Colors[ImGuiCol_ChildWindowBg] = ImVec4(1.0f, 0.8f, 1.0f, 0.8f);
		//style.Colors[ImGuiCol_Header] = ImVec4(1.0f, 0.1f, 0.9f, 0.8f);
		//style.Colors[ImGuiCol_Tab] = ImVec4(1.0f, 0.8f, 1.0f, 0.8f);
		//style.Colors[ImGuiCol_TabActive] = ImVec4(1.0f, 0.9f, 1.0f, 0.8f);
		//style.Colors[ImGuiCol_TabUnfocused] = ImVec4(1.0f, 0.8f, 1.0f, 0.8f);
		//style.Colors[ImGuiCol_Text] = ImVec4(0.0f, 0.0f, 0.0f, 1.0f);
	}

	TextEditor::TextEditor();
}

void ShutdownImGui()
{
	// Cleanup the ImGui implementation
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	// Destroy our ImGui context
	ImGui::DestroyContext();
}

std::vector<std::function<void()>> imGuiCallbacks;
void RenderImGui() {
	// Implementation new frame
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	// ImGui context new frame
	ImGui::NewFrame();

	glm::ivec2 size;
	ImGuiViewport* viewport = ImGui::GetMainViewport();
	if (viewport->Size.x > 0 && viewport->Size.y > 0) {
		ImGui::SetNextWindowPos(viewport->Pos);
		ImGui::SetNextWindowSize(viewport->Size);
		ImGui::SetNextWindowViewport(viewport->ID);
		ImGuiWindowFlags window_flags = ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoDocking;
		window_flags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;
		window_flags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus | ImGuiWindowFlags_NoBackground;

		ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
		ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
		ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
		ImGui::PushStyleColor(ImGuiCol_WindowBg, { 0,0,0,0 });
		ImGui::Begin("Docker Window", nullptr, window_flags);
		ImGui::PopStyleVar(3);
		ImGui::PopStyleColor();

		ImGuiDockNodeFlags dockspaceFlags = ImGuiDockNodeFlags_None;
		ImGuiID dockspaceID = ImGui::GetID("MainDocker");
		if (!ImGui::DockBuilderGetNode(dockspaceID)) {
			ImGui::DockBuilderRemoveNode(dockspaceID);
			ImGui::DockBuilderAddNode(dockspaceID, ImGuiDockNodeFlags_None);
			ImGui::DockBuilderSetNodeSize(dockspaceID, viewport->Size);

			ImGuiID dock_main_id = dockspaceID;
			ImGuiID dock_right_id = ImGui::DockBuilderSplitNode(dock_main_id, ImGuiDir_Right, 0.2f, nullptr, &dock_main_id);
			ImGuiID dock_right_lower_id = ImGui::DockBuilderSplitNode(dock_right_id, ImGuiDir_Down, 0.3f, nullptr, &dock_main_id);
			ImGuiID dock_left_id = ImGui::DockBuilderSplitNode(dock_main_id, ImGuiDir_Left, 0.2f, nullptr, &dock_main_id);
			ImGuiID dock_left_lower_id = ImGui::DockBuilderSplitNode(dock_left_id, ImGuiDir_Down, 0.2f, nullptr, &dock_main_id);
			ImGuiID dock_down_id = ImGui::DockBuilderSplitNode(dock_main_id, ImGuiDir_Down, 0.2f, nullptr, &dock_main_id);

			ImGui::DockBuilderDockWindow("Game View", dock_main_id);
			ImGui::DockBuilderDockWindow("Hierarchy", dock_left_id);
			ImGui::DockBuilderDockWindow("Textures", dock_left_lower_id);
			ImGui::DockBuilderDockWindow("Inspector", dock_right_id);
			ImGui::DockBuilderDockWindow("Materials", dock_right_lower_id);
			ImGui::DockBuilderDockWindow("Debug", dock_down_id);

			ImGui::DockBuilderFinish(dock_main_id);
		}
		ImGui::DockBuilderSetNodeSize(dockspaceID, viewport->Size);
		ImGui::DockSpace(dockspaceID, ImVec2(0.0f, 0.0f), 0);
		if (ImGui::BeginMenuBar())
		{
			if (ImGui::BeginMenu("File")) {
				if (ImGui::MenuItem("New Scene", NULL, false)) {
				}
				if (ImGui::MenuItem("Load Scene", NULL, false)) {
					std::optional<std::string> path = Platform::FileDialogs::OpenFile("Scene File\0*.scene\0\0");
					if (path.has_value()) {
						std::string name = std::filesystem::path(path.value()).filename().string();
						GameScene::sptr scene = Application::Instance().ActiveScene = GameScene::Create(name);
						scene->Load(path.value());
					}
				}
				if (ImGui::MenuItem("Save Scene", NULL, false)) {
					std::optional<std::string> path = Platform::FileDialogs::SaveFile("Scene File\0*.scene\0\0");
					if (path.has_value()) {
						Application::Instance().ActiveScene->Save(path.value());
					}
				}
				ImGui::Separator();
				if (ImGui::MenuItem("Save Asset Database", NULL, false)) {
					std::optional<std::string> path = Platform::FileDialogs::SaveFile("Asset File\0*.assets\0\0");
					if (path.has_value()) {
						std::string name = std::filesystem::path(path.value()).filename().string();
						std::ofstream file(path.value());
						cereal::BinaryOutputArchive outFile(file);
						AssetDatabase::Get().SaveAssets<Shader, ShaderMaterial, Texture2D, TextureCubeMap>(outFile);
					}
				}
				if (ImGui::MenuItem("Load Asset Database", NULL, false)) {
					std::optional<std::string> path = Platform::FileDialogs::OpenFile("Asset File\0*.assets\0\0");
					if (path.has_value()) {
						std::string name = std::filesystem::path(path.value()).filename().string();
						std::ifstream file(path.value());
						cereal::BinaryInputArchive inFile(file);
						AssetDatabase::Get().LoadAssets<Shader, ShaderMaterial, Texture2D, TextureCubeMap>(inFile);
					}
				}
				ImGui::EndMenu();
			}
			if (ImGui::BeginMenu("Windows")) {
				Phobos::Editor::EditorManager::RenderWindowMenuItems();
				ImGui::Separator();
				if (ImGui::MenuItem("<new item>")) {
					std::optional<std::string> file = Platform::FileDialogs::OpenFile("All Files\0*.*\0\0");
					if (file.has_value()) {
						auto ptr = Phobos::Editor::EditorManager::AddFileWindow<CodeWindow>(file.value());
						ptr->OpenFile(file.value());
					}
				}
				ImGui::EndMenu();
			}

			ImGui::EndMenuBar();
		}
		ImGui::End();
	}

	{
		ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
		ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
		ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
		ImGuiWindowFlags window_flags = 0;
		window_flags |= ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoBackground;
		window_flags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoMove;


		ImGuiViewport* viewport = ImGui::GetMainViewport();

		ImVec2 rootPos = viewport->Pos;
		ImVec2 rootSize = viewport->Size;

		ImGui::PushStyleColor(ImGuiCol_WindowBg, { 0,0,0,0 });
		ImGui::Begin("Game View", nullptr, window_flags);
		Application::Instance().GameHasFocus = ImGui::IsWindowFocused(ImGuiFocusedFlags_RootAndChildWindows);
		ImVec2 subPos = ImGui::GetWindowPos();
		ImVec2 relPos = { subPos.x - rootPos.x, subPos.y - rootPos.y };
		static ImVec2 prevSize = ImVec2(0.0f, 0.0f);
		ImVec2 size = ImGui::GetWindowSize();
		if (size.x != prevSize.x || size.y != prevSize.y) {
			//GlfwWindowResizedCallback(window, size.x, size.y);
			HandleGameWindowResize(size.x, size.y);
			prevSize = size;
		}
		gameViewport = glm::vec4(relPos.x, rootSize.y - relPos.y - size.y, size.x, size.y);
		ImGui::End();
		ImGui::PopStyleVar(3);
		ImGui::PopStyleColor();
	}

	Phobos::Editor::EditorManager::Render();

	// Render our GUI stuff
	for (auto& func : imGuiCallbacks) {
		func();
	}

	// Make sure ImGui knows how big our window is
	ImGuiIO& io = ImGui::GetIO();
	int width{ 0 }, height{ 0 };
	glfwGetWindowSize(window, &width, &height);
	io.DisplaySize = ImVec2((float)width, (float)height);

	// Render all of our ImGui elements
	ImGui::Render();
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

	// If we have multiple viewports enabled (can drag into a new window)
	if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable) {
		// Update the windows that ImGui is using
		ImGui::UpdatePlatformWindows();
		ImGui::RenderPlatformWindowsDefault();
		// Restore our gl context
		glfwMakeContextCurrent(window);
	}
}

void RenderVAO(
	const Shader::sptr& shader,
	const VertexArrayObject::sptr& vao,
	const glm::mat4& viewProjection,
	const Transform& transform)
{
	if (vao != nullptr && shader != nullptr) {
		shader->SetUniform("u_ModelViewProjection", viewProjection * transform.WorldTransform());
		shader->SetUniform("u_Model", transform.WorldTransform());
		shader->SetUniform("u_NormalMatrix", transform.WorldNormalMatrix());
		vao->Render();
	}
}

void SetupShaderForFrame(const Shader::sptr& shader, const glm::mat4& view, const glm::mat4& projection) {
	shader->Bind();
	// These are the uniforms that update only once per frame
	shader->SetUniform("u_View", view);
	shader->SetUniform("u_ViewProjection", projection * view);
	shader->SetUniform("u_SkyboxMatrix", projection * glm::mat4(glm::mat3(view)));
	float time = (float)Timing::Instance().CurrentFrame;
	shader->SetUniform("a_Time", time);
	glm::vec3 camPos = glm::inverse(view) * glm::vec4(0, 0, 0, 1);
	shader->SetUniform("u_CamPos", camPos);
}

int main() {
	Logger::Init(); // We'll borrow the logger from the toolkit, but we need to initialize it

	//Initialize GLFW
	if (!initGLFW())
		return 1;

	//Initialize GLAD
	if (!initGLAD())
		return 1;

	bool isDebugEnabled = false;

	int frameIx = 0;
	float fpsBuffer[128];
	float minFps, maxFps, avgFps;
	int selectedVao = 0; // select cube by default
	std::vector<GameObject> controllables;


	// Enable texturing
	//glEnable(GL_TEXTURE_2D);

	// Push another scope so most memory should be freed *before* we exit the app
	{
		float* heightMap;

		#pragma region Shader and ImGui
		const glm::mat3 DEFAULT_ENV_ROT = glm::mat3(glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(1, 0, 0)));

		// Load our shaders
		Shader::sptr shader = AssetDatabase::Get().LoadFromFile<Shader>("shaders/terrain.glsl");
		shader->SetUniformConfigurable("u_EnvironmentRotation", true);
		shader->SetDefault("u_EnvironmentRotation", DEFAULT_ENV_ROT);
		shader->SetDefault("u_TextureScaling", glm::vec2(0.05f, 0.05f));



		// These are our application / scene level uniforms that don't necessarily update
		// every frame

		#pragma endregion 

		// GL states
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glDepthFunc(GL_LEQUAL); // New 

		#pragma region TEXTURE LOADING

		// Load some textures from files
		AssetDatabase& db = AssetDatabase::Get();
		Texture2D::sptr diffuse = db.LoadFromFile<Texture2D>("images/Stone_001_Diffuse.png");
		Texture2D::sptr diffuse2 = db.LoadFromFile<Texture2D>("images/box.bmp");
		Texture2D::sptr specular = db.LoadFromFile<Texture2D>("images/Stone_001_Specular.png");
		Texture2D::sptr reflectivity = db.LoadFromFile<Texture2D>("images/box-reflections.bmp");
		Texture2D::sptr normal = db.LoadFromFile<Texture2D>("images/Stone_001_Normal.png");

		// Load terrain textures
		Texture2D::sptr sand = db.LoadFromFile<Texture2D>("images/sand.jpg");
		Texture2D::sptr grass = db.LoadFromFile<Texture2D>("images/grass.jpg");
		Texture2D::sptr stone = db.LoadFromFile<Texture2D>("images/stone.jpg");
		Texture2D::sptr snow = db.LoadFromFile<Texture2D>("images/snow.jpg");

		// Load the cube map
		//TextureCubeMap::sptr environmentMap = TextureCubeMap::LoadFromImages("images/cubemaps/skybox/sample.jpg");
		TextureCubeMap::sptr environmentMap = TextureCubeMap::LoadFromImages("images/cubemaps/milkyway/milky.png");
		AssetDatabase::Get().Register(environmentMap);

		// Creating an empty texture
		Texture2DDescription desc = Texture2DDescription();
		desc.Width = 1;
		desc.Height = 1;
		desc.Format = InternalFormat::RGB8;
		Texture2D::sptr texture2 = Texture2D::Create(desc);
		// Clear it with a white colour
		texture2->Clear();

		#pragma endregion

		///////////////////////////////////// Scene Generation //////////////////////////////////////////////////
		#pragma region Scene Generation

		// We need to tell our scene system what extra component types we want to support
		GameScene::RegisterComponentType<RendererComponent>();
		GameScene::RegisterComponentType<BehaviourBinding>();
		GameScene::RegisterComponentType<Camera>();
		GameScene::RegisterComponentType<RigidBody>();
		GameScene::RegisterComponentType<Collider>();

		// Create a scene, and set it to be the active scene in the application
		Application::Instance().ActiveScene = GameScene::Create("test");

		// We'll add some ImGui controls to control our shader
		EditorManager::RegisterEditor<ShaderWindow>();
		EditorManager::RegisterEditor<MaterialWindow>();
		EditorManager::RegisterEditor<TextureWindow>();
		EditorManager::RegisterEditor<HierarchyWindow>();
		EditorManager::RegisterEditor<InspectorWindow>();

		// Scene init
		{
			GameScene::sptr scene = Application::Instance().ActiveScene;

			GameObject cameraObject = GameObject(entt::handle(scene->Registry(), entt::null));
			ShaderMaterial::sptr material0;

			bool showDemo = false;
			imGuiCallbacks.push_back([&]() {

				if (ImGui::Begin("Debug")) {
					ImGui::Checkbox("Debug Enabled", &isDebugEnabled);

					minFps = FLT_MAX;
					maxFps = 0;
					avgFps = 0;
					for (int ix = 0; ix < 128; ix++) {
						if (fpsBuffer[ix] < minFps) { minFps = fpsBuffer[ix]; }
						if (fpsBuffer[ix] > maxFps) { maxFps = fpsBuffer[ix]; }
						avgFps += fpsBuffer[ix];
					}
					Transform& camTransform = Application::Instance().ActiveScene->MainCamera.Get<Transform>();
					glm::vec3 pos = camTransform.GetLocalPosition();

					ImGui::PlotLines("FPS", fpsBuffer, 128);
					ImGui::Text("MIN: %f MAX: %f AVG: %f", minFps, maxFps, avgFps / 128.0f);
					ImGui::Text("Cam Pos: %f, %f, %f", pos.x, pos.y, pos.z);

					ImGui::Checkbox("Show ImGui Demo", &showDemo);
					if (showDemo) ImGui::ShowDemoWindow(&showDemo);
				}
				ImGui::End();
			});

			// Create material 0 for the terrain
			{
				// Create a material and set some properties for it
				material0 = AssetDatabase::Get().Create<ShaderMaterial>("Terrain");
				material0->SetShader(shader);
				material0->Set("s_Sand", sand);
				material0->Set("s_Grass", grass);
				material0->Set("s_Rock", stone);
				material0->Set("s_Snow", snow);
				material0->Set("u_Shininess", 8.0f);
				material0->Set("u_TextureMix", 0.5f);
				material0->Set("u_LightPos", glm::vec3(0.0f, 0.0f, 2.0f));
				material0->Set("u_LightCol", glm::vec3(0.9f, 0.85f, 0.5f));
				material0->Set("u_AmbientLightStrength", 0.5f);
				material0->Set("u_SpecularLightStrength", 1.0f);
				material0->Set("u_AmbientCol", glm::vec3(1.0f));
				material0->Set("u_AmbientStrength", 0.1f);
				material0->Set("u_LightAttenuationConstant", 1.0f);
				material0->Set("u_LightAttenuationLinear", 0.09f);
				material0->Set("u_LightAttenuationQuadratic", 0.032f);
				material0->Set("u_HeightCutoffs", glm::vec3(0.35f, 0.567f, 0.764f));
				material0->Set("u_InterpolateFactors", glm::vec3(0.04f, 0.08f, 0.08f));
				material0->Set("u_MinHeight", -20.0f);
				material0->Set("u_MaxHeight", 20.0f);
			}

			// terrain generation
			{
				const size_t width{ 128 }, height{ 128 };
				const float scaleX{ 76.0f }, scaleY{ 76.0f };
				heightMap = new float[width * height];
				float min = FLT_MAX;
				float max = -FLT_MAX;
				srand(time(nullptr));
				float linearization = glm::sqrt(3.0f);
				for (int ix = 0; ix < width; ix++) {
					for (int iy = 0; iy < height; iy++) {
						float h = stb_perlin_noise3(ix / scaleX, iy / scaleY, 0.0f, 0, 0, 0) * linearization * 2.0f * 20.0f;
						min = glm::min(h, min);
						max = glm::max(h, max);
						heightMap[ix + iy * width] = h;
					}
				}

				GameObject terrainObj = scene->CreateEntity("Terrain");
				MeshBuilder<VertexPosNormTexCol> builder = MeshBuilder<VertexPosNormTexCol>();
				MeshFactory::AddTerrain(builder, width, height, heightMap, glm::vec3(1.0f, 1.0f, 1.0f), glm::vec2(2.0f));
				VertexArrayObject::sptr vao = builder.Bake();
				AssetDatabase::Get().Register(vao);
				//VertexArrayObject::sptr vao = ObjLoader::LoadFromFile("models/plane.obj");

				Collider::TerrainData data;
				data.Width = width;
				data.Height = height;
				data.DataType = TerrainDataType::Float;
				data.MinHeight = min;
				data.MaxHeight = max;
				data.HeightScale = 1.0f;
				data.Axis = TerrainAxis::Z;
				data.FlipQuadEdges = false;
				data.Allocate();
				memcpy(data.HeightMap, heightMap, data.GetDataSize());
				
				terrainObj.Add<RendererComponent>().SetMesh(vao).SetMaterial(material0);
				terrainObj.Get<Transform>().SetLocalPosition(0.0f, 0.0f, -10.0f);
				auto shape = std::make_shared<btHeightfieldTerrainShape>(width, height, heightMap, 1.0f, min, max, 2, PHY_FLOAT, false);
				shape->setFlipTriangleWinding(true);
				shape->buildAccelerator();
				terrainObj.Add<Collider>(ColliderType::Terrain).SetColliderDataPtr(data);
				terrainObj.Add<RigidBody>().SetMass(0.0f);
			}

			// Water
			{
				Shader::sptr shader = AssetDatabase::Get().Create<Shader>("Water");
				shader->LoadFromFile("shaders/water.glsl");
				shader->SetUniformConfigurable("u_EnvironmentRotation", true);
				shader->SetDefault("u_EnvironmentRotation", DEFAULT_ENV_ROT);
				shader->SetUniformConfigurable("u_CamPos", false);
				shader->SetUniformConfigurable("a_Time", false);
				shader->SetUniformEditorHints("a_EnabledWaves", EditorHints(0.0f, 8.0f, 1.0f));
				shader->SetUniformEditorHints("a_FresnelPower", EditorHints(0.0f, 1.0f, 0.001f));
				shader->SetUniformEditorHints("a_WaterAlpha", EditorHints(0.0f, 1.0f, 0.001f));
				shader->SetUniformEditorHints("a_WaterClarity", EditorHints(0.0f, 1.0f, 0.001f));
				shader->SetUniformEditorHints("a_Waves", EditorHints(0.0f, 64.0f, 0.001f));
				shader->SetUniformEditorHints("a_WaterColor", EditorRenderType::Color);

				ShaderMaterial::sptr waterMat = AssetDatabase::Get().Create<ShaderMaterial>("Water");
				waterMat->SetName("Water");
				waterMat->SetShader(shader);
				waterMat->DepthWrite = false;

				glm::vec4 waveData[4] = {
					glm::vec4(1.0f, 0.2f, 0.50f, 10.0f),
					glm::vec4(0.1f, 1.0f, 0.25f, 3.1f),
					glm::vec4(0.5f, 1.0f, 0.20f, 35.0f),
					glm::vec4(0.5f, 0.7f, 0.20f, 2.0f)
				};
				waterMat->Set("a_Gravity", 9.81f);
				waterMat->Set("a_EnabledWaves", 4);
				waterMat->Set("a_Waves", ShaderDataType::Float4, waveData, 4);
				waterMat->Set("a_WaterColor", glm::vec3(0.7f, 1.0f, 0.9f));
				waterMat->Set("a_WaterClarity", 0.9f);
				waterMat->Set("a_FresnelPower", 0.5f);
				waterMat->Set("a_WaterAlpha", 0.5f);
				waterMat->Set("a_RefractionIndex", 1.0f / 1.34f);
				waterMat->Set("s_Environment", environmentMap);
				waterMat->IsTransparent = true;
				waterMat->DepthWrite = true;

				MeshBuilder<VertexPosNormTexCol> builder = MeshBuilder<VertexPosNormTexCol>();
				MeshFactory::AddPlane(builder,
									  glm::vec3(0.0f), glm::vec3(0, 0, 1.0f), glm::vec3(1, 0, 0),
									  glm::vec2(128, 128), glm::ivec2(100, 100));
				VertexArrayObject::sptr vao = builder.Bake();

				GameObject waterObj = scene->CreateEntity("Water plane");
				waterObj.Add<RendererComponent>().SetMesh(vao).SetMaterial(waterMat);
				waterObj.Get<Transform>().SetLocalPosition(0.0f, 0.0f, -20.0f);
			}

			// camera
			{
				// Create an object to be our camera
				cameraObject = scene->CreateEntity("Camera");
				scene->MainCamera = cameraObject;
				{
					cameraObject.Get<Transform>().SetLocalPosition(0, 3, 3).LookAt(glm::vec3(0, 0, 0));

					// We'll make our camera a component of the camera object
					Camera& camera = cameraObject.Add<Camera>();// Camera::Create();
					camera.SetFovDegrees(90.0f); // Set an initial FOV
					camera.SetOrthoHeight(3.0f);
					BehaviourBinding::Bind<CameraControlBehaviour>(cameraObject);

					cameraObject.Add<Collider>(ColliderType::Sphere).SetColliderData(0.25f);
					cameraObject.Add<RigidBody>().
						SetMass(0.0f).
						SetAlwaysActive(true).
						SetIsKinematic(true).
						SetIsDebugDrawEnabled(false);
				}

			}

			// Scene initialization
			{
				// Load a second material for our reflective material!
				Shader::sptr reflectiveShader = AssetDatabase::Get().Create<Shader>("Mirror");
				reflectiveShader->LoadShaderPartFromFile("shaders/vertex_shader.glsl", ShaderStageType::VertexShader);
				reflectiveShader->LoadShaderPartFromFile("shaders/frag_reflection.frag.glsl", ShaderStageType::FragmentShader);
				reflectiveShader->Link();
				reflectiveShader->SetUniformConfigurable("u_EnvironmentRotation", true);
				reflectiveShader->SetUniformConfigurable("u_CamPos", false);
				reflectiveShader->SetDefault("u_EnvironmentRotation", DEFAULT_ENV_ROT);

				Shader::sptr reflective = AssetDatabase::Get().Create<Shader>("Reflective");
				reflective->LoadShaderPartFromFile("shaders/vertex_shader.glsl", ShaderStageType::VertexShader);
				reflective->LoadShaderPartFromFile("shaders/frag_blinn_phong_reflection.glsl", ShaderStageType::FragmentShader);
				reflective->Link();
				reflective->SetUniformConfigurable("u_EnvironmentRotation", true);
				reflective->SetUniformConfigurable("u_CamPos", false);
				reflective->SetUniformEditorHints("s_Diffuse", EditorRenderType::Default);
				reflective->SetUniformEditorHints("s_Diffuse2", EditorRenderType::Default);
				reflective->SetUniformEditorHints("s_Reflectivity", EditorRenderType::Default);
				reflective->SetUniformEditorHints("s_Specular", EditorRenderType::Default);
				reflective->SetDefault("u_EnvironmentRotation", DEFAULT_ENV_ROT);

				// 
				ShaderMaterial::sptr material1 = material0->Clone();
				material1->SetShader(reflective);
				material1->SetName("Reflective");
				material1->Set("s_Diffuse", diffuse);
				material1->Set("s_Diffuse2", diffuse2);
				material1->Set("s_Specular", specular);
				material1->Set("s_Reflectivity", reflectivity);
				material1->Set("s_Environment", environmentMap);

				ShaderMaterial::sptr reflectiveMat = AssetDatabase::Get().Create<ShaderMaterial>("Mirror");
				reflectiveMat->SetShader(reflectiveShader);
				reflectiveMat->SetName("Mirror");
				reflectiveMat->Set("s_Environment", environmentMap);

				std::vector<GameObject> _sceneDynamics;

				GameObject worldRoot = scene->CreateEntity("physicsRoot");
				auto& physicsController = worldRoot.Add<PhysicsWorldContainer>();
				physicsController.World = PhysicsSystem::Get().CreateWorld();
				auto moveCtrl = BehaviourBinding::Bind<SimpleRotateBehaviour>(worldRoot);
				moveCtrl->Relative = true;
				moveCtrl->EulerPerSec = glm::vec3(90.0f, 0.0, 0.0f);

				cameraObject.Get<RigidBody>().SetPhysicsWorld(physicsController.World);
				cameraObject.SetParent(worldRoot);
				
				VertexArrayObject::sptr monkey = ObjLoader::LoadFromFile("models/monkey.obj");
				monkey->SetName("Monkey");
				AssetDatabase::Get().Register(monkey);

				// Build a mesh
				MeshBuilder<VertexPosNormTexCol> builder = MeshBuilder<VertexPosNormTexCol>();
				MeshFactory::AddCube(builder, glm::vec3(0.0f), glm::vec3(1.0f, 1.0f, 1.0f));
				VertexArrayObject::sptr cube = builder.Bake();
				AssetDatabase::Get().Register(cube);

				GameObject obj3 = scene->CreateEntity("monkey_tris");
				{
					obj3.Add<RendererComponent>().SetMesh(monkey).SetMaterial(reflectiveMat);
					obj3.Get<Transform>().SetLocalPosition(2.0f, 0.0f, 1.5f);
					BehaviourBinding::BindDisabled<SimpleMoveBehaviour>(obj3);

					obj3.Add<Collider>(ColliderType::Sphere).SetColliderData(0.5f);
					obj3.Add<RigidBody>().SetMass(1.0f);
				}

				GameObject obj_floor = scene->CreateEntity("boxFloor");
				{

					obj_floor.Add<RendererComponent>().SetMesh(cube).SetMaterial(material1);
					obj_floor.SetParent(worldRoot);
					obj_floor.Get<Transform>().SetLocalPosition(0.0f, 0.0f, -0.5f).SetLocalScale(10.0f, 10.0f, 0.1f);

					obj_floor.Add<Collider>(ColliderType::Box).SetColliderData(0.5f, 0.5f, 0.5f);
					obj_floor.Add<RigidBody>().SetMass(0.0f).SetPhysicsWorld(physicsController.World);
				}

				GameObject obj4 = scene->CreateEntity("moving_box");
				{
					obj4.Add<RendererComponent>().SetMesh(cube).SetMaterial(material1);
					obj4.SetParent(worldRoot);
					obj4.Get<Transform>().SetLocalPosition(-2.0f, 0.0f, 1.0f).SetLocalScale(2.0f, 2.0f, 1.0f);

					obj4.Add<Collider>(ColliderType::Box).SetColliderData(0.5f, 0.5f, 0.5f);
					obj4.Add<RigidBody>().SetMass(1.0f).SetPhysicsWorld(physicsController.World);
				}

				GameObject obj6 = scene->CreateEntity("following_monkey");
				{
					obj6.Add<RendererComponent>().SetMesh(monkey).SetMaterial(reflectiveMat);
					obj6.SetParent(obj4);
					obj6.Get<Transform>().SetLocalPosition(0.0f, 0.0f, 3.0f);
					obj6.Add<Collider>(ColliderType::Sphere).SetColliderData(0.5f);

					auto pathing = BehaviourBinding::Bind<FollowPathBehaviour>(obj6);
					// Set up a path for the object to follow
					pathing->Points.push_back({ 0.0f, 0.0f, 1.0f });
					pathing->Points.push_back({ 0.0f, 0.0f, 3.0f });
					pathing->Speed = 2.0f;

				}


				GameObject obj7 = scene->CreateEntity("monkey_tris_normals");
				{
					Shader::sptr normalMapShader = AssetDatabase::Get().LoadFromFile<Shader>("shaders/normal_mapping.glsl");

					ShaderMaterial::sptr normalMaterial = material0->Clone();
					normalMaterial->SetShader(normalMapShader);
					normalMaterial->SetName("Normal Mapping");
					normalMaterial->Set("s_Diffuse", diffuse);
					normalMaterial->Set("s_Specular", specular);
					normalMaterial->Set("s_Normal", normal);
					normalMaterial->Set("s_Reflectivity", reflectivity);
					normalMaterial->Set("s_Environment", environmentMap);

					MeshBuilder<VertexPosNormTexCol> builder = MeshBuilder<VertexPosNormTexCol>();
					MeshFactory::AddCube(builder, glm::vec3(0.0f), glm::vec3(1.0f), glm::vec3(0.0f), glm::vec4(1.0f, 0.5f, 0.5f, 1.0f));
					VertexArrayObject::sptr vao = builder.Bake();
					AssetDatabase::Get().Register(vao);
					obj7.Add<RendererComponent>().SetMesh(vao).SetMaterial(normalMaterial);
					obj7.Get<Transform>().SetLocalPosition(4.0f, -4.0f, 1.5f);
				}

				#pragma endregion 
				//////////////////////////////////////////////////////////////////////////////////////////

				/////////////////////////////////// SKYBOX ///////////////////////////////////////////////
				{
					// Load our shaders
					Shader::sptr skybox = AssetDatabase::Get().Create<Shader>("Skybox");
					skybox->LoadShaderPartFromFile("shaders/skybox-shader.vert.glsl", ShaderStageType::VertexShader);
					skybox->LoadShaderPartFromFile("shaders/skybox-shader.frag.glsl", ShaderStageType::FragmentShader);
					skybox->Link();
					skybox->SetUniformConfigurable("u_EnvironmentRotation", true);
					skybox->SetDefault("u_EnvironmentRotation", DEFAULT_ENV_ROT);


					ShaderMaterial::sptr skyboxMat = AssetDatabase::Get().Create<ShaderMaterial>("Skybox");
					skyboxMat->SetShader(skybox);
					skyboxMat->Set("s_Environment", environmentMap);
					skyboxMat->Set("u_EnvironmentRotation", DEFAULT_ENV_ROT);
					skyboxMat->RenderLayer = 100;

					MeshBuilder<VertexPosNormTexCol> mesh;
					MeshFactory::AddIcoSphere(mesh, glm::vec3(0.0f), 1.0f);
					MeshFactory::InvertFaces(mesh);
					VertexArrayObject::sptr meshVao = mesh.Bake();
					AssetDatabase::Get().Register(meshVao);

					GameObject skyboxObj = scene->CreateEntity("skybox");
					skyboxObj.Get<Transform>().SetLocalPosition(0.0f, 0.0f, 0.0f);
					skyboxObj.GetOrAdd<RendererComponent>().SetMesh(meshVao).SetMaterial(skyboxMat);
				}
			}

			// Instanced object
			if (false) {
				struct InstanceData
				{
					glm::mat4 Model;
					glm::mat3 NormalMatrix;

					InstanceData() {}
					InstanceData(const glm::mat4& model) {
						Model = model;
						NormalMatrix = glm::mat3(glm::transpose(glm::inverse(model)));
					}
				};

				Shader::sptr instancedShader = AssetDatabase::Get().Create<Shader>("Instanced");
				instancedShader->LoadShaderPartFromFile("shaders/vertex_shader_instanced.glsl", ShaderStageType::VertexShader);
				instancedShader->LoadShaderPartFromFile("shaders/frag_reflection.frag.glsl", ShaderStageType::FragmentShader);
				instancedShader->Link();
				instancedShader->SetUniformConfigurable("u_CamPos", false);
				instancedShader->SetUniformConfigurable("u_EnvironmentRotation", true);
				instancedShader->SetDefault("u_EnvironmentRotation", DEFAULT_ENV_ROT);


				ShaderMaterial::sptr mat = material0->Clone();
				mat->SetName("Instanced");
				mat->SetShader(instancedShader);
				mat->Set("s_Environment", environmentMap);

				VertexArrayObject::sptr instanceTest = ObjLoader::LoadFromFile("models/monkey_quads.obj");
				VertexBuffer::sptr instanceData = VertexBuffer::Create();
				InstanceData* instances = new InstanceData[10000];
				glm::mat4 IDENTITY = glm::mat4(1.0f);
				for (int ix = 0; ix < 10; ix++) {
					for (int iy = 0; iy < 10; iy++) {
						for (int iz = 0; iz < 100; iz++) {
							instances[ix + iy * 10 + iz * 100] =
								InstanceData(glm::translate(IDENTITY, glm::vec3(ix * 2, iy * 2, iz * 2)));
						}
					}
				}
				instanceData->LoadData<InstanceData>(instances, 10000);
				delete[] instances;
				instanceTest->AddVertexBuffer(instanceData, {
					BufferAttribute(6,  4, GL_FLOAT, false, sizeof(InstanceData), 0, AttribUsage::User0, 1),
					BufferAttribute(7,  4, GL_FLOAT, false, sizeof(InstanceData), 4 * sizeof(float), AttribUsage::User0, 1),
					BufferAttribute(8,  4, GL_FLOAT, false, sizeof(InstanceData), 8 * sizeof(float), AttribUsage::User0, 1),
					BufferAttribute(9,  4, GL_FLOAT, false, sizeof(InstanceData), 12 * sizeof(float), AttribUsage::User0, 1),
					BufferAttribute(10, 3, GL_FLOAT, false, sizeof(InstanceData), 16 * sizeof(float), AttribUsage::User1, 1),
					BufferAttribute(11, 3, GL_FLOAT, false, sizeof(InstanceData), 19 * sizeof(float), AttribUsage::User1, 1),
					BufferAttribute(12, 3, GL_FLOAT, false, sizeof(InstanceData), 22 * sizeof(float), AttribUsage::User1, 1),
											  });

				GameObject testObj = scene->CreateEntity("instanced_test");
				testObj.Add<RendererComponent>().SetMesh(instanceTest).SetMaterial(mat);
			}

		}

		InitImGui();

		FrameBuffer::sptr backbuffer = std::make_shared<FrameBuffer>(1920, 1080, 4);
		RenderBufferDesc colorInfo = RenderBufferDesc();
		colorInfo.Attachment = RenderTargetAttachment::Color0;
		colorInfo.Format = RenderTargetType::Color32;
		colorInfo.ShaderReadable = true;
		backbuffer->AddAttachment(colorInfo);
		RenderBufferDesc depthInfo = RenderBufferDesc();
		depthInfo.Attachment = RenderTargetAttachment::DepthStencil;
		depthInfo.Format = RenderTargetType::DepthStencil;
		depthInfo.ShaderReadable = false;
		backbuffer->AddAttachment(depthInfo);

		backbuffer->Validate();

		// Initialize our timing instance and grab a reference for our use
		Timing& time = Timing::Instance();
		time.LastFrame = glfwGetTime();

		// Calculate initial transforms
		Application::Instance().ActiveScene->UpdateTransforms();

		///// Game loop /////
		while (!glfwWindowShouldClose(window)) {
			GameScene::sptr scene = Application::Instance().ActiveScene;
			// We can create a group ahead of time to make iterating on the group faster
			entt::basic_group<entt::entity, entt::exclude_t<>, entt::get_t<Transform>, RendererComponent> renderGroup =
				scene->Registry().group<RendererComponent>(entt::get_t<Transform>());

			glfwPollEvents();
			Input::InputManager::Poll();

			if (InputManager::GetKeyPressed(KeyCode::Q)) {
				isDebugEnabled = !isDebugEnabled;
			}

			// Update the timing
			time.CurrentFrame = glfwGetTime();
			time.DeltaTime = static_cast<float>(time.CurrentFrame - time.LastFrame);

			time.DeltaTime = time.DeltaTime > 1.0f ? 1.0f : time.DeltaTime;

			// Update our FPS tracker data
			fpsBuffer[frameIx] = 1.0f / time.DeltaTime;
			frameIx++;
			if (frameIx >= 128)
				frameIx = 0;

			// We'll make sure our UI isn't focused before we start handling input for our game
			if (!ImGui::IsAnyWindowFocused()) {
				// We need to poll our key watchers so they can do their logic with the GLFW state
				// Note that since we want to make sure we don't copy our key handlers, we need a const
				// reference!
			}

			// Iterate over all the behaviour binding components
			scene->Registry().view<BehaviourBinding>().each([&](entt::entity entity, BehaviourBinding& binding) {
				// Iterate over all the behaviour scripts attached to the entity, and update them in sequence (if enabled)
				for (const auto& behaviour : binding.Behaviours) {
					if (behaviour->Enabled) {
						behaviour->Update(entt::handle(scene->Registry(), entity));
					}
				}
			});

			// Update physics after scripts
			PhysicsSystem::Get().Update();

			// Update all world matrices for this frame, and removes destroyed entities 
			scene->Poll();

			// Grab out camera info from the camera object
			Transform& camTransform = scene->MainCamera.Get<Transform>();
			glm::mat4 view = glm::inverse(camTransform.WorldTransform());
			glm::mat4 projection = scene->MainCamera.Get<Camera>().GetProjection();
			glm::mat4 viewProjection = projection * view;

			DebugDrawer::Get().SetViewProjection(viewProjection);

			// Sort the renderers by shader and material, we will go for a minimizing context switches approach here,
			// but you could for instance sort front to back to optimize for fill rate if you have intensive fragment shaders
			renderGroup.sort<RendererComponent>([](const RendererComponent& l, const RendererComponent& r) {
				// Sort by render layer first, higher numbers get drawn last
				if (!l.Material->IsTransparent & r.Material->IsTransparent) return true;
				if (l.Material->IsTransparent & !r.Material->IsTransparent) return false;

				// Sort by render layer first, higher numbers get drawn last
				if (l.Material->RenderLayer < r.Material->RenderLayer) return true;
				if (l.Material->RenderLayer > r.Material->RenderLayer) return false;

				// Sort by shader pointer next (so materials using the same shader run sequentially where possible)
				if (l.Material->GetShader() < r.Material->GetShader()) return true;
				if (l.Material->GetShader() > r.Material->GetShader()) return false;

				// Sort by material pointer last (so we can minimize switching between materials)
				if (l.Material < r.Material) return true;
				if (l.Material > r.Material) return false;

				return false;
			});

			// Start by assuming no shader or material is applied
			Shader::sptr current = nullptr;
			ShaderMaterial::sptr currentMat = nullptr;
			bool currentTransparency = false;
			bool currentDepthWrite = true;
			bool currentDepthRead = true;

			glDisable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glDepthFunc(GL_LEQUAL);
			glDepthMask(true);

			// Clear the screen
			glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
			glViewport(0, 0, editorWindowSize.x, editorWindowSize.y);
			glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


			if (gameViewport.w * gameViewport.z > 0) {
				if ((uint32_t)gameViewport.z != backbuffer->GetWidth() || (uint32_t)gameViewport.w != backbuffer->GetHeight()) {
					backbuffer->Resize((uint32_t)gameViewport.z, (uint32_t)gameViewport.w);
				}

				backbuffer->Bind();
				glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
				glEnable(GL_DEPTH_TEST);
				glClearDepth(1.0);
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
				glViewport(0, 0, backbuffer->GetWidth(), backbuffer->GetHeight());

				// Iterate over the render group components and draw them
				renderGroup.each([&](entt::entity e, RendererComponent& renderer, Transform& transform) {
					// If the shader has changed, set up it's uniforms
					if (currentTransparency != renderer.Material->IsTransparent) {
						if (renderer.Material->IsTransparent) {
							glEnable(GL_BLEND);
						} else {
							glDisable(GL_BLEND);
						}
					}
					if (currentDepthWrite != renderer.Material->DepthWrite) {
						glDepthMask(renderer.Material->DepthWrite);
					}
					if (currentDepthRead != renderer.Material->DepthRead) {
						if (renderer.Material->DepthRead) {
							glDepthFunc(GL_LEQUAL);
						} else {
							glDepthFunc(GL_ALWAYS);
						}
					}
					if (current != renderer.Material->GetShader()) {
						current = renderer.Material->GetShader();
						current->Bind();
						SetupShaderForFrame(current, view, projection);
					}
					// If the material has changed, apply it
					if (currentMat != renderer.Material) {
						currentMat = renderer.Material;
						currentTransparency = currentMat->IsTransparent;
						currentDepthWrite = currentMat->DepthWrite;
						currentDepthRead = currentMat->DepthRead;
						currentMat->Apply();
					}
					// Render the mesh
					RenderVAO(renderer.Material->GetShader(), renderer.Mesh, viewProjection, transform);
				});

				glDisable(GL_BLEND);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				glDepthFunc(GL_LEQUAL);
				glDepthMask(true);

				if (isDebugEnabled) {
					scene->GetPhysics()->GetWorld()->debugDrawWorld();
					DebugDrawer::Get().FlushAll();

					DebugDrawer& instance = DebugDrawer::Get();

					auto worldView = scene->Registry().view<Transform, PhysicsWorldContainer>();
					worldView.each([&](entt::entity e, Transform& transform, PhysicsWorldContainer& worldContainer) {
						if (worldContainer.World != nullptr) {
							instance.PushWorldMatrix(transform.WorldTransform());
							worldContainer.World->GetWorld()->debugDrawWorld();
							instance.PopWorldMatrix();
							instance.FlushAll();
						}
					});


				}
				backbuffer->UnBind();
				glViewport(0, 0, editorWindowSize.x, editorWindowSize.y);
				Texture2D::sptr buffer = backbuffer->GetAttachment(RenderTargetAttachment::Color0);

				backbuffer->Bind(RenderTargetBinding::Read, true);
				glReadBuffer(*RenderTargetAttachment::Color0);
				glDrawBuffer(GL_BACK);
				glScissor(gameViewport.x, gameViewport.y, gameViewport.z, gameViewport.w);
				backbuffer->Blit(glm::ivec4(0, 0, backbuffer->GetWidth(), backbuffer->GetHeight()), gameViewport, BufferFlags::Color);
				backbuffer->UnBind();
				glScissor(0, 0, editorWindowSize.x, editorWindowSize.y);

			}

			DebugDrawer::Get().FlushAll();

			// Draw our ImGui content
			RenderImGui();

			glfwSwapBuffers(window);
			time.LastFrame = time.CurrentFrame;
		}

		// Nullify scene so that we can release references
		Application::Instance().ActiveScene = nullptr;
		Input::InputManager::Uninitialize();
		ShutdownImGui();
	}

	DebugDrawer::Uninitialize();

	// Clean up the toolkit logger so we don't leak memory
	Logger::Uninitialize();
	return 0;
}