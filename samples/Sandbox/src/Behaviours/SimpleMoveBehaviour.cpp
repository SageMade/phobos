#include "SimpleMoveBehaviour.h"
#include "Phobos/Application.h"
#include "Phobos/Gameplay/Transform.h"
#include "Phobos/Gameplay/Timing.h"

#include "GLFW/glfw3.h"
#include "Phobos/Application.h"

using namespace entt::literals;

void SimpleMoveBehaviour::Update(entt::handle entity)
{
	float dt = Phobos::Gameplay::Timing::Instance().DeltaTime;
	GLFWwindow* window = Phobos::Application::Instance().Window;
	Phobos::Gameplay::Transform& transform = entity.get<Phobos::Gameplay::Transform>();

	if (Relative) {
		if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
			transform.MoveLocal(0.0f, -1.0f * dt, 0.0f);
		}
		if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
			transform.MoveLocal(0.0f, 1.0f * dt, 0.0f);
		}
		if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
			transform.MoveLocal(-1.0f * dt, 0.0f, 0.0f);
		}
		if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
			transform.MoveLocal(1.0f * dt, 0.0f, 0.0f);
		}
		if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS) {
			transform.MoveLocal(0.0f, 0.0f, 1.0f * dt);
		}
		if (glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS) {
			transform.MoveLocal(0.0f, 0.0f, -1.0f * dt);
		}

		if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS) {
			transform.RotateLocal(0.0f, -45.0f * dt, 0.0f);
		}
		if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) {
			transform.RotateLocal(0.0f, 45.0f * dt, 0.0f);
		}
		if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
			transform.RotateLocal(45.0f * dt, 0.0f, 0.0f);
		}
		if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
			transform.RotateLocal(-45.0f * dt, 0.0f, 0.0f);
		}
		if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS) {
			transform.RotateLocal(0.0f, 0.0f, 45.0f * dt);
		}
		if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS) {
			transform.RotateLocal(0.0f, 0.0f, -45.0f * dt);
		}
	} else
	{
		if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
			transform.MoveLocalFixed(0.0f, -1.0f * dt, 0.0f);
		}
		if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
			transform.MoveLocalFixed(0.0f, 1.0f * dt, 0.0f);
		}
		if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
			transform.MoveLocalFixed(-1.0f * dt, 0.0f, 0.0f);
		}
		if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
			transform.MoveLocalFixed(1.0f * dt, 0.0f, 0.0f);
		}
		if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS) {
			transform.MoveLocalFixed(0.0f, 0.0f, 1.0f * dt);
		}
		if (glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS) {
			transform.MoveLocalFixed(0.0f, 0.0f, -1.0f * dt);
		}

		if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS) {
			transform.RotateLocalFixed(0.0f, -45.0f * dt, 0.0f);
		}
		if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) {
			transform.RotateLocalFixed(0.0f, 45.0f * dt, 0.0f);
		}
		if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
			transform.RotateLocalFixed(45.0f * dt, 0.0f, 0.0f);
		}
		if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
			transform.RotateLocalFixed(-45.0f * dt, 0.0f, 0.0f);
		}
		if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS) {
			transform.RotateLocalFixed(0.0f, 0.0f, 45.0f * dt);
		}
		if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS) {
			transform.RotateLocalFixed(0.0f, 0.0f, -45.0f * dt);
		}
	}
}

void SimpleMoveBehaviour::RegisterMeta()
{
	entt::meta<SimpleMoveBehaviour>().
		type(GetTypeNameHash()).
		base<IBehaviour>().
		ctor().
		data<&SimpleMoveBehaviour::Relative>("Relative"_hs).prop("_type", entt::type_seq<bool>::value());
}