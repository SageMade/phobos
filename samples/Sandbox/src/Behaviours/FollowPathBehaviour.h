#pragma once
#include "Phobos/Gameplay/IBehaviour.h"
#include <vector>
#include <GLM/glm.hpp>

class FollowPathBehaviour final : public Phobos::Gameplay::IBehaviour
{
public:
	FollowPathBehaviour() :
		Points(std::vector<glm::vec3>()),
		Speed(1.0f),
		_nextPointIx(0) { }
	~FollowPathBehaviour() override = default;

	std::vector<glm::vec3> Points;
	float                  Speed;

	void Update(entt::handle entity) override;
	inline entt::meta_handle GetHandle() override { return *this; };

protected:
	void RegisterMeta() override;

private:
	int _nextPointIx;

	friend class cereal::access;

	template <class Archive>
	void serialize(Archive& ar) {
		ar(CEREAL_NVP(Points));
		ar(CEREAL_NVP(Speed));
	}
};

RegisterBehaviourType(FollowPathBehaviour);