#pragma once
#include "Phobos/Gameplay/IBehaviour.h"
#include <GLM/glm.hpp>

class SimpleRotateBehaviour :
    public Phobos::Gameplay::IBehaviour {
public:
	bool Relative = true;
	glm::vec3 EulerPerSec = glm::vec3(0.0f);
	SimpleRotateBehaviour() = default;
	~SimpleRotateBehaviour() = default;
	void Update(entt::handle entity) override;
	inline entt::meta_handle GetHandle() override { return *this; };
protected:
	void RegisterMeta() override;

	friend class cereal::access;

	template <class Archive>
	void serialize(Archive & ar) {
		ar(CEREAL_NVP(Relative));
		ar(CEREAL_NVP(EulerPerSec));
	}
};

RegisterBehaviourType(SimpleRotateBehaviour);
