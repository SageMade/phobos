#include "FollowPathBehaviour.h"

#include "Phobos/Gameplay/Timing.h"
#include "Phobos/Gameplay/Transform.h"

using namespace Phobos::Gameplay;
using namespace entt::literals;

void FollowPathBehaviour::Update(entt::handle entity) {
	if (Points.size() >= 2) {
		Transform& transform = entity.get<Transform>();

		const glm::vec3 next = Points[_nextPointIx];
		const glm::vec3 direction = glm::normalize(next - transform.GetLocalPosition());
		//transform.LookAt(next);
		transform.MoveLocalFixed(direction * Speed * Timing::Instance().DeltaTime);
		if (glm::distance(transform.GetLocalPosition(), next) < Speed * Timing::Instance().DeltaTime) {
			_nextPointIx++;
			if (_nextPointIx >= Points.size()) {
				_nextPointIx = 0;
			}
		}
	}
}

void FollowPathBehaviour::RegisterMeta()
{
	entt::meta<FollowPathBehaviour>().
		type(GetTypeNameHash()).
		base<IBehaviour>().
		ctor().
		data<&FollowPathBehaviour::Points>("Points"_hs).
		data<&FollowPathBehaviour::Speed>("Speed"_hs);
}
