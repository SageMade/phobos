#pragma once
#include "Phobos/Gameplay/IBehaviour.h"
#include <GLM/glm.hpp>
#include <GLM/gtc/quaternion.hpp>

class CameraControlBehaviour final : public Phobos::Gameplay::IBehaviour
{
public:
	CameraControlBehaviour() = default;
	void OnLoad(entt::handle entity) override;
	void Update(entt::handle entity) override;
	inline entt::meta_handle GetHandle() override { return *this; };

protected:
	void RegisterMeta() override;

	double _prevMouseX, _prevMouseY;
	float _rotationX, _rotationY;
	bool _isPressed;
	glm::quat _initial;

	friend class cereal::access;

	template <class Archive>
	void serialize(Archive& ar) {
		ar(cereal::make_nvp("InitialRotation", _initial));
	}
};

RegisterBehaviourType(CameraControlBehaviour);
