#include "SimpleRotateBehaviour.h"


#include "Phobos/Gameplay/Timing.h"
#include "Phobos/Gameplay/Transform.h"

using namespace entt::literals;

void SimpleRotateBehaviour::Update(entt::handle entity)
{
	if (Relative)
	{
		auto& transform = entity.get<Phobos::Gameplay::Transform>();
		glm::vec3 rot = transform.GetLocalRotation();
		transform.SetLocalRotation(rot + EulerPerSec * Phobos::Gameplay::Timing::Instance().DeltaTime);
	}
}

void SimpleRotateBehaviour::RegisterMeta()
{
	entt::meta<SimpleRotateBehaviour>().
		type(GetTypeNameHash()).
		base<IBehaviour>().
		ctor().
		data<&SimpleRotateBehaviour::Relative>("Relative"_hs).prop("_name"_hs, "Relative").
		data<&SimpleRotateBehaviour::EulerPerSec>("EulerPerSec"_hs).prop("_name"_hs, "Degrees/s");
}
