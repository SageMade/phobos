#pragma once
#include "Phobos/Gameplay/IBehaviour.h"
class SimpleMoveBehaviour : public Phobos::Gameplay::IBehaviour
{
public:
	bool Relative = true;
	SimpleMoveBehaviour() = default;
	~SimpleMoveBehaviour() = default;

	void Update(entt::handle entity) override;
	inline entt::meta_handle GetHandle() override { return *this; };
protected:
	void RegisterMeta() override;

	friend class cereal::access;

	template <class Archive>
	void serialize(Archive& ar) {
		ar(CEREAL_NVP(Relative));
	}
};

RegisterBehaviourType(SimpleMoveBehaviour);
